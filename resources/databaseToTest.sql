CREATE DATABASE  IF NOT EXISTS `telecomTest` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `telecomTest`;
-- MySQL dump 10.13  Distrib 5.6.23, for Linux (i686)
--
-- Host: localhost    Database: telecom
-- ------------------------------------------------------
-- Server version	5.6.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `company_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) NOT NULL,
  `company_description` text NOT NULL,
  `company_shortname` varchar(20) NOT NULL,
  `company_url` varchar(45) NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` (`company_id`, `company_name`, `company_description`, `company_shortname`, `company_url`) VALUES (1,'Movistar','Movistar (Telefónica del Perú S.A.A.). \"Compartida, la vida es más\"','movistar','http://www.movistar.com.pe');
INSERT INTO `company` (`company_id`, `company_name`, `company_description`, `company_shortname`, `company_url`) VALUES (2,'Claro','Claro que somos mas!!!','claro','http://www.claro.com.pe');
INSERT INTO `company` (`company_id`, `company_name`, `company_description`, `company_shortname`, `company_url`) VALUES (3,'Bitel','Te escuchamos siempre, Bitel.','bitel','http://www.bitel.com.pe');
INSERT INTO `company` (`company_id`, `company_name`, `company_description`, `company_shortname`, `company_url`) VALUES (4,'Entel','Un Perú mejor conectado es posible, Entel.','entel','http://www.entel.pe');
/*!40000 ALTER TABLE `company` ENABLE KEYS */;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `product_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(600) NOT NULL,
  `product_shortname` varchar(100) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `product_price` double NOT NULL,
  `product_image_path` varchar(45) NOT NULL,
  `product_category_id` bigint(20) NOT NULL,
  `product_description` varchar(2000) NOT NULL,
  `product_positive` bigint(20) NOT NULL,
  `product_negative` bigint(20) NOT NULL,
  PRIMARY KEY (`product_id`),
  KEY `fk_product_category_idx` (`product_category_id`),
  KEY `fk_product_company_idx` (`company_id`),
  CONSTRAINT `fk_product_company` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_product_category` FOREIGN KEY (`product_category_id`) REFERENCES `product_category` (`product_category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`product_id`, `product_name`, `product_shortname`, `company_id`, `product_price`, `product_image_path`, `product_category_id`, `product_description`, `product_positive`, `product_negative`) VALUES (1,'Internet 2000 Kbps','Internet 2000 Kbps',2,68,'image.png',1,'Conéctate desde una PC o utiliza el router Wi-fi para acceder a Internet desde una laptop u otro dispositivo. Comparte, navega y descubre todo lo que puedes hacer con una conexión a Internet.',23,22);
INSERT INTO `product` (`product_id`, `product_name`, `product_shortname`, `company_id`, `product_price`, `product_image_path`, `product_category_id`, `product_description`, `product_positive`, `product_negative`) VALUES (2,'Internet 4000 Kbps','Internet 4000 Kbps',2,88,'image.png',1,'Una súper velocidad para disfrutar de Internet ilimitadamente. Si lo tuyo es la velocidad este plan está hecho para ti. Descarga o sube tus archivos favoritos a la web.',5,8);
INSERT INTO `product` (`product_id`, `product_name`, `product_shortname`, `company_id`, `product_price`, `product_image_path`, `product_category_id`, `product_description`, `product_positive`, `product_negative`) VALUES (3,'Internet 8000 Kbps','Internet 8000 Kbps',2,118,'image.png',1,'Una súper velocidad para disfrutar ilimitadamente de tu conexión Internet. Un plan increible que esparabas para tu hogar u oficina te ofrece Claro. Por promoción sin costo de instalación.',15,30);
INSERT INTO `product` (`product_id`, `product_name`, `product_shortname`, `company_id`, `product_price`, `product_image_path`, `product_category_id`, `product_description`, `product_positive`, `product_negative`) VALUES (4,'Dúo Internet 2 Mbps','2 Mbps',1,69,'image.png',1,'Por promoción navega a 2 Mbps, velocidad nominal de 500 kbps. Modem Wifi. Aula 365 gratis. Línea Tarifa Control 30. 1,800 segundos para realizar llamadas locales de fijo a fijo dentro de la Red de Movistar.',1,50);
INSERT INTO `product` (`product_id`, `product_name`, `product_shortname`, `company_id`, `product_price`, `product_image_path`, `product_category_id`, `product_description`, `product_positive`, `product_negative`) VALUES (5,'Dúo Internet 4 Mbps','4 Mbps',1,89,'image.png',1,'Por promoción navega a 4 Mbps, velocidad nominal de 1 Mbps. Modem Wifi. Aula 365 Gratis. Tarifa Semiplana 250. Llamadas ilimitadas a fijos locales de la red Movistar en Horario Reducido (HR) y 250 minutos para llamadas a fijos locales dentro y fuera de la red fija de Movistar en Horario normal (HN). No incluye SVA\'s.',5,20);
INSERT INTO `product` (`product_id`, `product_name`, `product_shortname`, `company_id`, `product_price`, `product_image_path`, `product_category_id`, `product_description`, `product_positive`, `product_negative`) VALUES (6,'Dúo Internet 8 Mbps','8 Mbps',1,129,'image.png',1,'Por promoción navega a 8 Mbps, velocidad nominal de 4 Mbps. Modem Wifi. Aula 365 Gratis. Tarifa Plana Local. Llamadas ilimitadas a fijos locales dentro de la red fija de Movistar las 24 horas del día. Incluye identificador de llamadas y casilla de voz.',12,25);
INSERT INTO `product` (`product_id`, `product_name`, `product_shortname`, `company_id`, `product_price`, `product_image_path`, `product_category_id`, `product_description`, `product_positive`, `product_negative`) VALUES (7,'Dúo Internet 15 Mbps','15 Mbps',1,199,'image.png',1,'Navegación a velocidad de 15 Mbps. Modem Wifi. Aula 365 Gratis. Tarifa Plana Local. Llamadas ilimitadas a fijos locales dentro de la red fija de Movistar las 24 horas del día. Incluye identificador de llamadas y casilla de voz.',12,23);
INSERT INTO `product` (`product_id`, `product_name`, `product_shortname`, `company_id`, `product_price`, `product_image_path`, `product_category_id`, `product_description`, `product_positive`, `product_negative`) VALUES (8,'Multidestino 100','Multidestino 100',2,39,'image.png',2,'¡Llamar al extranjero nunca fue tan cómodo! Ahora podrás hablar 100 minutos a más de 20 países.',12,43);
INSERT INTO `product` (`product_id`, `product_name`, `product_shortname`, `company_id`, `product_price`, `product_image_path`, `product_category_id`, `product_description`, `product_positive`, `product_negative`) VALUES (9,'Multidestino 200','Multidestino 200',2,49,'image.png',2,'Habla a más de 20 países desde tu teléfono fijo sin problemas. Conoce el plan Multidestino 200 aquí.',22,43);
INSERT INTO `product` (`product_id`, `product_name`, `product_shortname`, `company_id`, `product_price`, `product_image_path`, `product_category_id`, `product_description`, `product_positive`, `product_negative`) VALUES (10,'Multidestino 400','Multidestino 400',2,69,'image.png',2,'Llama sin problemas al extranjero y habla todo lo que quieras con el plan Multidestino 400.',22,55);
INSERT INTO `product` (`product_id`, `product_name`, `product_shortname`, `company_id`, `product_price`, `product_image_path`, `product_category_id`, `product_description`, `product_positive`, `product_negative`) VALUES (11,'Líneas Libres Tarifa Plana Nacional 89','Tarifa Plana Nacional 89',1,74.8,'image.png',2,'Permite realizar llamadas ilimitadas a destinos fijos locales y destinos fijos de Larga Distancia de la red de Movistar las 24 horas del día. Full SVA’s. Costo de instalación: S/.60.00 en 12 cuotas',34,333);
INSERT INTO `product` (`product_id`, `product_name`, `product_shortname`, `company_id`, `product_price`, `product_image_path`, `product_category_id`, `product_description`, `product_positive`, `product_negative`) VALUES (12,'Líneas Libres Tarifa Plana Nacional 99','Tarifa Plana Nacional 99',1,83.47,'image.png',2,'Llamadas ilimitadas a destinos fijos locales y Nacionales dentro de la red de Movistar y 200 minutos para llamadas a destinos fijos locales a otras operadoras. Los minutos adicionales a otras operadoras  se cobran al segundo .Full SVA’s. Costo de instalación: S/.118.02 en 12 cuotas',33,22);
INSERT INTO `product` (`product_id`, `product_name`, `product_shortname`, `company_id`, `product_price`, `product_image_path`, `product_category_id`, `product_description`, `product_positive`, `product_negative`) VALUES (13,'Línea Control. Control al Segundo','Control al Segundo',1,32,'image.png',2,'Es un plan que brinda 7.800 segundos para realizar llamadas a locales de fijo a fijo. Costo de instalación: S/.60.00 en 12 cuotas',12,22);
INSERT INTO `product` (`product_id`, `product_name`, `product_shortname`, `company_id`, `product_price`, `product_image_path`, `product_category_id`, `product_description`, `product_positive`, `product_negative`) VALUES (14,'Línea Prepago. Prepago al Segundo','Prepago al Segundo',1,23,'image.png',2,'Es un plan que permite contar con 4.500 segundos (75 minutos) mensuales para realizar llamadas locales de fijo a fijo de cualquier operador. Costo de instalación: S/.395.30',1,222);
INSERT INTO `product` (`product_id`, `product_name`, `product_shortname`, `company_id`, `product_price`, `product_image_path`, `product_category_id`, `product_description`, `product_positive`, `product_negative`) VALUES (15,'Líneas Fonoya','Líneas Fonoya',1,20,'image.png',2,'El plan incluye 100 minutos a fijos locales de cualquier operador o 113 minutos a móviles de Movistar y 50 Mensajes de texto a Movistar y Fonoya a nivel nacional. Incluye Casilla de Voz, identificador de llamadas y *Llámame que no tengo saldo”. Costo de instalación: S/.69 al contado',12,222);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

--
-- Table structure for table `product_category`
--

DROP TABLE IF EXISTS `product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_category` (
  `product_category_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_category_name` varchar(600) NOT NULL,
  `product_category_image_path` varchar(100) NOT NULL,
  PRIMARY KEY (`product_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_category`
--

/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
INSERT INTO `product_category` (`product_category_id`, `product_category_name`, `product_category_image_path`) VALUES (1,'Internet Fijo','image_category.png');
INSERT INTO `product_category` (`product_category_id`, `product_category_name`, `product_category_image_path`) VALUES (2,'Telefonía Fija','image_category.png');
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;

--
-- Table structure for table `product_has_company`
--

DROP TABLE IF EXISTS `product_has_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_has_company` (
  `product_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `product_has_company_positive` int(11) NOT NULL,
  `product_has_company_negative` int(11) NOT NULL,
  KEY `fk_product_has_company_1_idx` (`product_id`),
  KEY `fk_product_has_company_company_idx` (`company_id`),
  CONSTRAINT `fk_product_has_company_company` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_has_company_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_has_company`
--

/*!40000 ALTER TABLE `product_has_company` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_has_company` ENABLE KEYS */;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review` (
  `review_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `review_title` varchar(600) NOT NULL,
  `review_body` text NOT NULL,
  `review_polarity` int(11) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `company_id` bigint(20) NOT NULL,
  `source_id` bigint(20) NOT NULL,
  PRIMARY KEY (`review_id`),
  KEY `fk_review_product_idx` (`product_id`),
  KEY `fk_review_source_idx` (`source_id`),
  KEY `fk_review_company_idx` (`company_id`),
  CONSTRAINT `fk_review_company` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_review_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_review_source` FOREIGN KEY (`source_id`) REFERENCES `source` (`source_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

/*!40000 ALTER TABLE `review` DISABLE KEYS */;
/*!40000 ALTER TABLE `review` ENABLE KEYS */;

--
-- Table structure for table `sentence`
--

DROP TABLE IF EXISTS `sentence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sentence` (
  `sentence_id` bigint(20) NOT NULL,
  `sentence_text` varchar(1000) NOT NULL,
  `sentence_polarity` int(11) NOT NULL,
  `review_id` bigint(20) NOT NULL,
  PRIMARY KEY (`sentence_id`),
  KEY `fk_sentence_review_idx` (`review_id`),
  CONSTRAINT `fk_sentence_review` FOREIGN KEY (`review_id`) REFERENCES `review` (`review_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sentence`
--

/*!40000 ALTER TABLE `sentence` DISABLE KEYS */;
/*!40000 ALTER TABLE `sentence` ENABLE KEYS */;

--
-- Table structure for table `source`
--

DROP TABLE IF EXISTS `source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `source` (
  `source_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `source_name` varchar(255) DEFAULT NULL,
  `source_url` varchar(100) DEFAULT NULL,
  `source_description` text,
  PRIMARY KEY (`source_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `source`
--

/*!40000 ALTER TABLE `source` DISABLE KEYS */;
/*!40000 ALTER TABLE `source` ENABLE KEYS */;

--
-- Table structure for table `top_product`
--

DROP TABLE IF EXISTS `top_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `top_product` (
  `top_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) NOT NULL,
  `product_category_id` bigint(20) NOT NULL,
  PRIMARY KEY (`top_product_id`),
  KEY `fk_top_product_product_idx` (`product_id`),
  KEY `fk_top_product_product_category_idx` (`product_category_id`),
  CONSTRAINT `fk_top_product_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_top_product_product_category` FOREIGN KEY (`product_category_id`) REFERENCES `product_category` (`product_category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `top_product`
--

/*!40000 ALTER TABLE `top_product` DISABLE KEYS */;
INSERT INTO `top_product` (`top_product_id`, `product_id`, `product_category_id`) VALUES (1,1,1);
INSERT INTO `top_product` (`top_product_id`, `product_id`, `product_category_id`) VALUES (2,10,2);
/*!40000 ALTER TABLE `top_product` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-12 10:48:06
