CREATE DATABASE  IF NOT EXISTS `parlakuytest` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `parlakuytest`;
-- MySQL dump 10.13  Distrib 5.6.23, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: parlakuytest
-- ------------------------------------------------------
-- Server version	5.6.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `category_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(200) NOT NULL,
  `category_image_path` varchar(100) DEFAULT NULL,
  `category_top_id` bigint(20) NOT NULL DEFAULT '0',
  `category_pattern_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `company_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(200) NOT NULL,
  `company_description` text,
  `company_url` varchar(200) DEFAULT NULL,
  `company_image_path` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `company_has_product`
--

DROP TABLE IF EXISTS `company_has_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_has_product` (
  `company_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `company_has_product_url` varchar(200) DEFAULT NULL COMMENT '\n\n',
  `company_has_product_state` tinyint(1) NOT NULL DEFAULT '1',
  `company_has_product_price` double DEFAULT NULL,
  PRIMARY KEY (`company_id`,`product_id`),
  KEY `fk_company_has_product_product1` (`product_id`),
  KEY `fk_company_has_product_company1` (`company_id`),
  CONSTRAINT `fk_company_has_product_company1` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_company_has_product_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dictionary`
--

DROP TABLE IF EXISTS `dictionary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dictionary` (
  `dictionary_id` bigint(20) NOT NULL,
  `dictionary_word` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`dictionary_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `most_searched_sentences`
--

DROP TABLE IF EXISTS `most_searched_sentences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `most_searched_sentences` (
  `most_searched_sentences_id` bigint(20) NOT NULL,
  `most_searched_sentences_sentence` varchar(200) DEFAULT NULL,
  `most_searched_sentences_value` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`most_searched_sentences_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `product_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(200) NOT NULL,
  `product_brand` varchar(200) DEFAULT NULL,
  `product_dimension` varchar(45) DEFAULT NULL,
  `product_operating_system` varchar(200) DEFAULT NULL,
  `product_image_path` varchar(100) DEFAULT NULL,
  `product_description` text,
  `product_positive` bigint(20) NOT NULL DEFAULT '0',
  `product_negative` bigint(20) NOT NULL DEFAULT '0',
  `product_memory` float DEFAULT NULL,
  `product_ram` float DEFAULT NULL,
  `product_processor` varchar(100) DEFAULT NULL,
  `product_weight` float DEFAULT NULL,
  `product_battery` varchar(200) DEFAULT NULL,
  `product_extra_feature` varchar(1000) DEFAULT NULL,
  `category_id` bigint(20) NOT NULL,
  PRIMARY KEY (`product_id`),
  KEY `fk_product_category` (`category_id`),
  CONSTRAINT `fk_product_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reviewproduct`
--

DROP TABLE IF EXISTS `reviewproduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviewproduct` (
  `reviewProduct_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) NOT NULL,
  `source_id` bigint(20) NOT NULL,
  `reviewProduct_title` varchar(600) NOT NULL,
  `reviewProduct_body` text,
  `reviewProduct_polarity` int(11) DEFAULT NULL,
  PRIMARY KEY (`reviewProduct_id`),
  KEY `fk_reviewProduct_product1` (`product_id`),
  KEY `fk_reviewProduct_source1` (`source_id`),
  CONSTRAINT `fk_reviewProduct_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reviewProduct_source1` FOREIGN KEY (`source_id`) REFERENCES `source` (`source_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reviewservice`
--

DROP TABLE IF EXISTS `reviewservice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviewservice` (
  `reviewService_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `service_id` bigint(20) NOT NULL,
  `source_id` bigint(20) NOT NULL,
  `reviewService_title` varchar(600) NOT NULL,
  `reviewService_body` text,
  `reviewService_polarity` int(11) DEFAULT NULL,
  PRIMARY KEY (`reviewService_id`),
  KEY `fk_reviewService_sevice1` (`service_id`),
  KEY `fk_reviewService_source1` (`source_id`),
  CONSTRAINT `fk_reviewService_sevice1` FOREIGN KEY (`service_id`) REFERENCES `service` (`service_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reviewService_source1` FOREIGN KEY (`source_id`) REFERENCES `source` (`source_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sentenceproduct`
--

DROP TABLE IF EXISTS `sentenceproduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sentenceproduct` (
  `sentenceProduct_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reviewProduct_id` bigint(20) NOT NULL,
  `sentenceProduct_text` text,
  `sentenceProduct_polarity` int(11) DEFAULT NULL,
  PRIMARY KEY (`sentenceProduct_id`),
  KEY `fk_sentenceProduct_reviewProduct1` (`reviewProduct_id`),
  CONSTRAINT `fk_sentenceProduct_reviewProduct1` FOREIGN KEY (`reviewProduct_id`) REFERENCES `reviewproduct` (`reviewProduct_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sentenceservice`
--

DROP TABLE IF EXISTS `sentenceservice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sentenceservice` (
  `sentenceService_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reviewService_id` bigint(20) NOT NULL,
  `sentenceService_text` text,
  `sentenceService_polarity` int(11) DEFAULT NULL,
  PRIMARY KEY (`sentenceService_id`),
  KEY `fk_sentenceService_reviewService1` (`reviewService_id`),
  CONSTRAINT `fk_sentenceService_reviewService1` FOREIGN KEY (`reviewService_id`) REFERENCES `reviewservice` (`reviewService_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `service_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `service_name` varchar(200) NOT NULL,
  `service_description` text,
  `service_price` double DEFAULT NULL,
  `service_phone_mb` int(11) DEFAULT NULL,
  `service_phone_min_all` int(11) DEFAULT NULL,
  `service_phone_extra` varchar(1000) DEFAULT NULL,
  `service_tv_channel` varchar(200) DEFAULT NULL,
  `service_tv_extra` varchar(1000) DEFAULT NULL,
  `service_positive` bigint(20) NOT NULL DEFAULT '0',
  `service_negative` bigint(20) NOT NULL DEFAULT '0',
  `service_image_path` varchar(100) DEFAULT NULL,
  `service_url` varchar(100) DEFAULT NULL,
  `service_state` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`service_id`),
  KEY `fk_sevice_company` (`company_id`),
  KEY `fk_sevice_category1` (`category_id`),
  CONSTRAINT `fk_sevice_category1` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sevice_company` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `service_has_product`
--

DROP TABLE IF EXISTS `service_has_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_has_product` (
  `service_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `service_has_product_num` int(11) NOT NULL,
  `service_has_product_price` double DEFAULT NULL,
  `service_has_product_extra` varchar(1000) DEFAULT NULL,
  `service_has_product_state` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`service_id`,`product_id`,`service_has_product_num`),
  KEY `fk_sevice_has_product_product1` (`product_id`),
  KEY `fk_sevice_has_product_sevice1` (`service_id`),
  CONSTRAINT `fk_sevice_has_product_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sevice_has_product_sevice1` FOREIGN KEY (`service_id`) REFERENCES `service` (`service_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `source`
--

DROP TABLE IF EXISTS `source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `source` (
  `source_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `source_name` varchar(255) DEFAULT NULL,
  `source_url` varchar(100) DEFAULT NULL,
  `source_description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`source_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-14 14:58:08
