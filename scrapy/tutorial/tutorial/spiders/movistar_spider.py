import scrapy
from scrapy.contrib.linkextractors.lxmlhtml import LxmlLinkExtractor
#from tutorial.settings import *
#from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from tutorial.items import MobileItem
from tutorial.items import MovistarImage
from tutorial.items import MovistarCarac
from tutorial.items import ServiceTV
#from MySQLdb import escape_string
#from scrapy.item import Item, Field
import time
#class MyItem(Item):
#    url= Field()

class MovistarSpider(CrawlSpider):
    name = "movistar"
    allowed_domains = ["catalogo.movistar.com.pe"]
    start_urls = ["http://catalogo.movistar.com.pe/smartphones","http://catalogo.movistar.com.pe/basicos"]
    pipelines = ['MovilTestPipeline']
    rules =(Rule(LxmlLinkExtractor(allow=(),deny=(),restrict_xpaths=('.//article[@id]'), deny_extensions=()), callback='parse_fuckyou', follow=True),)
    #rules = (Rule(SgmlLinkExtractor(), callback='parse_otros', follow=False), )

    def parse_fuckyou(self, response):
        item = MobileItem()
        time.sleep(1)
        item['product_name'] = response.xpath('//h1[@class="block-title"]/text()').extract()[0].encode("ascii","ignore").replace(" LTE", "")
        item['product_price'] = [float(x.replace(' S/. ', '').replace(',', '')) for x in response.xpath('//td[@class="pequipo-value"]/text()').extract()]
        item['product_descrip'] = ""
        #"".join(response.xpath('//div[@id="caracteristicas"]/div[2]/div/p/text()').extract()).replace("  ","")
        item['product_url'] = response.url
        item['product_company'] = 1
        if len(item['product_price']) < 2:
            item['product_category'] = 4
        else:
            item['product_category'] = 3
        item['product_image_path'] = item['product_name'].replace(" ","+") + ".png"
        #(item['product_name'].upper()[item['product_name'].index(" ")+1:]).replace('+', '').replace('-',' ').replace('@','A') + '.png'
        item['product_plan'] = ["Plan Vuela " + x.replace('S/. ','').replace('.','') for x in response.xpath('//td[@class="pequipo-value-mensual"]/text()').extract()]
        if item['product_plan'] != []:
            if "34" in item['product_plan'][0]:
                item['product_plan'] = ["Plan Voz 34"]
        item['product_prepago'] = [x.replace('S/.','') for x in response.xpath('//p[@style="margin:0;"]/span/text()').extract()]
        if item['product_prepago'] == [] and response.xpath('//span[@id="titleprepprecio"]/text()').extract() != []:
            item['product_prepago'] = response.xpath('//span[@id="titleprepprecio"]/text()').extract()
        if item['product_prepago'] == []:
            item['product_prepago'] = item['product_price'][0]
        else:
            item['product_prepago'] = int((item['product_prepago'][0]).replace(',',''))
        item['product_plan_time'] = [18]*len(item['product_plan'])
        if response.xpath('//span[contains(@href, "precios-planes/portabilidad")]').extract() != [] :
            item['product_plan_time'] = item['product_plan_time'][len(item['product_plan_time'])/2:]
            for i in range(len(item['product_plan_time'])/2):
                item['product_plan_time'].append("Portabilidad")
        yield item

    """
    def parse_otros(self,response):
        #print response.url
        item=MovistarItem()
        item['product_name'] = response.url
        return item

    def parse_url(self, response):
        item = MyItem()
        item['url'] = response.xpath('//td[@class="pequipo-value"]/text()').extract()[0]
        return item

    """




class MovistarImageSpider(CrawlSpider):

    name = "movistar_imagenes"
    allowed_domains = ["catalogo.movistar.com.pe"]
    start_urls = ["http://catalogo.movistar.com.pe/smartphones","http://catalogo.movistar.com.pe/basicos"]

    rules =(Rule(LxmlLinkExtractor(allow=(),deny=(),restrict_xpaths=('.//article[@id]'), deny_extensions=()), callback='parse_image_url', follow=False),)

    def parse_image_url(self,response):
        item = MovistarImage()
        item['image_name'] = response.xpath('//h1[@class="block-title"]/text()').extract()[0].encode("ascii","ignore")
        item['image_urls'] = response.xpath('//figure[@class="image-canvas"][1]/img/@src').extract()
        return item

class MovistarCaracSpider(CrawlSpider):

    name = "movistar_caracteristicas"
    allowed_domains = ["catalogo.movistar.com.pe"]
    start_urls = ["http://catalogo.movistar.com.pe/smartphones","http://catalogo.movistar.com.pe/basicos"]

    rules =(Rule(LxmlLinkExtractor(allow=(),deny=(),restrict_xpaths=('.//article[@id]'), deny_extensions=()), callback='parse_fuck', follow=False),)

    def parse_fuck(self,response):
        item=MovistarCarac()
        item['nameProduct'] = response.xpath('//h1[@class="block-title"]/text()').extract()[0].encode("ascii","ignore")
        item['Category_idCategory'] = 1
        item['brandProduct'] = item['nameProduct'].split(" ")[0]
        item['operatingSystemProduct'] = response.xpath('//span[contains(text(),"Sistema Operativo")]/following-sibling::div/p/text()').extract()
        item['dimensionProduct'] = response.xpath('//span[contains(text(),"Dimensiones")]/following-sibling::div/p/text()').extract()
        item['bandProduct'] = [x for x in response.xpath('//div[@class="grid-item-row" and not(div/p/img)]/span[contains(text(),"G")]/text()').extract() if len(x)==2]
        item['imagePathProduct'] = "/imagenes/" + item['nameProduct'].upper()
        item['descriptionProduct'] = response.xpath('//span[contains(text(),"Sistema Operativo")]/following-sibling::div/p/text()').extract()
        item['batteryProduct'] = response.xpath('//span[contains(text(),"Bate")]/following-sibling::div/p/text()').extract()
        item['bluetoothProduct'] = response.xpath('//span[contains(text(),"Bluetooth")]/following-sibling::div/p/text()').extract()
        item['cameraProduct'] = response.xpath('//div/span[contains(text()," de Fotos")]/following-sibling::div/div/p/text()').extract()
        item['dualSimProduct'] = response.xpath('//span[contains(text(),"Dual SIM")]/following-sibling::div/p/img/@src').extract()[0] == "/images/img-check.gif"
        item['gpsProduct'] = response.xpath('//span[contains(text(),"GPS")]/following-sibling::div/p/img/@src').extract()[0] == "/images/img-check.gif"
        item['memoryProduct'] = response.xpath('//span[contains(text(),"Memoria del ")]/following-sibling::div/p/text()').extract()
        item['ramProduct'] = response.xpath('//p[contains(text(),"Memoria RAM")]/text()').extract()
        item['expandMemoryProduct'] = response.xpath('//span[contains(text(),"Memoria expandible")]/following-sibling::div/p/text()').extract()[0].replace("Capacidad","")
        item['screenProduct'] = response.xpath('//p[contains(text(),"Resolucion")]/text()').extract()[0].replace("Resolucion ","")
        item['processorProduct'] = response.xpath('//span[contains(text(),"Procesador")]/following-sibling::div/p/text()').extract()
        item['weightProduct'] = response.xpath('//span[contains(text(),"Peso")]/following-sibling::div/p/text()').extract()
        item['wifiProduct'] = response.xpath('//span[contains(text(),"Wi-fi")]/following-sibling::div/p/img/@src').extract()[0] == "/images/img-check.gif"

        return item


class MovistarTVSpider(CrawlSpider):
    name = "movistartv"
    allowed_domains = ["movistar.com.pe"]
    start_urls = ["http://www.movistar.com.pe/hogar/tv/estelar", "http://www.movistar.com.pe/hogar/tv/estandar"]
    pipelines = ['TvCableTestPipeline']
    #rules =(Rule(LxmlLinkExtractor(allow=(),deny=(),restrict_xpaths=(), deny_extensions=()), callback='parse_fuckmtv', follow=False),)

    def parse(self, response):
        for sel in response.xpath('//tr'):
            try:
                item = ServiceTV()
                item['nombre_plan'] = response.xpath('//h1[@class="page-title"]/text()').extract()[0].encode('ascii','replace').replace('?','a')
                item['tipo_plan'] = "postpago"
                item['precio_plan'] = float(sel.xpath('td[@class="hide-mobile"][2]/h4/b/text()').extract()[0].replace('S/.',''))
                item['canal_nohd_plan'] = int(sel.xpath('td[2]/h4/text()').extract()[0].split(" ")[0])
                item['descrip_plan'] = response.xpath('//span[@class="atm-b2-subtitulo"]/text()').extract()[0].replace('\n','').replace('\t','')
                item['canal_hd_plan'] = 30
                item['audio_plan'] = 50
                item['radio_plan'] = 0
                item['costo_install'] = 180
                item['ciudad'] = sel.xpath('td[1]/h4/b/text()').extract()[0]
                item['nombre_plan'] = item['nombre_plan']+" "+item['ciudad']
                item['url'] = response.url
                item['image_path'] = "movistartv.png"
                
                #valores estaticos
                item['category'] = 3
                item['company'] = 1
                ##################
                yield item
            except:
                 pass
