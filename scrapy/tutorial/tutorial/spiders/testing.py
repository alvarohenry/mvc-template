__author__ = 'Carlos'
from scrapy.http import FormRequest
from tutorial.items import TryItem
from scrapy.spider import BaseSpider
import re

class myspiderSpider(BaseSpider):
    name = "clarinete"
    #start_urls = ['http://catalogo.claro.com.pe']

    def start_requests(self):
        return [FormRequest("http://catalogo.claro.com.pe/recibe-parametros.php?stock=SI",
                     formdata={'data1': '256', 'data2': 'acuerdo'},
                     callback=self.parse_ohmy)]

    def parse_ohmy(self,response):
        item = TryItem()
        item['title'] = response.body
        item['source_Website'] = response.url
        yield item
