# -*- coding: utf-8 -*-
import scrapy
from scrapy.contrib.linkextractors.lxmlhtml import LxmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from tutorial.items import ServiceTV


class DirectvSpider(CrawlSpider):
    name = 'directv'
    allowed_domains = ['directv.com.pe']
    start_urls = ['http://www.directv.com.pe/paquetes/ver-paquetes', 'http://www.directv.com.pe/paquetes/ver-paquetes/prepago']
    pipelines = ['TvCableTestPipeline']
    #rules =(Rule(LxmlLinkExtractor(allow=(),deny=(),restrict_xpaths=('.//article[@id]'), deny_extensions=()), callback='parse_fuckyou', follow=True),)
 
    def parse(self, response):
        i=0
        j=0
        for sel in response.xpath('//div[contains(@class,"Plan ")]/h3'):
            item = ServiceTV()
            item['nombre_plan'] = sel.xpath('text()').extract()[0]
            item['nombre_plan'] = "TV " + item['nombre_plan']  
            item['url'] = response.url
            if response.url.split('/')[-1] != 'prepago':
 
                item['precio_plan'] = int(response.xpath('//td[.="Tarifa Mensual"]/following-sibling::td/abbr/text()').extract()[i])
                item['tipo_plan'] = "postpago"
                item['canal_nohd_plan'] = int(filter(lambda a:a!='Acceso', response.xpath('//td[contains(.,"Video SD")]/following-sibling::td/abbr/text()').extract())[i].split()[-1])
                item['canal_hd_plan'] = int(response.xpath('//td[contains(.,"Video HD")]/following-sibling::td/abbr/text()').extract()[i].replace('-','0'))
                item['costo_install'] = 1
                item['audio_plan'] = int(response.xpath('//td[contains(.,"de Audio")]/following-sibling::td/abbr/text()').extract()[i])
                item['radio_plan'] = int(response.xpath('//td[contains(.,"de Radio")]/following-sibling::td/abbr/text()').extract()[i])
                deco_sd = response.xpath('//td[contains(.,"DIRECTV Digital")]/following-sibling::td/abbr//text()').extract()[i]
                deco_sd_plus = response.xpath('//td[contains(.,"DIRECTV Plus DVR")]/following-sibling::td/abbr//text()').extract()[i]
                deco_hd1 = response.xpath('//td[contains(.,"DIRECTV HD")][1]/following-sibling::td/abbr//text()').extract()
		deco_hd2 = response.xpath('//td[contains(.,"DIRECTV HD")][3]/following-sibling::td/abbr//text()').extract()
		deco_hd = (deco_hd1 + deco_hd2)[i]
                deco_hd_plus = response.xpath('//td[contains(.,"DIRECTV HD DVR")]/following-sibling::td/abbr//text()').extract()[i]
                i += 1
 
            else:
                item['precio_plan'] = int(response.xpath('//td[.="Recarga Mensual"]/following-sibling::td/abbr/text()').extract()[j])
                item['tipo_plan'] = "prepago"
                item['canal_nohd_plan'] = int(response.xpath('//td[contains(.,"Video SD")]/following-sibling::td/abbr/text()').extract()[j])
                item['canal_hd_plan'] = 0
                item['costo_install'] = response.xpath('//abbr[span[@class="sup"]="S/."]/text()').extract()[8]
                item['audio_plan'] = int(response.xpath('//td[contains(.,"de Audio")]/following-sibling::td/abbr/text()').extract()[j])
                item['radio_plan'] = int(response.xpath('//td[contains(.,"de Radio")]/following-sibling::td/abbr/text()').extract()[j])
                deco_sd = response.xpath('//td[contains(.,"DIRECTV Digital")]/following-sibling::td/abbr//text()').extract()[j]
                deco_sd_plus, deco_hd, deco_hd_plus = "-", "-", "-"
                j += 1
 
            item['descrip_plan'] = ""
            if deco_sd != '-':
                item['descrip_plan'] += deco_sd + " Deco basico en SD "
            if deco_sd_plus != '-':
                item['descrip_plan'] += deco_sd_plus + " Deco basico que graba y pausa la television "
            if deco_hd != '-':
                item['descrip_plan'] += deco_hd + " Deco HD "
            if deco_hd_plus != '-':
                item['descrip_plan'] += deco_hd_plus + " Deco HD que graba y pausa la television "
            item['ciudad'] = "Todas"
            item['image_path'] = "directv.png"

            #valores estaticos
            item['category'] = 3
            item['company'] = 5
            ##################
 
            yield item

