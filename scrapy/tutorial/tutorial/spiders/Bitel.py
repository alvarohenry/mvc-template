
# -*- coding: utf-8 -*-
import scrapy
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.lxmlhtml import LxmlLinkExtractor

from tutorial.items import MobileItem
import time

""""""""""""""""""""""""""""""""""""
"""  falta crawling de planes!  """
""""""""""""""""""""""""""""""""""""

class BitelSpider(CrawlSpider):
    name = 'Bitel'
    allowed_domains = ['bitel.com.pe']
    start_urls = ['http://bitel.com.pe/phone-device/celulares-basicos.html','http://bitel.com.pe/phone-device/smartphones.html']
    pipelines = ['MovilTestPipeline']
    rules =(Rule(LxmlLinkExtractor(allow=(), deny=(), restrict_xpaths=('.//h3[@class="name-product"]'), deny_extensions=()), callback='parse_fuckyou', follow=True),)
    def parse_fuckyou(self, response):
            item = MobileItem()
            time.sleep(1)
            item['product_name'] = response.xpath('//h3[@class="tab-title-orange"]/span/a/@title').extract()[0].replace("- ", "").title().replace("  ", " ")
            if response.url.split('/')[-2] == 'smartphones':
                item['product_price'] = [float(x.replace(".", " ").split()[1].replace(",","")) for x in response.xpath('//tbody[1]//td//text()').extract() if "S/." in x]
                #[int(float(x.encode("ascii","ignore").replace("\r","").replace("\n","").replace("\t","").replace("S/.","").replace(" ","").replace(",",""))) for x in response.xpath('//table/tbody[descendant::strong]/tr/td/p/descendant::text()').extract() if "S/." in x]
                #item['product_price'] = [int(float(x.encode("ascii","ignore").split()[1].replace(',',''))) for x in response.xpath('//div[@class="content-body"][1]/table[1]/tbody/tr/td[2]/p/text()[normalize-space()]').extract()]
                item['product_plan'] = [("MegaPlus"+" "+ x.split()[0]) for x in response.xpath('//tbody[1]//td//text()').extract() if "9.9C" in x]
                #[x.encode("ascii","ignore").replace("\r","").replace("\n","").replace("\t","").replace(" +","") for x in response.xpath('//table/tbody[descendant::strong]/tr/td/p/descendant::text()').extract() if "Mega" in x]
                #item['product_plan'] = " ".join(response.xpath('//div[@class="content-body"][1]/table[1]/tbody/tr/td[1]/descendant::*/text()[normalize-space()]').extract()[1:]).encode("ascii","ignore").replace("+", "").split()
                #item['product_plan'] = [x+" "+y for x,y in zip(item['product_plan'][::2], item['product_plan'][1::2])]
                item['product_prepago'] = item['product_price'][0]
                item['product_price'] = item['product_price'][1:]
                if len(item['product_price']) != len(item['product_plan']):
                    item['product_plan'] = ("-".join([str(x)+ "-"+str(x) for x in item['product_plan']])).split('-')
                item['product_plan_time'] = [int(x.split()[0]) for x in response.xpath('//tbody[1]//td//text()').extract() if "Meses" in x]
                item['product_plan_time']*=(len(item['product_plan'])/len(item['product_plan_time']))
            else:
                item['product_price'] = []
                item['product_plan'] = []
                item['product_prepago'] = [float(x.replace("S/.","")) for x in response.xpath('//tbody[1]//td//text()').extract() if "S/." in x]
                item['product_prepago'] = item['product_prepago'][0]
                item['product_plan_time'] = []

            item['product_image_path'] = item['product_name'].upper().replace(" ","-") + ".png"
            item['product_descrip'] = ""
            item['product_url'] = response.url

            #valores estaticos
            item['product_category'] = 5
            item['product_company'] = 3
            ##################

            yield item
