# -*- coding: utf-8 -*-
import scrapy
#from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.contrib.spiders import CrawlSpider

from tutorial.items import ServiceTV


class CableStarSpider(CrawlSpider):
    name = 'cablestarspider'
    allowed_domains = ['star.com.pe']
    pipelines = ['TvCableTestPipeline']
    start_urls = ['http://www.star.com.pe/arequipa/cablestar_tarifas.php','http://www.star.com.pe/tacna/cablestar_tarifas.php']
 
 
    def parse(self, response):
        for sel in response.xpath('//tr[contains(.,"Servicio Cable")][1]/following-sibling::tr'):
            item = ServiceTV()
            item['nombre_plan'] = sel.xpath('td/text()').extract()[0].encode("ascii","replace").replace("?","a")
            item['precio_plan'] = float(sel.xpath('td/text()').extract()[1].replace("S/. ",""))
            item['url'] = response.url
            item['tipo_plan'] = "postpago"
            """
                if "Estelar HD" in item['nombre_plan']:
                    item['precio_plan'] += 96
                elif "HD" in item['nombre_plan']:
                    item['precio_plan'] += 74
                item['tipo_plan'] = "postpago"

                item['canal_hd_plan'] = sel.xpath('following-sibling::td[2]/text()').extract()
                if item['canal_hd_plan'] != []:
                    item['canal_hd_plan'] = int(item['canal_hd_plan'][0])
                else:
                    item['canal_hd_plan'] = 0
            """
            if "Estelar" in item['nombre_plan']:
                item['canal_nohd_plan'] = 91
            else:
                item['canal_nohd_plan'] = 61
            item['canal_hd_plan'] = 0
            item['audio_plan'] = 0
            item['radio_plan'] = 0
            item['costo_install'] = 0
            item['descrip_plan'] = ""
            item['ciudad'] = response.url.split('/')[-2].capitalize()
            item['nombre_plan'] = "TV " + item['nombre_plan'] + " " + item['ciudad']
            item['image_path'] = "starglobal.png"

            #valores estaticos
            item['category'] = 3
            item['company'] = 6
            ##################

            yield item
