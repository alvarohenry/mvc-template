# -*- coding: utf-8 -*-
import scrapy
import unicodedata
from scrapy.contrib.linkextractors.lxmlhtml import LxmlLinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector
from scrapy.http import Request
from tutorial.items import MobileItem
from tutorial.items import ServiceTV
from tutorial.items import PlanMobileItem
from selenium import webdriver
from scrapy.http import Request
from selenium.webdriver.common.by import By
import selenium
from scrapy.selector import Selector
import time

import w3lib.url

#class Itemurl(scrapy.Item):
#    url_item = scrapy.Field()


class ClarotvSpider(CrawlSpider):
    name = 'clarotv'
    allowed_domains = ['claro.com.pe']
    start_urls = ['http://www.claro.com.pe/wps/portal/pe/sc/personas/tv']
    pipelines = ['TvCableTestPipeline']
    rules =(Rule(LxmlLinkExtractor(allow=('/claro-tv'),deny=(), restrict_xpaths=('//a[@href]'), deny_extensions=()), callback='parse_item', follow=True),)

    def parse_item(self, response):
        if 'planes' in response.url:
            return self.parse_clarin(response)
        return None


    def parse_clarin(self,response):
        item = ServiceTV()
        item['nombre_plan'] = unicodedata.normalize('NFKD', response.xpath('//div[@class="g6 left"]/h1/text()').extract()[0]).encode('ascii', 'ignore')
        item['nombre_plan'] = "TV " + item['nombre_plan']
        item['precio_plan'] = int(float(response.xpath('//span/text()')[2].extract().replace('S/. ','')))
        item['tipo_plan'] = "postpago"
        item['url'] = response.url
        item['canal_nohd_plan'] = response.xpath('//div[contains(span,"en SD")]/preceding-sibling::div/span/text()').extract()[0].replace("canales", "")
        item['canal_hd_plan'] = response.xpath('//div[contains(span,"en HD")]/preceding-sibling::div/span/text()').extract()
        if item['canal_hd_plan'] == ["-"] or item['canal_hd_plan'] == []:
            item['canal_hd_plan'] = 0
	else:
	    item['canal_hd_plan'] = item['canal_hd_plan'][0].replace("canales", "")
        item['audio_plan'] = response.xpath('//div[contains(span,"de audio")]/preceding-sibling::div/span/text()').extract()
        if item['audio_plan'] == ["-"] or item['audio_plan'] == []:
            item['audio_plan'] = 0
	else:
	    item['audio_plan'] = item['audio_plan'][0].replace("canales", "")
        """
            if item['nombre_plan'] == "Full HD Digital":
                item['canal_nohd_plan'] = int(response.xpath('//span/text()')[4].extract()[:response.xpath('//span/text()')[4].extract().index('c')].replace(" ",""))
            elif response.xpath('//span/text()')[5].extract() == "en SD ":
                item['canal_nohd_plan'] = int(response.xpath('//span/text()')[4].extract().split()[0])
            else:
                item['canal_nohd_plan'] = int(response.xpath('//span/text()')[3].extract().split()[0])
            if (response.xpath('//span/text()')[7].extract() == "en HD " or response.xpath('//span/text()')[9].extract() == "en HD " )and item['nombre_plan'] != "PLUS":
                item['canal_hd_plan'] = int(response.xpath('//span/text()')[response.xpath('//span/text()').extract().index("en HD ")-1].extract().split()[0])
            else:
               item['canal_hd_plan'] = 0
            if response.xpath('//span/text()')[9].extract() == "de audio ":
                item['audio_plan'] = int(response.xpath('//span/text()')[8].extract().split()[0].replace('-',''))
            else:
                item['audio_plan'] = 0
        """
        item['radio_plan'] = 0
        item['costo_install'] = 198.32
        item['descrip_plan'] = unicodedata.normalize('NFKD', " ".join(response.xpath('//div[contains(p," ")]/p[1]/text()').extract())).encode('ascii','ignore').replace(" International visitors","")
        item['ciudad'] = "Todas"
        item['image_path'] = "clarotv.png"

        #valores estaticos
        item['category'] = 3
        item['company'] = 2
        ##################

        yield item

class ClarotvPaquetesSpider(CrawlSpider):
    name = 'clarotvpaq'
    allowed_domains = ['http://www.claro.com.pe/wps/portal/pe/sc/personas/tv']
    start_urls = ['http://www.claro.com.pe/wps/portal/pe/sc/personas/tv/paquetes-premium', 'http://www.claro.com.pe/wps/portal/pe/sc/personas/tv/premium-sat']

    #rules =(Rule(LxmlLinkExtractor(allow=('/claro-tv'),deny=(), restrict_xpaths=('//a[@href]'), deny_extensions=()), callback='parse_item', follow=True),)
    rules = (Rule(SgmlLinkExtractor(allow=('\.html', )), callback='parse_page',follow=True),)

    def __init__(self):
        self.driver = webdriver.Firefox()

    def parse(self, response):

        self.driver.get(response.url)

        self.driver.close()
        return items

class ClaroCelSpider(CrawlSpider):
    name = "clarineteps"
    #start_urls = ['http://www.tiendaclaroperu.com/2012/06/smartphone-de-claro.html']
    start_urls = ['http://www.tiendaclaroperu.com/2012/02/equipos-celuares-iso-iphone-de-claro.html','http://www.tiendaclaroperu.com/2012/02/equipos-blackberry-de-claro.html','http://www.tiendaclaroperu.com/2012/04/equipos-htc-de-claro.html','http://www.tiendaclaroperu.com/2012/02/equipos-lg-de-claro.html','http://www.tiendaclaroperu.com/2015/03/equipos-microsoft.html','http://www.tiendaclaroperu.com/2012/02/equipos-motorola-de-claro.html','http://www.tiendaclaroperu.com/2012/02/equipos-nokia-symbian-de-claro.html','http://www.tiendaclaroperu.com/2012/02/equipos-samsung-de-claro.html','http://www.tiendaclaroperu.com/2012/02/equipos-samsung-de-claro.html','http://www.tiendaclaroperu.com/2012/02/equipos-sony-ericssion-de-claro.html','http://www.tiendaclaroperu.com/2012/02/equipos-alcatel-de-claro.html','http://www.tiendaclaroperu.com/2014/05/celulares-smartphone-azumi-claro.html','http://www.tiendaclaroperu.com/2012/04/equipos-huawei-de-claro.html','http://www.tiendaclaroperu.com/2014/05/celulares-smartphone-lanix-claro-peru.html','http://www.tiendaclaroperu.com/2014/07/equipos-levono-claro-peru.html','http://www.tiendaclaroperu.com/2015/01/m4tel-claro.html','http://www.tiendaclaroperu.com/2015/01/verykool-claro.html','http://www.tiendaclaroperu.com/2012/11/equipos-zte-de-claro.html']
    pipelines = ['MovilTestPipeline']
    #rules =(Rule(LxmlLinkExtractor(allow=(),deny=(), restrict_xpaths=('//dl[contains(.,"Marcas")]/dd/a'), deny_extensions=()), callback='parse_ohmy', follow=True),)


    """def __init__(self):
        self.driver = webdriver.Firefox()
    """

    def parse_item(self, response):
        item = MobileItem()
        item['product_name'] = str(response.xpath('//h1[@itemprop="name"]/text()').extract()[0].split(":")[0]).replace(" 4G","").replace(" LTE","").title().replace("  ", " ")
        #item['product_prepago'] = "incluido en planes"
        #response.xpath('//div[@id="pregado"]/div/table/tbody/tr[1]/td/text()').extract()
        item['product_price'] = [float(x.replace("S/.","")) for x in response.xpath('//div[@id="pnuevo"]//table[@class="t-precios"]/tbody/tr[1]/td/text()').extract()]
        item['product_prepago'] = item['product_price'][-1]
        item['product_plan'] = [unicodedata.normalize('NFKD', x).encode('ascii','ignore') for x in response.xpath('//div[@id="pnuevo"]//div[@class="precio-tittle"]/h4/a/text()').extract()]
        #item['product_category'] = 3
        item['product_image_path'] = item['product_name'].replace(" ","-") + ".png"
        item['product_descrip'] = "nodata"
        #unicodedata.normalize('NFKD', " ".join(response.xpath('//div[@id="caracteristicas"]/ul/li/div/span/text()').extract())).encode('ascii','ignore')
        #if "propio" in item['product_descrip'].lower():
        #    item['product_category'] = 4
        item['product_url'] = response.url
        item['product_plan_time'] = [18] * (len(item['product_plan'])-1)
        item['product_plan_time'].append(0)
        #item['product_name'] = response.xpath('//h1[@itemprop="name"]/text()').extract()[0].split(":")[0]
        #.replace(" ","+"))
        """calling = "https://www.google.com.pe/#q=" + name_item + "+site:smart-gsm.com"
        self.driver.get(calling)
        time.sleep(5)
        try:
            item['product_name'] = Selector(text=self.driver.page_source).xpath('//div[@class="srg"]/li[1]/div/h3/a/text()').extract()[0].split(":")[0]
        except:
            item['product_name'] = name_item.replace("+", " ")
            print("test", file =self.log)
            pass
        #[u for u in self.driver.find_element_by_xpath('//div[@class="srg"]/li[1]/div/h3/a')]
        #Selector(text=self.driver.page_source).xpath('//cite[@class="_Rm"][1]//text()').extract()
        """

        #valores estaticos
        item['product_category'] = 5
        item['product_company'] = 2
        ##################

        yield item
        #new_request = Request(url=calling, meta={'item': item}, callback=self.parse_match)
        #yield new_request

    def parse(self,response):
        lista_total= response.xpath('//div[contains(@id,"si-dispo")]/following-sibling::article/div/div[2]/a/@href').extract()
        lista_no_dispo = response.xpath('//div[contains(@id,"no-dispo")]/following-sibling::article/div/div[2]/a/@href').extract()
        for borrar in lista_no_dispo:
            lista_total.remove(borrar)
        for item_found in lista_total:
            yield Request(url=str(item_found), callback=self.parse_item)
    """
    def spider_close(self):
        self.driver.close()

    
    def parse_match(self,response):
        self.initilizatione()
        self.driver.get(response.url)
        time.sleep(5)
        item = response.meta['item']
        item['product_name'] = "".join(Selector(text=self.driver.page_source).xpath('//cite[@class="_Rm"][1]//text()').extract())
        yield item

    def parse_presearch(self,response,name_item):
        return


    def parse_search(self,response):
        return " ".join(" ".join(response.xpath('//div[@class="paddingLeft100 productInfoBox"][1]/h3//text()').extract()).split())

    def parse_page1(self, response):
        item = MyItem()
        item['main_url'] = response.url
        request = Request("http://www.example.com/some_page.html", callback=self.parse_page2)
        request.meta['item'] = item
        return request


    def parse_page2(self, response):
        item = response.meta['item']
        item['other_url'] = response.url
        return item

    def parse_nofk(self):
        return [FormRequest("http://catalogo.claro.com.pe/recibe-parametros.php?stock=SI",
                     formdata={'data1': '256', 'data2': 'acuerdo'},
                     callback=self.parse_ohmy)]
    """

class ClaroPlanSpider(CrawlSpider):
    name = 'claroplan'
    allowed_domains = ['claro.com.pe']
    start_urls = ['http://www.claro.com.pe/wps/portal/pe/sc/personas/movil/postpago?utm_source=movil%20home%20personas&utm_medium=banner&utm_campaign=planes%20postpago#info-03']
    pipelines = ['MovilPlanTestPipeline']

    def parse(self, response):
        for selec in response.xpath('//div[@class="plan-element g3"]'):
            item = PlanMobileItem()
            item['plan_Name'] = unicodedata.normalize('NFKD', selec.xpath('div/div[@class="plan-head"]/span/text()').extract()[0]).encode('ascii', 'ignore')
            item['plan_PriceTag'] = float(selec.xpath('div/div[@class="plan-price-action"]/div[@class="plan-price"]/span/text()').extract()[0].replace("S/. ",""))
            item['plan_MinutosRed'] = selec.xpath('div//span[contains(text(),"RPC")]/preceding-sibling::span/text()').extract()
            item['plan_Minutos'] = selec.xpath('div//span[contains(text(),"Minutos")]/preceding-sibling::span/text()').extract()
            item['plan_Internet'] = sum([int(x.replace(" MB","")) for x in selec.xpath('div//span[contains(text(),"nternet")]/preceding-sibling::span/text()').extract() if x != '-'])
            item['plan_Soles'] = [float(x.replace("S/. ","").replace(" SMS","")) for x in selec.xpath('div//span[contains(text(),"soles")]/preceding-sibling::span/text()').extract()]
            item['plan_SMS'] = selec.xpath('div//span[contains(text(),"SMS")]/preceding-sibling::span/text()').extract()
            if item['plan_Soles'] == []:
                item['plan_Soles'] = 0
            else:
                item['plan_Soles']=item['plan_Soles'][0]
            if "Plus" in item['plan_Name'] and "Conexion" in item['plan_Name']:
                item['plan_SMS'] = ['500']
            if item['plan_SMS'] == []:
                item['plan_SMS'] = selec.xpath('div//span[contains(text(),"mensajes de")]/preceding-sibling::span/text()').extract()
            if "iPlus" in item['plan_Name']:
                item['plan_Minutos'] = ['300']
            if "RPC" in item['plan_Name']:
                item['plan_MinutosRed'] = ["ilimitado"]
            item['plan_MinutosRed'] = item['plan_MinutosRed'][0]
            item['plan_SMS'] = item['plan_SMS'][0]
            if "limitado" in item['plan_SMS']:
                item['plan_SMS'] = -1
            else:
                item['plan_SMS'] = int(float(item['plan_SMS'].replace(" SMS","").replace("S/. ","")))
            if item['plan_Minutos'] != []:
                item['plan_Minutos'] = item['plan_Minutos'][0]
                if "limitado" in item['plan_Minutos']:
                    item['plan_Minutos'] = -1
                else:
                    item['plan_Minutos'] = int(item['plan_Minutos'])
            else:
                item['plan_Minutos'] = 0
            if "limitado" in item['plan_MinutosRed']:
                item['plan_MinutosRed'] = -1
            else:
                item['plan_MinutosRed'] = int(item['plan_MinutosRed'].replace("min","").replace(".",""))
            if item['plan_Name'] == "RPC 49 Ilimitado Plus" :
                item['plan_SMS'], item['plan_Soles'] = item['plan_Soles'], item['plan_SMS']
            item["plan_url"] = response.url

            #valores estaticos
            item['plan_image_path'] = "claro.png"
            item['plan_category'] = 4
            item['plan_company'] = 2
            ##################

            yield item
