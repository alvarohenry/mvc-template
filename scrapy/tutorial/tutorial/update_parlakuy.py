#! /usr/bin/python

import MySQLdb

#select_company = ("SELECT category_name, category_polarity FROM category")
#select_product = ("SELECT product_id FROM product")
select_product = ("SELECT DISTINCT product_id FROM parlakuy.reviewproduct")
select_polarityProd = ("SELECT count(CASE WHEN reviewProduct_polarity = 1 THEN 1 END), count(CASE WHEN reviewProduct_polarity = 2 THEN 1 END), count(CASE WHEN reviewProduct_polarity = 3 THEN 1 END) FROM reviewproduct WHERE product_id = %s")

select_service = ("SELECT DISTINCT service_id FROM parlakuy.reviewservice")
select_polarityServ = ("SELECT count(CASE WHEN reviewService_polarity = 1 THEN 1 END), count(CASE WHEN reviewService_polarity = 2 THEN 1 END), count(CASE WHEN reviewService_polarity = 3 THEN 1 END) FROM reviewservice WHERE service_id = %s")

select_maxProduct = ("SELECT product_id, category_id, product_positive FROM (SELECT * FROM product ORDER BY category_id, product_positive DESC) t GROUP BY category_id")
select_maxService = ("SELECT service_id, category_id, service_positive FROM (SELECT * FROM service ORDER BY category_id, service_positive DESC) t GROUP BY category_id")

up_product = ("UPDATE product SET product_positive = %s, product_negative = %s, product_neutro = %s WHERE product_id = %s")
up_service = ("UPDATE service SET service_positive = %s, service_negative = %s, service_neutro = %s WHERE service_id = %s")

up_category =("UPDATE category SET category_top_id = %s WHERE category_id = %s")

def update_product():
 
    conn = MySQLdb.connect(user='root', passwd='root', db='parlakuy', host='localhost', charset="utf8", use_unicode=True)
    cursor = conn.cursor()         # Crear un cursor 
 	
    cursor.execute(select_product)
    data = cursor.fetchall()

    for i in range(len(data)):
        cursor.execute(select_polarityProd, (data[i][0]))
        data_polarity = cursor.fetchall()
	
        data_product = (data_polarity[0][0], data_polarity[0][1], data_polarity[0][2], data[i][0])
        cursor.execute(up_product, data_product)
        #print(str(data_polarity[0][0]) + " - " + str(data_polarity[0][1]) + " - " + str(data_polarity[0][2]))

    conn.commit()
    cursor.close() 
    conn.close()

def update_service():
 
    conn = MySQLdb.connect(user='root', passwd='root', db='parlakuy', host='localhost', charset="utf8", use_unicode=True)
    cursor = conn.cursor()         # Crear un cursor 
 	
    cursor.execute(select_service)
    data = cursor.fetchall()

    for i in range(len(data)):
        cursor.execute(select_polarityServ, (data[i][0]))
        data_polarity = cursor.fetchall()
	
        data_service = (data_polarity[0][0], data_polarity[0][1], data_polarity[0][2], data[i][0])
        cursor.execute(up_service, data_service)
        #print(str(data_polarity[0][0]) + " - " + str(data_polarity[0][1]) + " - " + str(data_polarity[0][2]))

    conn.commit()
    cursor.close() 
    conn.close()

def update_topCategory():
    conn = MySQLdb.connect(user='root', passwd='root', db='parlakuy', host='localhost', charset="utf8", use_unicode=True)
    cursor = conn.cursor()         # Crear un cursor 
    
    cursor.execute(select_maxProduct)
    data = cursor.fetchall()
    
    for i in range(len(data)):

        data_topProd = (data[i][0], data[i][1])
        cursor.execute(up_category, data_topProd)
        #print(str(data[i][0]) + " - " + str(data[i][1]) + " - " + str(data[i][2]))
    
    cursor.execute(select_maxService)
    data = cursor.fetchall()
    
    for i in range(len(data)):

        data_topServ = (data[i][0], data[i][1])
        cursor.execute(up_category, data_topServ)
        #print(str(data[i][0]) + " - " + str(data[i][1]) + " - " + str(data[i][2]))

    conn.commit()
    cursor.close() 
    conn.close()

update_product()
update_service()
update_topCategory()
