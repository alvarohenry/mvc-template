# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import scrapy
#from scrapy.exceptions import DropItem
#from scrapy.contrib.pipeline.images import ImagesPipeline
import sys
import MySQLdb
import hashlib
from scrapy.exceptions import DropItem
from scrapy.http import Request


sql_product = ("INSERT INTO product (product_name, category_id, product_image_path, created_on) VALUES (%s,%s,%s,NOW())")
sql_service = ("INSERT INTO service (company_id, category_id, service_name, service_description, service_price, service_tv_channel, service_tv_extra, service_image_path, service_url, created_on) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,NOW())")
sql_service_plan = ("INSERT INTO service (company_id, category_id, service_name, service_description, service_price, service_phone_mb, service_phone_min_all, service_phone_extra, service_image_path, service_url, created_on) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,NOW())")
sql_company_has_product = ("INSERT INTO company_has_product (product_id, company_id, company_has_product_price, company_has_product_url, created_on) VALUES (%s,%s,%s,%s,NOW())")
sql_service_has_product = ("INSERT INTO service_has_product (product_id, service_id, service_has_product_num, service_has_product_price, service_has_product_url, created_on) VALUES (%s,%s,%s,%s,%s,NOW())")

update_service = ("UPDATE service SET service_description = %s, service_price = %s, service_tv_channel = %s, service_tv_extra = %s, service_image_path = %s, service_url = %s WHERE service_id = %s")
update_service_plan = ("UPDATE service SET service_description = %s, service_price = %s, service_phone_mb = %s, service_phone_min_all = %s, service_phone_extra = %s, service_image_path = %s, service_url = %s WHERE service_id = %s")

select_product = ("SELECT product_id FROM product WHERE product_name = %s")
select_service = ("SELECT service_id FROM service WHERE service_name = %s")
select_company_has_product = ("SELECT * FROM company_has_product WHERE product_id = %s AND company_id = %s")
select_service_has_product = ("SELECT * FROM service_has_product WHERE service_id = %s AND product_id = %s AND service_has_product_num = %s")


class MovilTestPipeline(object):
    def __init__(self):
        self.conn = MySQLdb.connect(user='root', passwd='root', db='parlakuy', host='localhost', charset="utf8", use_unicode=True)
        self.cursor = self.conn.cursor()

    def process_item(self, item, spider):
        if 'MovilTestPipeline' not in getattr(spider, 'pipelines'):
            return item
        try:
            insertChasP = True
            findShasP = True
            lastId_prod = 0

            self.cursor.execute(select_product,(item['product_name']))
            data = self.cursor.fetchone()

            if self.cursor.rowcount > 0:
                lastId_prod = data[0]
                self.cursor.execute(select_company_has_product,(lastId_prod,item['product_company']))
                data = self.cursor.fetchone()
                """findShasP = False"""
                if self.cursor.rowcount > 0:
                    insertChasP = False
            else:
                data_product = (item['product_name'],item['product_category'],item['product_image_path'])
                self.cursor.execute(sql_product,data_product)
                lastId_prod = self.cursor.lastrowid

            if insertChasP:
                data_company_has_product = (lastId_prod,item['product_company'],item['product_prepago'],item['product_url'])

                self.cursor.execute(sql_company_has_product,data_company_has_product)

            for i in range(len(item["product_plan"])):
                insertShasP = True
                self.cursor.execute(select_service,(item["product_plan"][i]))
                data = self.cursor.fetchone()
                if self.cursor.rowcount > 0:
                    lastId_serv = data[0]
                    if findShasP:
                        self.cursor.execute(select_service_has_product,(lastId_serv, lastId_prod, item["product_plan_time"][i]))
                        data = self.cursor.fetchone()
                        if self.cursor.rowcount > 0:
                            insertShasP = False
                    if insertShasP:
                        data_service_has_product = (lastId_prod, lastId_serv, item["product_plan_time"][i], item['product_price'][i], item['product_url'])
                        self.cursor.execute(sql_service_has_product, data_service_has_product)

            self.conn.commit()

        except MySQLdb.Error, e:
            print "Error %d: %s" % (e.args[0], e.args[1])
        return item


class MovilPlanTestPipeline(object):
    def __init__(self):
        self.conn = MySQLdb.connect(user='root', passwd='root', db='parlakuy', host='localhost', charset="utf8", use_unicode=True)
        self.cursor = self.conn.cursor()

    def process_item(self, item, spider):
        if 'MovilPlanTestPipeline' not in getattr(spider, 'pipelines'):
            return item
        try:
            self.cursor.execute(select_service,(item['plan_Name']))
            data = self.cursor.fetchone()

            if self.cursor.rowcount == 0:
                data_service = (item['plan_company'], item['plan_category'], item['plan_Name'], "", item['plan_PriceTag'], item["plan_Internet"], item["plan_Minutos"], "", item["plan_image_path"], item['plan_url'])

                self.cursor.execute(sql_service_plan, data_service)
            else:
                data_service = ("", item['plan_PriceTag'], item["plan_Internet"], item["plan_Minutos"], "", item["plan_image_path"], item['plan_url'], data[0])

                self.cursor.execute(update_service_plan, data_service)  
            self.conn.commit()

        except MySQLdb.Error, e:
            print "Error %d: %s" % (e.args[0], e.args[1])
        return item


class TvCableTestPipeline(object):
    def __init__(self):
        self.conn = MySQLdb.connect(user='root', passwd='root', db='parlakuy', host='localhost', charset="utf8", use_unicode=True)
        self.cursor = self.conn.cursor()

    def process_item(self, item, spider):
        if 'TvCableTestPipeline' not in getattr(spider, 'pipelines'):
            return item
        try:

            self.cursor.execute(select_service,(item['nombre_plan']))
            data = self.cursor.fetchone()

            if self.cursor.rowcount == 0:
                data_service = (item['company'], item['category'], item['nombre_plan'], item['tipo_plan']+", "+item['descrip_plan'], item['precio_plan'], "sd:"+str(item['canal_nohd_plan'])+", hd:"+str(item['canal_hd_plan'])+", audio:"+str(item['audio_plan'])+", radio:"+str(item['radio_plan']), "ciudad:"+item['ciudad'] +", costo instalacion:"+str(item['costo_install']), item['image_path'], item['url'])

                self.cursor.execute(sql_service, data_service)
            else:
                data_service = (item['tipo_plan']+", "+item['descrip_plan'], item['precio_plan'], "sd:"+str(item['canal_nohd_plan'])+", hd:"+str(item['canal_hd_plan'])+", audio:"+str(item['audio_plan'])+", radio:"+str(item['radio_plan']), "ciudad:"+item['ciudad'] +", costo instalacion:"+str(item['costo_install']), item['image_path'], item['url'], data[0])

                self.cursor.execute(update_service, data_service)
            self.conn.commit()

        except MySQLdb.Error, e:
            print "Error %d: %s" % (e.args[0], e.args[1])
        return item
