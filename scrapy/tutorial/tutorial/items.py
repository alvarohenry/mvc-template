# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class TryItem(scrapy.Item):
    title = scrapy.Field()
    source_Website = scrapy.Field()


class MovistarImage(scrapy.Item):
    image_name = scrapy.Field()
    image_urls = scrapy.Field()
    #image_paths = scrapy.Field()


class MobileItem(scrapy.Item):
    product_name = scrapy.Field()
    product_prepago = scrapy.Field()
    product_price = scrapy.Field()
    product_plan = scrapy.Field()
    product_category = scrapy.Field()
    product_image_path = scrapy.Field()
    product_descrip = scrapy.Field()
    product_url = scrapy.Field()
    product_company = scrapy.Field()
    product_plan_time = scrapy.Field()

class PlanMobileItem(scrapy.Item):
    plan_Name = scrapy.Field()
    plan_PriceTag = scrapy.Field()
    plan_MinutosRed = scrapy.Field()
    plan_Minutos = scrapy.Field()
    plan_Internet = scrapy.Field()
    plan_SMS = scrapy.Field()
    plan_Soles = scrapy.Field()
    plan_category = scrapy.Field()
    plan_company = scrapy.Field()
    plan_url = scrapy.Field()
    plan_image_path = scrapy.Field()

class MovistarCarac(scrapy.Item):
    nameProduct = scrapy.Field()
    Category_idCategory = scrapy.Field()
    brandProduct = scrapy.Field()
    operatingSystemProduct = scrapy.Field()
    dimensionProduct = scrapy.Field()
    bandProduct = scrapy.Field()
    imagePathProduct = scrapy.Field()
    descriptionProduct = scrapy.Field()
    batteryProduct = scrapy.Field()
    bluetoothProduct = scrapy.Field()
    cameraProduct = scrapy.Field()
    dualSimProduct = scrapy.Field()
    gpsProduct = scrapy.Field()
    memoryProduct = scrapy.Field()
    ramProduct = scrapy.Field()
    expandMemoryProduct = scrapy.Field()
    screenProduct = scrapy.Field()
    processorProduct = scrapy.Field()
    weightProduct = scrapy.Field()
    wifiProduct = scrapy.Field()

class ServiceTV(scrapy.Item):
    nombre_plan = scrapy.Field()
    precio_plan = scrapy.Field()
    tipo_plan = scrapy.Field()
    descrip_plan = scrapy.Field()
    canal_nohd_plan = scrapy.Field()
    canal_hd_plan = scrapy.Field()
    audio_plan = scrapy.Field()
    radio_plan = scrapy.Field()
    costo_install = scrapy.Field()
    ciudad = scrapy.Field()
    url = scrapy.Field()
    category = scrapy.Field()
    company = scrapy.Field()
    image_path = scrapy.Field()
