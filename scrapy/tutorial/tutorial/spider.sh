#!/bin/bash

cd /home/julio/project/mvc-template/scrapy/tutorial/tutorial/spiders/
PATH=$PATH:/usr/local/bin
export PATH

scrapy crawl cablestarspider
scrapy crawl directv
scrapy crawl clarotv
scrapy crawl movistartv

scrapy crawl claroplan

scrapy crawl Bitel
scrapy crawl clarineteps
