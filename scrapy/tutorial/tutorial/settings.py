# -*- coding: utf-8 -*-

# Scrapy settings for tutorial project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#
#import sys
#import MySQLdb

BOT_NAME = 'tutorial'

SPIDER_MODULES = ['tutorial.spiders']
NEWSPIDER_MODULE = 'tutorial.spiders'

#ITEM_PIPELINES = {'scrapy.contrib.pipeline.images.ImagesPipeline': 1}
#IMAGES_STORE = '/Scrapy/tutorial/imagenes'
#ITEM_PIPELINES = {'tutorial.pipelines.MovilPipeline':300}
#ITEM_PIPELINES = {'tutorial.pipelines.TvCablePipeline':300}
ITEM_PIPELINES = {'tutorial.pipelines.MovilTestPipeline':300, 'tutorial.pipelines.TvCableTestPipeline':300, 'tutorial.pipelines.MovilPlanTestPipeline':300}

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'tutorial (+http://www.yourdomain.com)'
