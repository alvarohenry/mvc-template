package com.leitmotiff.persistence.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import junit.framework.Assert;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.leitmotiff.persistence.entity.Category;
import com.leitmotiff.persistence.entity.Company;
import com.leitmotiff.persistence.entity.Product;
import com.leitmotiff.persistence.entity.Reviewproduct;
import com.leitmotiff.persistence.entity.Source;
import com.leitmotiff.persistence.entity.User;


@ContextConfiguration(locations = {"classpath:test-persistence-context.xml"})
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class ReviewProductDaoTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	private ReviewProductDao reviewProductDao;
	
	@Autowired
	private CategoryDao categoryDao;
	
	@Autowired
	private CompanyDao companyDao;
	
	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private SourceDao sourceDao;
	
	@Autowired
	private UserDao userDao;
	
	@Before
	public void setUp(){
		Category category = new Category(1L, "category", "imagePath",1L);
		categoryDao.saveOrUpdate(category);
		Company company = new Company(1L, "company", "description", "shortname", "Url");
		companyDao.saveOrUpdate(company);
		Product product = new Product(1L, category, "product", 100L, 30L);
		productDao.saveOrUpdate(product);
		Source source = new Source(1L);
		sourceDao.saveOrUpdate(source);
		Reviewproduct review = new Reviewproduct(1L, product, source, "Title");
		review.setReviewProductPolarity(1);
		User user = new User(1L,"user","fb","email","pass");
		Calendar calendar = new GregorianCalendar(1996,10,10);
		review.setReviewProductDate(calendar);
		userDao.saveOrUpdate(user);
		review.setUser(user);
		reviewProductDao.saveOrUpdate(review);
		
		
	}
	
	@Test
	public void testGetByCriteria(){
		Criterion criterion = Restrictions.eq("product.productId", 1L);
		List<Reviewproduct> list = reviewProductDao.findByCriteria(criterion);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd");
		assertEquals("Title",list.get(0).getReviewProductTitle());
		assertEquals("1996 nov 10",sdf.format(list.get(0).getReviewProductDate().getTime()));
	}	
	
	@Test
	public void testNumberReviews(){
		List<Integer> result = reviewProductDao.numberComentary(1L);
		if(result.get(0) != 1){
			fail();
		}
		if(result.get(1) != 1){
			fail();
		}
		if(result.get(2) != 0){
			fail();
		}
		if(result.get(3) != 0){
			fail();
		}
	}
}
