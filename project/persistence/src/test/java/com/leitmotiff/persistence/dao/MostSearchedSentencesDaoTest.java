package com.leitmotiff.persistence.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.leitmotiff.persistence.entity.MostSearchedSentences;

@ContextConfiguration(locations = {"classpath:test-persistence-context.xml"})
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class MostSearchedSentencesDaoTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	private MostSearchedSentencesDao mostSearchedSentencesDao;
	
	@Test
	public void testGetMostSearchedSentences(){
		MostSearchedSentences sentence = new MostSearchedSentences(1L, "Sentencia Prueba", 17L);
		mostSearchedSentencesDao.saveOrUpdate(sentence);
		assertEquals("Sentencia Prueba",mostSearchedSentencesDao.getMostSearchedSentences("Prueba").get(0).getMostSearchedSentencesSentence());
	}
	
}
