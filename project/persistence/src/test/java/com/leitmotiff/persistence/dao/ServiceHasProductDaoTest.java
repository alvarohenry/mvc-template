package com.leitmotiff.persistence.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.leitmotiff.persistence.entity.Category;
import com.leitmotiff.persistence.entity.Company;
import com.leitmotiff.persistence.entity.Product;
import com.leitmotiff.persistence.entity.Services;
import com.leitmotiff.persistence.entity.ServiceHasProduct;
import com.leitmotiff.persistence.entity.ServiceHasProductId;

@ContextConfiguration(locations = { "classpath:test-persistence-context.xml" })
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class ServiceHasProductDaoTest extends AbstractTransactionalJUnit4SpringContextTests {
	
	@Autowired
	private ServiceHasProductDao hasDao;
	
	@Autowired
	private ServiceDao serviceDao;

	@Autowired
	private CategoryDao categoryDao;

	@Autowired
	private CompanyDao companyDao;
	
	@Autowired
	private ProductDao productDao;
	
	@Before
	public void setUp() {
		Category category = new Category(1L, "Category", "path", 1L);
		categoryDao.saveOrUpdate(category);
		Company company = new Company(1L, "company", "", "", "");
		companyDao.saveOrUpdate(company);
		
		Services service = new Services(1L, category, company, "service1", 200L, 10L, true);
		service.setServicePrice(20.9);
		serviceDao.saveOrUpdate(service);	    
		Services service2 = new Services(2L, category, company, "service2", 120L, 100L, true);
		service2.setServicePrice(100.0);
		serviceDao.saveOrUpdate(service2);
		Product product = new Product(1L, category, "ProductTest1", 100L, 50L);
	    productDao.saveOrUpdate(product);
		Product product2 = new Product(2L, category, "ProductTest2", 110L, 50L);
	    productDao.saveOrUpdate(product2);
	    Product product3 = new Product(3L, category, "ProductTest3", 110L, 100L);
	    productDao.saveOrUpdate(product3);
	    
	    ServiceHasProductId hasId = new ServiceHasProductId(service.getServiceId(), product.getProductId(), 12);
	    ServiceHasProduct has = new ServiceHasProduct(hasId, product, service,80.99, "", "", true);
	    hasDao.saveOrUpdate(has);
	    
		hasId = new ServiceHasProductId(service.getServiceId(), product2.getProductId(), 12);
		has = new ServiceHasProduct(hasId, product2, service, 220.50, "", "", true);
	    hasDao.saveOrUpdate(has);
	      
	    hasId = new ServiceHasProductId(service2.getServiceId(), product2.getProductId(), 18);
		has = new ServiceHasProduct(hasId, product2, service2, 200.0, "", "", true);
	    hasDao.saveOrUpdate(has);
		
	    hasId = new ServiceHasProductId(service.getServiceId(), product3.getProductId(), 18);
		has = new ServiceHasProduct(hasId, product3, service, 20.0, "", "", true);
	    hasDao.saveOrUpdate(has);
	    
	}

	@After
	public void tearDown(){
		
	}
	
	@Test 
	public void testGetMinurService(){
		ServiceHasProduct service = hasDao.getMinurService(2L);
		assertEquals("service2", service.getService().getServiceName());
	}
	
	@Test
	public void testGetServiceHasProductOrderPrice() {
		List<ServiceHasProduct> serHasProd = hasDao.getServiceHasProductOrderPrice("service", 2L, false);
		assertTrue(serHasProd.size() == 2);
		assertEquals("service2", serHasProd.get(0).getService().getServiceName());
		serHasProd = hasDao.getServiceHasProductOrderPrice("service", 3L, true);
		assertEquals("service1", serHasProd.get(0).getService().getServiceName());
		assertTrue(serHasProd.size() == 1);
		serHasProd = hasDao.getServiceHasProductOrderPrice("product", 1L, true);
		assertTrue(serHasProd.size() == 3);
		assertTrue(serHasProd.get(0).getServiceHasProductPrice() == 20.0);
		serHasProd = hasDao.getServiceHasProductOrderPrice("product", 2L, false);
		assertTrue(serHasProd.size() == 1);
		assertEquals("ProductTest2", serHasProd.get(0).getProduct().getProductName());
		serHasProd = hasDao.getServiceHasProductOrderPrice("asdas", 1L, true);
		assertTrue(serHasProd == null);
	}
	
	@Test
	public void testGetServiceHasProductOrderValue() {
		List<ServiceHasProduct> serHasProd = hasDao.getServiceHasProductOrderValue("service", 1L, false);
		assertTrue(serHasProd.size() == 1);
		assertTrue(serHasProd.get(0).getService().getServiceNegative() == 10L);
		serHasProd = hasDao.getServiceHasProductOrderValue("product", 1L, false);
		assertTrue(serHasProd.size() == 3);
		assertEquals("ProductTest2", serHasProd.get(0).getProduct().getProductName());
		serHasProd = hasDao.getServiceHasProductOrderValue("service", 2L, true);
		assertTrue(serHasProd.size() == 2);
		assertEquals("service2", serHasProd.get(0).getService().getServiceName());
		assertEquals("service1", serHasProd.get(1).getService().getServiceName());
		serHasProd = hasDao.getServiceHasProductOrderValue("product", 2L, true);
		assertTrue(serHasProd.size() == 1);
		serHasProd = hasDao.getServiceHasProductOrderPrice("product", 3L, true);
		assertTrue(serHasProd.size() == 0);
		serHasProd = hasDao.getServiceHasProductOrderPrice("asdas", 1L, false);
		assertTrue(serHasProd == null);
	}
	
	@Test
	public void testFindByCriteria() {
		Criterion criterion = Restrictions.eq("service.id", 1L);
		List<ServiceHasProduct> hases = hasDao.findByCriteria(criterion);
		assertTrue(hases.size() == 3);
		criterion = Restrictions.eq("product.id", 2L);
		hases = hasDao.findByCriteria(criterion);
		assertTrue(hases.size() == 2);
		assertEquals(18, hases.get(1).getId().getServiceHasProductNum());
	}
	
}
