package com.leitmotiff.persistence.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.leitmotiff.persistence.entity.Category;
import com.leitmotiff.persistence.entity.Company;
import com.leitmotiff.persistence.entity.Services;

@ContextConfiguration(locations = { "classpath:test-persistence-context.xml" })
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class ServiceDaoTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	private ServiceDao serviceDao;

	@Autowired
	private CategoryDao categoryDao;

	@Autowired
	private CompanyDao companyDao;

	@Before
	public void setUp() {
		Category category = new Category(1L, "Category", "path", 1L);
		categoryDao.saveOrUpdate(category);
		Company company = new Company(1L, "company", "", "", "");
		companyDao.saveOrUpdate(company);
		Services service = new Services(1L, category, company, "tv Test", 200L,
				10L, true);
		service.setServicePrice(20.9);
		serviceDao.saveOrUpdate(service);
		service = new Services(2L, category, company, "tv Test N2", 120L, 100L,
				true);
		service.setServicePrice(100.0);
		serviceDao.saveOrUpdate(service);
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testGetServiceOrderPrice() {
		List<Services> services = serviceDao.getServiceOrderPrice(1L, false);
		assertTrue(services.size() == 2);
		assertEquals("tv Test N2", services.get(0).getServiceName());
		services = serviceDao.getServiceOrderPrice(1L, true);
		assertEquals("tv Test", services.get(0).getServiceName());
	}

	@Test
	public void testGetProductOrderValue() {
		List<Services> services = serviceDao.getServiceOrderValue(1L, false);
		assertTrue(services.size() == 2);
		assertEquals("tv Test", services.get(0).getServiceName());
		services = serviceDao.getServiceOrderValue(1L, true);
		assertEquals("tv Test N2", services.get(0).getServiceName());
	}


	@Test
	public void testGetFindAll() {
		List<Services> services = serviceDao.findAll();
		assertTrue(services.size() == 2);
	}

	@Test
	public void testGetMore() {
		List<Services> services = serviceDao.findAll(1);
		services = serviceDao.getMore();
		assertEquals("tv Test N2", services.get(0).getServiceName());
		Criterion criterion = Restrictions.eq("category.categoryId", 1L);
		services = serviceDao.findByCriteria(criterion, 1);
		services = serviceDao.getMore();
		assertEquals("tv Test N2", services.get(0).getServiceName());
	}

	@Test
	public void testFindByCriteria() {
		Criterion criterion = Restrictions.eq("company.id", 1L);
		List<Services> services = serviceDao.findByCriteria(criterion);
		assertEquals("tv Test", services.get(0).getServiceName());
	}
	
	@Test
	public void testGetSearchedProduct_1_Init(){
		List<Services> resultList = serviceDao.getSearchedService_1_Init("tv Test");
		assertEquals("tv Test", resultList.get(0).getServiceName());
	}
	
	@Test
	public void testGetSearchedProduct_2_Tokenizer(){
		List<Services> resultList = serviceDao.getSearchedServices_2_Tokenizer("Test");
		if(!resultList.isEmpty()){
			fail();
		}
	}
	
	@Test
	public void testGetSearchedProduct_3_Count(){
		List<Services> resultList = serviceDao.getSearchedService_3_Count("tv N2");
		assertEquals("tv Test N2", resultList.get(0).getServiceName());
	}
	
	@Test
	public void testGetSearchedProduct_4_TokenizerInit(){
		List<Services> resultList = serviceDao.getSearchedService_4_TokenizerInit("Test tv");
		assertEquals("tv Test", resultList.get(0).getServiceName());
	}
	
	@Test
	public void testGetSearchedProduct_5_Contiene(){
		List<Services> resultList = serviceDao.getSearchedServices_5_Contiene("Test");
		assertEquals("tv Test", resultList.get(0).getServiceName());
	}
	
	@Test
	public void testGetSearchedProduct_6_(){
		List<Services> resultList = serviceDao.getSearchedProduct_6_("N2");
		assertEquals("tv Test N2", resultList.get(0).getServiceName());
	}

}
