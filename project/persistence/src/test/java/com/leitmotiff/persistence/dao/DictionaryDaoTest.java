package com.leitmotiff.persistence.dao;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.leitmotiff.persistence.entity.Category;
import com.leitmotiff.persistence.entity.Company;
import com.leitmotiff.persistence.entity.Dictionary;
import com.leitmotiff.persistence.entity.Product;

@ContextConfiguration(locations = {"classpath:test-persistence-context.xml"})
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class DictionaryDaoTest {

	@Autowired
	private DictionaryDao dictionary;
	
	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private CategoryDao categoryDao;
	
	@Autowired
	private CompanyDao companyDao;
	
	@Before
	public void setUp(){
		Category category = new Category(1L, "Category Prueba", "path", 1L);
		categoryDao.saveOrUpdate(category);
		Company company = new Company(1L, "Company Prueba", "Descripcion", "Company", "URL");
		companyDao.saveOrUpdate(company);
		Product product = new Product(1L, category,"Producto Prueba", 11L,13L);
	    product.setProductDescription("Descripcion");
		productDao.saveOrUpdate(product);
		
	}
	
	@Test
	public void testCreateDictionary(){
		dictionary.createDictionary();
		List<Dictionary> list = dictionary.findAll();
		assertEquals("category", list.get(0).getDictionaryWord());
	}
	
}
