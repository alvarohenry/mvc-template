package com.leitmotiff.persistence.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.leitmotiff.persistence.entity.Company;

@ContextConfiguration(locations = {"classpath:test-persistence-context.xml"})
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class CompanyDaoTest extends AbstractTransactionalJUnit4SpringContextTests {
	
	@Autowired
	private CompanyDao companyDao;
	
	@Before
	public void setUp(){
		Company com = new Company(1L, "company", "company description", "com", "www.company.com");
		companyDao.saveOrUpdate(com);
		
	}
	
	@Test	
	public void testGetAll() {
		List<Company> companies = companyDao.findAll();
		assertTrue(companies.size()==1);
    }
	
	@Test
	public void testGetById() {
		Company com = companyDao.getById(Long.parseLong("1"));
		assertEquals(com.getCompanyName(), "company");
	}
	
	@Test
	public void testSave() {
		List<Company> companies = companyDao.findAll();
		assertTrue(companies.size()==1);
	}
	
	@Test
	public void testUpdate() {
		Company cat = companyDao.getById(1L);
		assertEquals(cat.getCompanyName(), "company");
		String string = "Movi";
		cat.setCompanyName(string);
		companyDao.saveOrUpdate(cat);
		cat = companyDao.getById(1L);
		assertEquals(cat.getCompanyName(), string);
	}
}
