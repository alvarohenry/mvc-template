package com.leitmotiff.persistence.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.leitmotiff.persistence.entity.Category;

@ContextConfiguration(locations = {"classpath:test-persistence-context.xml"})
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class CategoryDaoTest extends AbstractTransactionalJUnit4SpringContextTests {
    
	@Autowired
	private CategoryDao categoryDao;
	
	@Before
	public void setUp(){
		Category cat = new Category(1L, "NewCat", "Path Cat",1L);
		categoryDao.saveOrUpdate(cat);
	}
	
	@Test	
	public void testGetAll() {
		List<Category> categories = categoryDao.findAll();
		assertTrue(categories.size()==1);
    }
	
	@Test
	public void testGetById() {
		Category cat = categoryDao.getById(1L);
		assertEquals(cat.getCategoryName(), "NewCat");
	}
	
	@Test
	public void testSave() {
		List<Category> categories = categoryDao.findAll();
		assertTrue(categories.size()==1);
	}
	
	@Test
	public void testUpdate() {
		Category cat = categoryDao.getById(1L);
		assertEquals(cat.getCategoryName(), "NewCat");
		String string = "Another Cat";
		cat.setCategoryName(string);
		categoryDao.saveOrUpdate(cat);
		cat = categoryDao.getById(1L);
		assertEquals(cat.getCategoryName(), string);
	}
}
