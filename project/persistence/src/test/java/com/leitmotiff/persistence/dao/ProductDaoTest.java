package com.leitmotiff.persistence.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.leitmotiff.persistence.entity.Category;
import com.leitmotiff.persistence.entity.Company;
import com.leitmotiff.persistence.entity.Product;


@ContextConfiguration(locations = {"classpath:test-persistence-context.xml"})
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class ProductDaoTest extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private CategoryDao categoryDao;
	
	@Autowired
	private CompanyDao companyDao;
	
	@Before
	public void setUp(){
	    Category category = new Category(1L, "Category", "path", 1L);
	    categoryDao.saveOrUpdate(category);
	    Company company = new Company(1L, "company", "", "", "");
	    companyDao.saveOrUpdate(company);
	    Product product = new Product(1L, category, "Product Test", 100L, 50L);
	    productDao.saveOrUpdate(product);
	    product = new Product(2L, category, "Product Test 2", 110L, 50L);
	    productDao.saveOrUpdate(product);
	    product = new Product(3L, category, "Product Test 3", 110L, 50L);
	    productDao.saveOrUpdate(product);
	}

	@After
	public void tearDown(){
	}
	
//	@Test
//	public void testGetProductsOrderPrice(){
//		List<Product> products = productDao.getProductsOrderPrice(1L, false);
//		assertTrue(products.size() == 2);
//		assertEquals("ProductTest", products.get(0).getProductName());
//		products = productDao.getProductsOrderPrice(1L, true);
//		assertTrue(products.size() == 2);
//		assertEquals("ProductTest2", products.get(0).getProductName());
//	}
	
	@Test
	public void testGetProductOrderValue(){
		List<Product> products = productDao.getProductOrderValue(1L, false);
		assertTrue(products.size() == 3);
		assertEquals("Product Test 2", products.get(0).getProductName());
		products = productDao.getProductOrderValue(1L, true);
		assertTrue(products.size() == 3);
		assertEquals("Product Test", products.get(0).getProductName());
	}
	
	@Test
	public void testFindAll(){
		List<Product> products = productDao.findAll(5);
		assertEquals("Product Test 2",products.get(1).getProductName());
	}
	
	@Test
	public void testGetMore(){
		List<Product> products = productDao.findAll(1);
		products = productDao.getMore();
		assertEquals("Product Test 2",products.get(0).getProductName());
		Category category = new Category();
		category.setCategoryId(1L);
		Criterion criterion = Restrictions.eq("category",category);
		products = productDao.findByCriteria(criterion, 1);
		products = productDao.getMore();
		assertEquals("Product Test 2",products.get(0).getProductName());
	}
	
	@Test
	public void testFindByCriteria(){
		Criterion criterion = Restrictions.eq("category.id",1L);
		List<Product> products = productDao.findByCriteria(criterion, 1);
		assertEquals("Product Test",products.get(0).getProductName());
	}
	
	@Test
	public void testGetSearchedProduct_1_Init(){
		List<Product> resultList = productDao.getSearchedProduct_1_Init("Product Test");
		assertEquals("Product Test", resultList.get(0).getProductName());
	}
	
	@Test
	public void testGetSearchedProduct_2_Tokenizer(){
		List<Product> resultList = productDao.getSearchedProduct_2_Tokenizer("Test");
		if(!resultList.isEmpty()){
			fail();
		}
	}
	
	@Test
	public void testGetSearchedProduct_3_Count(){
		List<Product> resultList = productDao.getSearchedProduct_3_Count("Product 2");
		assertEquals("Product Test 2", resultList.get(0).getProductName());
	}
	
	@Test
	public void testGetSearchedProduct_4_TokenizerInit(){
		List<Product> resultList = productDao.getSearchedProduct_4_TokenizerInit("Test Product");
		assertEquals("Product Test", resultList.get(0).getProductName());
	}
	
	@Test
	public void testGetSearchedProduct_5_Contiene(){
		List<Product> resultList = productDao.getSearchedProduct_5_Contiene("Test");
		assertEquals("Product Test", resultList.get(0).getProductName());
	}
	
	@Test
	public void testGetSearchedProduct_6_(){
		List<Product> resultList = productDao.getSearchedProduct_6_("3");
		assertEquals("Product Test 3", resultList.get(0).getProductName());
	}
}