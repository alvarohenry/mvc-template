package com.leitmotiff.persistence.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "reviewservice")
public class Reviewservice implements Serializable {

	private static final long serialVersionUID = 1L;

	private long reviewServiceId;
	
	private Services service;
	
	private Source source;
	
	private String reviewServiceTitle;
	
	private String reviewServiceBody;
	
	private Integer reviewServicePolarity;
	
	private Calendar reviewServiceDate;
	
	private String reviewServiceUrl;
	
	private User user;
	
	private Set<Sentenceservice> sentenceservices = new HashSet<Sentenceservice>(0);

	/** default constructor */
	public Reviewservice() { }

	/** minimal constructor */
	public Reviewservice(long reviewServiceId, Services service, Source source,
			String reviewServiceTitle) {
		this.reviewServiceId = reviewServiceId;
		this.service = service;
		this.source = source;
		this.reviewServiceTitle = reviewServiceTitle;
	}

	/** full constructor */
	public Reviewservice(long reviewServiceId, Services service, Source source,
			String reviewServiceTitle, String reviewServiceBody,
			Integer reviewServicePolarity, Set<Sentenceservice> sentenceservices) {
		this.reviewServiceId = reviewServiceId;
		this.service = service;
		this.source = source;
		this.reviewServiceTitle = reviewServiceTitle;
		this.reviewServiceBody = reviewServiceBody;
		this.reviewServicePolarity = reviewServicePolarity;
		this.sentenceservices = sentenceservices;
	}

	// Property accessors
	@Id
	@Column(name = "reviewService_id", unique = true, nullable = false, insertable = true, updatable = true)
	public long getReviewServiceId() {
		return this.reviewServiceId;
	}

	public void setReviewServiceId(long reviewServiceId) {
		this.reviewServiceId = reviewServiceId;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "service_id", unique = false, nullable = false, insertable = true, updatable = true)
	public Services getService() {
		return this.service;
	}

	public void setService(Services service) {
		this.service = service;
	}
	
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", unique = false, nullable = false, insertable = true, updatable = true)
	public User getUser(){
		return this.user;
	}
	
	public void setUser(User user){
		this.user = user;
	}
	

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "source_id", unique = false, nullable = false, insertable = true, updatable = true)
	public Source getSource() {
		return this.source;
	}

	public void setSource(Source source) {
		this.source = source;
	}

	@Column(name = "reviewService_title", unique = false, nullable = false, insertable = true, updatable = true, length = 600)
	public String getReviewServiceTitle() {
		return this.reviewServiceTitle;
	}

	public void setReviewServiceTitle(String reviewServiceTitle) {
		this.reviewServiceTitle = reviewServiceTitle;
	}

	@Column(name = "reviewService_body", unique = false, nullable = true, insertable = true, updatable = true, length = 65535)
	public String getReviewServiceBody() {
		return this.reviewServiceBody;
	}

	public void setReviewServiceBody(String reviewServiceBody) {
		this.reviewServiceBody = reviewServiceBody;
	}

	@Column(name = "reviewService_polarity", unique = false, nullable = true, insertable = true, updatable = true)
	public Integer getReviewServicePolarity() {
		return this.reviewServicePolarity;
	}

	public void setReviewServicePolarity(Integer reviewServicePolarity) {
		this.reviewServicePolarity = reviewServicePolarity;
	}
	
	@Column(name = "reviewService_date", unique = false, nullable = true, insertable = true, updatable = true)
	public Calendar getReviewServiceDate(){
		return this.reviewServiceDate;
	}
	
	public void setReviewServiceDate(Calendar date){
		this.reviewServiceDate = date;
	}
	
	@Column(name = "reviewService_url", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
	public String getReviewServiceUrl(){
		return this.reviewServiceUrl;
	}
	
	public void setReviewServiceUrl(String url){
		this.reviewServiceUrl = url;
	}
	
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "reviewservice")
	public Set<Sentenceservice> getSentenceservices() {
		return this.sentenceservices;
	}

	public void setSentenceservices(Set<Sentenceservice> sentenceservices) {
		this.sentenceservices = sentenceservices;
	}

}
