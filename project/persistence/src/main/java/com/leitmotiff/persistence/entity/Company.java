package com.leitmotiff.persistence.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "company")
public class Company implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long companyId;
	
	private String companyName;
	
	private String companyDescription;
	
	private String companyUrl;
	
	private String companyImagePath;
	
	private Set<CompanyHasProduct> companyHasProducts = new HashSet<CompanyHasProduct>(0);
	
	private Set<Services> services = new HashSet<Services>(0);

	/** default constructor */
	public Company() {
	}

	/** minimal constructor */
	public Company(long companyId, String companyName) {
		this.companyId = companyId;
		this.companyName = companyName;
	}

	public Company(long companyId, String companyName,
			String companyDescription, String companyImagePath,
			String companyUrl) {
		this.companyId = companyId;
		this.companyName = companyName;
		this.companyDescription = companyDescription;
		this.companyUrl = companyUrl;
		this.companyImagePath = companyImagePath;

	}

	/** full constructor */
	public Company(long companyId, String companyName,
			String companyDescription, String companyUrl,
			String companyImagePath, Set<CompanyHasProduct> companyHasProducts,
			Set<Services> services) {
		this.companyId = companyId;
		this.companyName = companyName;
		this.companyDescription = companyDescription;
		this.companyUrl = companyUrl;
		this.companyImagePath = companyImagePath;
		this.companyHasProducts = companyHasProducts;
		this.services = services;
	}

	// Property accessors
	@Id
	@Column(name = "company_id", unique = true, nullable = false, insertable = true, updatable = true)
	public long getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	@Column(name = "company_name", unique = false, nullable = false, insertable = true, updatable = true, length = 200)
	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Column(name = "company_description", unique = false, nullable = true, insertable = true, updatable = true, length = 65535)
	public String getCompanyDescription() {
		return this.companyDescription;
	}

	public void setCompanyDescription(String companyDescription) {
		this.companyDescription = companyDescription;
	}

	@Column(name = "company_url", unique = false, nullable = true, insertable = true, updatable = true, length = 200)
	public String getCompanyUrl() {
		return this.companyUrl;
	}

	public void setCompanyUrl(String companyUrl) {
		this.companyUrl = companyUrl;
	}

	@Column(name = "company_image_path", unique = false, nullable = true, insertable = true, updatable = true, length = 200)
	public String getCompanyImagePath() {
		return this.companyImagePath;
	}

	public void setCompanyImagePath(String companyImagePath) {
		this.companyImagePath = companyImagePath;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "company")
	public Set<CompanyHasProduct> getCompanyHasProducts() {
		return this.companyHasProducts;
	}

	public void setCompanyHasProducts(Set<CompanyHasProduct> companyHasProducts) {
		this.companyHasProducts = companyHasProducts;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "company")
	public Set<Services> getServices() {
		return this.services;
	}

	public void setServices(Set<Services> services) {
		this.services = services;
	}

}
