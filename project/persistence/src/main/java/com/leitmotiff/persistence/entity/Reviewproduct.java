package com.leitmotiff.persistence.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "reviewproduct")
public class Reviewproduct implements Serializable {

	private static final long serialVersionUID = 1L;

	private long reviewProductId;
	
	private Product product;
	
	private Source source;
	
	private String reviewProductTitle;
	
	private String reviewProductBody;
	
	private Integer reviewProductPolarity;
	
	private Calendar reviewProductDate;
	
	private String reviewProductUrl;
	
	private User user;
	
	
	
	private Set<Sentenceproduct> sentenceproducts = new HashSet<Sentenceproduct>(0);

	/** default constructor */
	public Reviewproduct() {
	}

	/** minimal constructor */
	public Reviewproduct(long reviewProductId, Product product, Source source,
			String reviewProductTitle) {
		this.reviewProductId = reviewProductId;
		this.product = product;
		this.source = source;
		this.reviewProductTitle = reviewProductTitle;
	}

	/** full constructor */
	public Reviewproduct(long reviewProductId, Product product, Source source,
			String reviewProductTitle, String reviewProductBody,
			Integer reviewProductPolarity, Set<Sentenceproduct> sentenceproducts) {
		this.reviewProductId = reviewProductId;
		this.product = product;
		this.source = source;
		this.reviewProductTitle = reviewProductTitle;
		this.reviewProductBody = reviewProductBody;
		this.reviewProductPolarity = reviewProductPolarity;
		this.sentenceproducts = sentenceproducts;
	}

	// Property accessors
	@Id
	@Column(name = "reviewProduct_id", unique = true, nullable = false, insertable = true, updatable = true)
	public long getReviewProductId() {
		return this.reviewProductId;
	}

	public void setReviewProductId(long reviewProductId) {
		this.reviewProductId = reviewProductId;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "product_id", unique = false, nullable = false, insertable = true, updatable = true)
	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", unique = false, nullable = false, insertable = true, updatable = true)
	public User getUser(){
		return this.user;
	}
	
	public void setUser(User user){
		this.user = user;
	}
	
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "source_id", unique = false, nullable = false, insertable = true, updatable = true)
	public Source getSource() {
		return this.source;
	}

	public void setSource(Source source) {
		this.source = source;
	}

	@Column(name = "reviewProduct_title", unique = false, nullable = false, insertable = true, updatable = true, length = 600)
	public String getReviewProductTitle() {
		return this.reviewProductTitle;
	}

	public void setReviewProductTitle(String reviewProductTitle) {
		this.reviewProductTitle = reviewProductTitle;
	}

	@Column(name = "reviewProduct_body", unique = false, nullable = true, insertable = true, updatable = true, length = 65535)
	public String getReviewProductBody() {
		return this.reviewProductBody;
	}

	public void setReviewProductBody(String reviewProductBody) {
		this.reviewProductBody = reviewProductBody;
	}

	@Column(name = "reviewProduct_polarity", unique = false, nullable = true, insertable = true, updatable = true)
	public Integer getReviewProductPolarity() {
		return this.reviewProductPolarity;
	}

	public void setReviewProductPolarity(Integer reviewProductPolarity) {
		this.reviewProductPolarity = reviewProductPolarity;
	}
	
	@Column(name = "reviewProduct_date", unique = false, nullable = true ,insertable = true, updatable = true)
	public Calendar getReviewProductDate(){
		return this.reviewProductDate;
	}
	
	public void setReviewProductDate(Calendar date){
		this.reviewProductDate = date;
	}
	
	@Column(name = "reviewProduct_url", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
	public String getReviewProductUrl(){
		return this.reviewProductUrl;
	}
	
	public void setReviewProductUrl(String url){
		this.reviewProductUrl = url;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "reviewproduct")
	public Set<Sentenceproduct> getSentenceproducts() {
		return this.sentenceproducts;
	}

	public void setSentenceproducts(Set<Sentenceproduct> sentenceproducts) {
		this.sentenceproducts = sentenceproducts;
	}
}
