package com.leitmotiff.persistence.dao;

import org.springframework.stereotype.Repository;

import com.leitmotiff.persistence.entity.Source;

@Repository("sourceDao")
public class SourceDaoImpl extends GenericDaoImpl<Source> implements SourceDao {
	
	protected SourceDaoImpl(){
		super(Source.class);
	}
	
}