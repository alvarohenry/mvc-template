package com.leitmotiff.persistence.dao;

import org.springframework.stereotype.Repository;

import com.leitmotiff.persistence.entity.User;

@Repository("userDao")
public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao {

	protected UserDaoImpl(){
		super(User.class);
	}
	
}
