package com.leitmotiff.persistence.dao;

import com.leitmotiff.persistence.entity.Source;

public interface SourceDao extends GenericDao<Source>{

}
