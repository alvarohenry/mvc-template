package com.leitmotiff.persistence.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "service")
public class Services implements Serializable {

	private static final long serialVersionUID = 1L;

	private long serviceId;
	
	private Category category;
	
	private Company company;
	
	private String serviceName;
	
	private String serviceDescription;
	
	private Double servicePrice;
	
	private Integer servicePhoneMb;
	
	private Integer servicePhoneMinAll;
	
	private String servicePhoneExtra;
	
	private String serviceTvChannel;
	
	private String serviceTvExtra;
	
	private long servicePositive;
	
	private long serviceNegative;
	
	private String serviceImagePath;
	
	private String serviceUrl;
	
	private boolean serviceState;
	
	private Set<Reviewservice> reviewservices = new HashSet<Reviewservice>(0);
	
	private Set<ServiceHasProduct> serviceHasProducts = new HashSet<ServiceHasProduct>(0);

	/** default constructor */
	public Services() {
	}

	/** minimal constructor */
	public Services(long serviceId, Category category, Company company,
			String serviceName, long servicePositive, long serviceNegative,
			boolean serviceState) {
		this.serviceId = serviceId;
		this.category = category;
		this.company = company;
		this.serviceName = serviceName;
		this.servicePositive = servicePositive;
		this.serviceNegative = serviceNegative;
		this.serviceState = serviceState;
	}

	/** full constructor */
	public Services(long serviceId, Category category, Company company,
			String serviceName, String serviceDescription, Double servicePrice,
			Integer servicePhoneMb, Integer servicePhoneMinAll,
			String servicePhoneExtra, String serviceTvChannel,
			String serviceTvExtra, long servicePositive, long serviceNegative,
			String serviceImagePath, String serviceUrl, boolean serviceState,
			Set<Reviewservice> reviewservices,
			Set<ServiceHasProduct> serviceHasProducts) {
		this.serviceId = serviceId;
		this.category = category;
		this.company = company;
		this.serviceName = serviceName;
		this.serviceDescription = serviceDescription;
		this.servicePrice = servicePrice;
		this.servicePhoneMb = servicePhoneMb;
		this.servicePhoneMinAll = servicePhoneMinAll;
		this.servicePhoneExtra = servicePhoneExtra;
		this.serviceTvChannel = serviceTvChannel;
		this.serviceTvExtra = serviceTvExtra;
		this.servicePositive = servicePositive;
		this.serviceNegative = serviceNegative;
		this.serviceImagePath = serviceImagePath;
		this.serviceUrl = serviceUrl;
		this.serviceState = serviceState;
		this.reviewservices = reviewservices;
		this.serviceHasProducts = serviceHasProducts;
	}

	// Property accessors
	@Id
	@Column(name = "service_id", unique = true, nullable = false, insertable = true, updatable = true)
	public long getServiceId() {
		return this.serviceId;
	}

	public void setServiceId(long serviceId) {
		this.serviceId = serviceId;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "category_id", unique = false, nullable = false, insertable = true, updatable = true)
	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "company_id", unique = false, nullable = false, insertable = true, updatable = true)
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Column(name = "service_name", unique = false, nullable = false, insertable = true, updatable = true, length = 200)
	public String getServiceName() {
		return this.serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	@Column(name = "service_description", unique = false, nullable = true, insertable = true, updatable = true, length = 65535)
	public String getServiceDescription() {
		return this.serviceDescription;
	}

	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}

	@Column(name = "service_price", unique = false, nullable = true, insertable = true, updatable = true, precision = 22, scale = 0)
	public Double getServicePrice() {
		return this.servicePrice;
	}

	public void setServicePrice(Double servicePrice) {
		this.servicePrice = servicePrice;
	}

	@Column(name = "service_phone_mb", unique = false, nullable = true, insertable = true, updatable = true)
	public Integer getServicePhoneMb() {
		return this.servicePhoneMb;
	}

	public void setServicePhoneMb(Integer servicePhoneMb) {
		this.servicePhoneMb = servicePhoneMb;
	}

	@Column(name = "service_phone_min_all", unique = false, nullable = true, insertable = true, updatable = true)
	public Integer getServicePhoneMinAll() {
		return this.servicePhoneMinAll;
	}

	public void setServicePhoneMinAll(Integer servicePhoneMinAll) {
		this.servicePhoneMinAll = servicePhoneMinAll;
	}

	@Column(name = "service_phone_extra", unique = false, nullable = true, insertable = true, updatable = true, length = 1000)
	public String getServicePhoneExtra() {
		return this.servicePhoneExtra;
	}

	public void setServicePhoneExtra(String servicePhoneExtra) {
		this.servicePhoneExtra = servicePhoneExtra;
	}

	@Column(name = "service_tv_channel", unique = false, nullable = true, insertable = true, updatable = true, length = 200)
	public String getServiceTvChannel() {
		return this.serviceTvChannel;
	}

	public void setServiceTvChannel(String serviceTvChannel) {
		this.serviceTvChannel = serviceTvChannel;
	}

	@Column(name = "service_tv_extra", unique = false, nullable = true, insertable = true, updatable = true, length = 1000)
	public String getServiceTvExtra() {
		return this.serviceTvExtra;
	}

	public void setServiceTvExtra(String serviceTvExtra) {
		this.serviceTvExtra = serviceTvExtra;
	}

	@Column(name = "service_positive", unique = false, nullable = false, insertable = true, updatable = true)
	public long getServicePositive() {
		return this.servicePositive;
	}

	public void setServicePositive(long servicePositive) {
		this.servicePositive = servicePositive;
	}

	@Column(name = "service_negative", unique = false, nullable = false, insertable = true, updatable = true)
	public long getServiceNegative() {
		return this.serviceNegative;
	}

	public void setServiceNegative(long serviceNegative) {
		this.serviceNegative = serviceNegative;
	}

	@Column(name = "service_image_path", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
	public String getServiceImagePath() {
		return this.serviceImagePath;
	}

	public void setServiceImagePath(String serviceImagePath) {
		this.serviceImagePath = serviceImagePath;
	}

	@Column(name = "service_url", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
	public String getServiceUrl() {
		return this.serviceUrl;
	}

	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}

	@Column(name = "service_state", unique = false, nullable = false, insertable = true, updatable = true)
	public boolean isServiceState() {
		return this.serviceState;
	}

	public void setServiceState(boolean serviceState) {
		this.serviceState = serviceState;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "service")
	public Set<Reviewservice> getReviewservices() {
		return this.reviewservices;
	}

	public void setReviewservices(Set<Reviewservice> reviewservices) {
		this.reviewservices = reviewservices;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "service")
	public Set<ServiceHasProduct> getServiceHasProducts() {
		return this.serviceHasProducts;
	}

	public void setServiceHasProducts(Set<ServiceHasProduct> serviceHasProducts) {
		this.serviceHasProducts = serviceHasProducts;
	}

}
