package com.leitmotiff.persistence.dao;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.leitmotiff.persistence.entity.Category;
import com.leitmotiff.persistence.entity.Company;
import com.leitmotiff.persistence.entity.Dictionary;
import com.leitmotiff.persistence.entity.Product;
import com.leitmotiff.persistence.entity.Services;

import edu.vt.middleware.dictionary.ArrayWordList;

@Repository("dictionaryDao")
public class DictionaryDaoImpl extends GenericDaoImpl<Dictionary> implements DictionaryDao	{
	
	public DictionaryDaoImpl(){
		super(Dictionary.class);
	}
	
	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private CategoryDao categoryDao;
	
	@Autowired
	private CompanyDao companyDao;
	
	@Autowired
	private ServiceDao serviceDao;
	
	@Transactional
	public void createDictionary(){
		List<String> words = new ArrayList<String>();
		List<Product> products =  productDao.findAll();
		for(Product product : products){
			System.out.println(product.getProductName());
			words.addAll(createString(product.getProductName()));
			words.addAll(createString(product.getProductDescription()));
			words.addAll(createString(product.getProductBattery()));
			words.addAll(createString(product.getProductBrand()));
			words.addAll(createString(product.getProductDimension()));
			words.addAll(createString(product.getProductExtraFeature()));
			words.addAll(createString(product.getProductOperatingSystem()));
			words.addAll(createString(product.getProductProcessor()));
		}
		
		System.out.println("==================================");
		for(int i = 0; i< words.size(); i++){
			System.out.println(words.get(i));
		}
		
		List<Category> categories = categoryDao.findAll();
		for(Category category : categories){
			words.addAll(createString(category.getCategoryName()));
		}
		
		System.out.println("==================================");
		for(int i = 0; i< words.size(); i++){
			System.out.println(words.get(i));
		}
		
		List<Company> companies = companyDao.findAll();
		for(Company company : companies){
			words.addAll(createString(company.getCompanyDescription()));
			words.addAll(createString(company.getCompanyName()));
		}
		
		System.out.println("==================================");
		for(int i = 0; i< words.size(); i++){
			System.out.println(words.get(i));
		}
		
		List<Services> services = serviceDao.findAll();
		for(Services service : services){
			words.addAll(createString(service.getServiceDescription()));
			words.addAll(createString(service.getServiceName()));
			words.addAll(createString(service.getServicePhoneExtra()));
			words.addAll(createString(service.getServiceTvChannel()));
			words.addAll(createString(service.getServiceTvExtra()));
		}
		
		words = repeatFiltre(words);
		System.out.println("==================================");
		for(int i = 0; i< words.size(); i++){
			System.out.println(words.get(i));
		}
		
		String[] array = words.toArray(new String[0]);
		String[] wordList = new String[1];
		wordList[0] = "Words";
		Comparator<String> comparator = new ArrayWordList(wordList).getComparator();
		for (int i = 0; i < array.length; i++){
			for (int j = i + 1; j < array.length; j++){	
				if (comparator.compare(array[j], array[i]) < 0){
					String temporal = array[j];
					array[j] = array[i];
					array[i] = temporal;
				}
			}
		}
		
		for (int i = 0; i< array.length; i++){
			Dictionary dictionary = new Dictionary(i + 1,array[i]);
			System.out.println(i);
			saveOrUpdate(dictionary);
		}
	}
	
	private List<String> createString(String string){
		List<String> returnList = new ArrayList<String>();
		if(string != null){	
			StringTokenizer stringToken = new StringTokenizer(string);
			while(stringToken.hasMoreElements()){
				String token = stringToken.nextElement().toString();
				token = tokenRemplace(token);
				token = token.trim();
				returnList.add(token);
			}
		}
		return returnList;
	}
	
	private List<String> repeatFiltre(List<String> addList){
		List<String> returnList = new ArrayList<String>();
		for(String token : addList){
			if(!returnList.contains(token.toLowerCase())){
				returnList.add(token.toLowerCase());
			}
		}
		return returnList;
	}
	
	private String tokenRemplace(String token){
		String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ°!#$%&/()=?¡'¿´¨+*~{[^}]`,;.:-_\"";
	    String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
	    String resultToken = token;
	    
	    for (int i=0; i<original.length(); i++) {
			resultToken = resultToken.replace(original.charAt(i), ascii.charAt(i));
		}
		
	    return resultToken;
	}
}
