package com.leitmotiff.persistence.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.leitmotiff.persistence.entity.ServiceHasProduct;

@Repository("serviceHasProductDao")
public class ServiceHasProductDaoImpl extends GenericDaoImpl<ServiceHasProduct> implements ServiceHasProductDao {

	private ServiceHasProductDaoImpl(){
		super(ServiceHasProduct.class);
	}
	
	@Transactional
	public List<ServiceHasProduct> getServiceHasProductOrderPrice(String table, Long id, boolean order){
		String ord = "ASC";
		if (!order) {ord = "DESC";}
		
		String condOrd = "";
		if (table == "service") { condOrd = "shp.product = "+id+" ORDER BY t.servicePrice "; }
		else if (table == "product") { condOrd = "shp.service = "+id+" ORDER BY shp.serviceHasProductPrice "; }
		
		try{
			return (List<ServiceHasProduct>) getCurrentSession().
					createQuery("SELECT shp FROM ServiceHasProduct AS shp INNER JOIN shp."
							+ table + " AS t WHERE " + condOrd + ord).list();
		}catch(Exception e){
			return null;
		}
	}
	
	
	@Transactional
	public List<ServiceHasProduct> getServiceHasProductOrderValue(String table, Long id, boolean order){
		String ord = "ASC";
		if (!order) {ord = "DESC";}
		
		String condOrd = "";
		if (table == "service") { condOrd = "shp.product = "+id+" ORDER BY (t.servicePositive - t.serviceNegative) "; }
		else if (table == "product") { condOrd = "shp.service = "+id+" ORDER BY (t.productPositive - t.productNegative) "; }
		
		try{
			return (List<ServiceHasProduct>) getCurrentSession().
					createQuery("SELECT shp FROM ServiceHasProduct AS shp INNER JOIN shp." 
							+ table + " AS t WHERE " + condOrd + ord).list();
			
		}catch(Exception e){
			return null;
		}
	}
	
	@Transactional
	public ServiceHasProduct getMinurService(long id){	
		Criterion criterion = Restrictions.eq("service.id", id);
		Query query = null;
		query = getCurrentSession().createQuery("select shp from ServiceHasProduct as shp where product ="+id+"order by serviceHasProductPrice asc");
		query.setMaxResults(1);
		List<ServiceHasProduct> services = query.list();
		ServiceHasProduct minur = null;
		double minurI = 10000000;
		for(ServiceHasProduct service : services){
			if(service.getServiceHasProductPrice() < minurI){
				minurI = service.getServiceHasProductPrice();
				minur = service;
			}
		}
		return minur;
	}
	
}
