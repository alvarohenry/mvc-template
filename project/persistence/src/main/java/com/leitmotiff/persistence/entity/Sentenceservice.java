package com.leitmotiff.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sentenceservice")
public class Sentenceservice implements Serializable {

	private static final long serialVersionUID = 1L;

	private long sentenceServiceId;
	
	private Reviewservice reviewservice;
	
	private String sentenceServiceText;
	
	private Integer sentenceServicePolarity;

	/** default constructor */
	public Sentenceservice() { }

	/** minimal constructor */
	public Sentenceservice(long sentenceServiceId, Reviewservice reviewservice) {
		this.sentenceServiceId = sentenceServiceId;
		this.reviewservice = reviewservice;
	}

	/** full constructor */
	public Sentenceservice(long sentenceServiceId, Reviewservice reviewservice,
			String sentenceServiceText, Integer sentenceServicePolarity) {
		this.sentenceServiceId = sentenceServiceId;
		this.reviewservice = reviewservice;
		this.sentenceServiceText = sentenceServiceText;
		this.sentenceServicePolarity = sentenceServicePolarity;
	}

	// Property accessors
	@Id
	@Column(name = "sentenceService_id", unique = true, nullable = false, insertable = true, updatable = true)
	public long getSentenceServiceId() {
		return this.sentenceServiceId;
	}

	public void setSentenceServiceId(long sentenceServiceId) {
		this.sentenceServiceId = sentenceServiceId;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "reviewService_id", unique = false, nullable = false, insertable = true, updatable = true)
	public Reviewservice getReviewservice() {
		return this.reviewservice;
	}

	public void setReviewservice(Reviewservice reviewservice) {
		this.reviewservice = reviewservice;
	}

	@Column(name = "sentenceService_text", unique = false, nullable = true, insertable = true, updatable = true, length = 65535)
	public String getSentenceServiceText() {
		return this.sentenceServiceText;
	}

	public void setSentenceServiceText(String sentenceServiceText) {
		this.sentenceServiceText = sentenceServiceText;
	}

	@Column(name = "sentenceService_polarity", unique = false, nullable = true, insertable = true, updatable = true)
	public Integer getSentenceServicePolarity() {
		return this.sentenceServicePolarity;
	}

	public void setSentenceServicePolarity(Integer sentenceServicePolarity) {
		this.sentenceServicePolarity = sentenceServicePolarity;
	}

}
