package com.leitmotiff.persistence.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.leitmotiff.persistence.entity.MostSearchedSentences;

@Repository("mostSearchedSentencesDao")
public class MostSearchedSentencesDaoImpl extends GenericDaoImpl<MostSearchedSentences> implements MostSearchedSentencesDao {

	protected MostSearchedSentencesDaoImpl(){
		super(MostSearchedSentences.class);
	}
	
	@Transactional
	public List<MostSearchedSentences> getMostSearchedSentences(String search){
		return (List<MostSearchedSentences>) getCurrentSession().createQuery("select t from MostSearchedSentences as t "
									+ "where mostSearchedSentencesSentence like '%"
									+ search + "%' order by mostSearchedSentencesValue desc").list();
	}
	
	
	
}
