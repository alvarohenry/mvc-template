package com.leitmotiff.persistence.dao;

import com.leitmotiff.persistence.entity.Category;

public interface CategoryDao extends GenericDao<Category> {

}
