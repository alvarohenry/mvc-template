package com.leitmotiff.persistence.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "category")
public class Category implements Serializable {

	private static final long serialVersionUID = 1L;

	private long categoryId;

	private String categoryName;

	private String categoryImagePath;

	private long categoryTopId;

	private long categoryPatternId;

	private boolean categoryPolarity;
	
	private Set<Services> services = new HashSet<Services>(0);

	private Set<Product> products = new HashSet<Product>(0);

	public Category() { }

	public Category(long categoryId, String categoryName, String imagePath,
			long categoryTopId) {
		this.categoryId = categoryId;
		this.categoryName = categoryName;
		this.categoryImagePath = imagePath;
		this.categoryTopId = categoryTopId;
	}

	/**
	 * minimal constructor 
	 * @param categoryId
	 * @param categoryName
	 * @param categoryTopId
	 * @param categoryPatternId
	 */
	public Category(long categoryId, String categoryName, long categoryTopId,
			long categoryPatternId) {
		this.categoryId = categoryId;
		this.categoryName = categoryName;
		this.categoryTopId = categoryTopId;
		this.categoryPatternId = categoryPatternId;
	}

	/**
	 * * full constructor 
	 * @param categoryId
	 * @param categoryName
	 * @param categoryImagePath
	 * @param categoryTopId
	 * @param categoryPatternId
	 * @param services
	 */
	public Category(long categoryId, String categoryName,
			String categoryImagePath, long categoryTopId,
			long categoryPatternId, Set<Services> services) {
		this.categoryId = categoryId;
		this.categoryName = categoryName;
		this.categoryImagePath = categoryImagePath;
		this.categoryTopId = categoryTopId;
		this.categoryPatternId = categoryPatternId;
		this.services = services;
	}

	// Property accessors
	@Id
	@Column(name = "category_id", unique = true, nullable = false, insertable = true, updatable = true)
	public long getCategoryId() {
		return this.categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	@Column(name = "category_name", unique = false, nullable = false, insertable = true, updatable = true, length = 200)
	public String getCategoryName() {
		return this.categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	@Column(name = "category_image_path", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
	public String getCategoryImagePath() {
		return this.categoryImagePath;
	}

	public void setCategoryImagePath(String categoryImagePath) {
		this.categoryImagePath = categoryImagePath;
	}

	@Column(name = "category_top_id", unique = false, nullable = false, insertable = true, updatable = true)
	public long getCategoryTopId() {
		return this.categoryTopId;
	}

	public void setCategoryTopId(long categoryTopId) {
		this.categoryTopId = categoryTopId;
	}

	@Column(name = "category_pattern_id", unique = false, nullable = false, insertable = true, updatable = true)
	public long getCategoryPatternId() {
		return this.categoryPatternId;
	}

	public void setCategoryPatternId(long categoryPatternId) {
		this.categoryPatternId = categoryPatternId;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "category")
	public Set<Services> getServices() {
		return this.services;
	}

	public void setServices(Set<Services> services) {
		this.services = services;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "category")
	public Set<Product> getProducts() {
		return this.products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}

	@Column(name = "category_polarity", unique = false, nullable = false, insertable = true, updatable = true)
	public boolean isCategoryPolarity() {
		return categoryPolarity;
	}

	public void setCategoryPolarity(boolean categoryPolarity) {
		this.categoryPolarity = categoryPolarity;
	}

}
