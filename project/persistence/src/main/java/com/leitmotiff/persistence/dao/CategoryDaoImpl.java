package com.leitmotiff.persistence.dao;

import org.springframework.stereotype.Repository;

import com.leitmotiff.persistence.entity.Category;

@Repository("categoryDao")
public class CategoryDaoImpl extends GenericDaoImpl<Category> implements CategoryDao {

	protected CategoryDaoImpl() {
		super(Category.class);
	}

}
