package com.leitmotiff.persistence.entity;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "company_has_product")
public class CompanyHasProduct implements Serializable {

	private static final long serialVersionUID = 1L;

	private CompanyHasProductId id;

	private Company company;

	private Product product;

	private String companyHasProductUrl;

	private boolean companyHasProductState;

	private Double companyHasProductPrice;

	/** default constructor */
	public CompanyHasProduct() {
	}

	/** minimal constructor */
	public CompanyHasProduct(CompanyHasProductId id, Company company,
			Product product, boolean companyHasProductState) {
		this.id = id;
		this.company = company;
		this.product = product;
		this.companyHasProductState = companyHasProductState;
	}

	/** full constructor */
	public CompanyHasProduct(CompanyHasProductId id, Company company,
			Product product, String companyHasProductUrl,
			boolean companyHasProductState, Double companyHasProductPrice) {
		this.id = id;
		this.company = company;
		this.product = product;
		this.companyHasProductUrl = companyHasProductUrl;
		this.companyHasProductState = companyHasProductState;
		this.companyHasProductPrice = companyHasProductPrice;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "companyId", column = @Column(name = "company_id", unique = false, nullable = false, insertable = true, updatable = true)),
			@AttributeOverride(name = "productId", column = @Column(name = "product_id", unique = false, nullable = false, insertable = true, updatable = true)) })
	public CompanyHasProductId getId() {
		return this.id;
	}

	public void setId(CompanyHasProductId id) {
		this.id = id;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "company_id", unique = false, nullable = false, insertable = false, updatable = false)
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "product_id", unique = false, nullable = false, insertable = false, updatable = false)
	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Column(name = "company_has_product_url", unique = false, nullable = true, insertable = true, updatable = true, length = 200)
	public String getCompanyHasProductUrl() {
		return this.companyHasProductUrl;
	}

	public void setCompanyHasProductUrl(String companyHasProductUrl) {
		this.companyHasProductUrl = companyHasProductUrl;
	}

	@Column(name = "company_has_product_state", unique = false, nullable = false, insertable = true, updatable = true)
	public boolean isCompanyHasProductState() {
		return this.companyHasProductState;
	}

	public void setCompanyHasProductState(boolean companyHasProductState) {
		this.companyHasProductState = companyHasProductState;
	}

	@Column(name = "company_has_product_price", unique = false, nullable = true, insertable = true, updatable = true, precision = 22, scale = 0)
	public Double getCompanyHasProductPrice() {
		return this.companyHasProductPrice;
	}

	public void setCompanyHasProductPrice(Double companyHasProductPrice) {
		this.companyHasProductPrice = companyHasProductPrice;
	}
}
