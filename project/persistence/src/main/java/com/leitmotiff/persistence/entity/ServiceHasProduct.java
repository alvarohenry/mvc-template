package com.leitmotiff.persistence.entity;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "service_has_product")
public class ServiceHasProduct implements Serializable {

	private static final long serialVersionUID = 1L;

	private ServiceHasProductId id;
	
	private Product product;
	
	private Services service;
	
	private Double serviceHasProductPrice;
	
	private String serviceHasProductExtra;
	
	private String serviceHasProductUrl;
	
	private boolean serviceHasProductState;

	/** default constructor */
	public ServiceHasProduct() {
	}

	/** minimal constructor */
	public ServiceHasProduct(ServiceHasProductId id, Product product,
			Services service, boolean serviceHasProductState) {
		this.id = id;
		this.product = product;
		this.service = service;
		this.serviceHasProductState = serviceHasProductState;
	}

	/** full constructor */
	public ServiceHasProduct(ServiceHasProductId id, Product product,
			Services service, Double serviceHasProductPrice,
			String serviceHasProductExtra, String serviceHasProductUrl,
			boolean serviceHasProductState) {
		this.id = id;
		this.product = product;
		this.service = service;
		this.serviceHasProductPrice = serviceHasProductPrice;
		this.serviceHasProductExtra = serviceHasProductExtra;
		this.serviceHasProductUrl = serviceHasProductUrl;
		this.serviceHasProductState = serviceHasProductState;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "serviceId", column = @Column(name = "service_id", unique = false, nullable = false, insertable = true, updatable = true)),
			@AttributeOverride(name = "productId", column = @Column(name = "product_id", unique = false, nullable = false, insertable = true, updatable = true)),
			@AttributeOverride(name = "serviceHasProductNum", column = @Column(name = "service_has_product_num", unique = false, nullable = false, insertable = true, updatable = true)) })
	public ServiceHasProductId getId() {
		return this.id;
	}

	public void setId(ServiceHasProductId id) {
		this.id = id;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "product_id", unique = false, nullable = false, insertable = false, updatable = false)
	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "service_id", unique = false, nullable = false, insertable = false, updatable = false)
	public Services getService() {
		return this.service;
	}

	public void setService(Services service) {
		this.service = service;
	}

	@Column(name = "service_has_product_price", unique = false, nullable = true, insertable = true, updatable = true, precision = 22, scale = 0)
	public Double getServiceHasProductPrice() {
		return this.serviceHasProductPrice;
	}

	public void setServiceHasProductPrice(Double serviceHasProductPrice) {
		this.serviceHasProductPrice = serviceHasProductPrice;
	}

	@Column(name = "service_has_product_extra", unique = false, nullable = true, insertable = true, updatable = true, length = 1000)
	public String getServiceHasProductExtra() {
		return this.serviceHasProductExtra;
	}

	public void setServiceHasProductExtra(String serviceHasProductExtra) {
		this.serviceHasProductExtra = serviceHasProductExtra;
	}
	
	@Column(name="service_has_product_url", unique=false, nullable=true, insertable=true, updatable=true, length=100)
    public String getServiceHasProductUrl() {
        return this.serviceHasProductUrl;
    }
    
    public void setServiceHasProductUrl(String serviceHasProductUrl) {
        this.serviceHasProductUrl = serviceHasProductUrl;
    }

	@Column(name = "service_has_product_state", unique = false, nullable = false, insertable = true, updatable = true)
	public boolean isServiceHasProductState() {
		return this.serviceHasProductState;
	}

	public void setServiceHasProductState(boolean serviceHasProductState) {
		this.serviceHasProductState = serviceHasProductState;
	}

}
