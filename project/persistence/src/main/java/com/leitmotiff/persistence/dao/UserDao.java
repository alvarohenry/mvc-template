package com.leitmotiff.persistence.dao;

import com.leitmotiff.persistence.entity.User;

public interface UserDao extends GenericDao<User>{

}
