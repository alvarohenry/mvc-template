package com.leitmotiff.persistence.dao;

import com.leitmotiff.persistence.entity.Dictionary;

public interface DictionaryDao extends GenericDao<Dictionary> {

	void createDictionary();
	
}
