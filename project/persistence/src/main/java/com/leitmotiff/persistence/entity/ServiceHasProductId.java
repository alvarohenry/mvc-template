package com.leitmotiff.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ServiceHasProductId implements Serializable {

	private static final long serialVersionUID = 1L;

	private long serviceId;
	
	private long productId;
	
	private int serviceHasProductNum;

	/** default constructor */
	public ServiceHasProductId() {
	}

	/** full constructor */
	public ServiceHasProductId(long serviceId, long productId,
			int serviceHasProductNum) {
		this.serviceId = serviceId;
		this.productId = productId;
		this.serviceHasProductNum = serviceHasProductNum;
	}

	// Property accessors
	@Column(name = "service_id", unique = false, nullable = false, insertable = true, updatable = true)
	public long getServiceId() {
		return this.serviceId;
	}

	public void setServiceId(long serviceId) {
		this.serviceId = serviceId;
	}

	@Column(name = "product_id", unique = false, nullable = false, insertable = true, updatable = true)
	public long getProductId() {
		return this.productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	@Column(name = "service_has_product_num", unique = false, nullable = false, insertable = true, updatable = true)
	public int getServiceHasProductNum() {
		return this.serviceHasProductNum;
	}

	public void setServiceHasProductNum(int serviceHasProductNum) {
		this.serviceHasProductNum = serviceHasProductNum;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ServiceHasProductId))
			return false;
		ServiceHasProductId castOther = (ServiceHasProductId) other;

		return (this.getServiceId() == castOther.getServiceId())
				&& (this.getProductId() == castOther.getProductId())
				&& (this.getServiceHasProductNum() == castOther
						.getServiceHasProductNum());
	}

	public int hashCode() {
		int result = 17;
		result = 37 * result + (int) this.getServiceId();
		result = 37 * result + (int) this.getProductId();
		result = 37 * result + this.getServiceHasProductNum();
		return result;
	}

}
