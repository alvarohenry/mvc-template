package com.leitmotiff.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sentenceproduct")
public class Sentenceproduct implements Serializable {

	private static final long serialVersionUID = 1L;

	private long sentenceProductId;

	private Reviewproduct reviewproduct;

	private String sentenceProductText;

	private Integer sentenceProductPolarity;

	/** default constructor */
	public Sentenceproduct() {
	}

	/** minimal constructor */
	public Sentenceproduct(long sentenceProductId, Reviewproduct reviewproduct) {
		this.sentenceProductId = sentenceProductId;
		this.reviewproduct = reviewproduct;
	}

	/** full constructor */
	public Sentenceproduct(long sentenceProductId, Reviewproduct reviewproduct,
			String sentenceProductText, Integer sentenceProductPolarity) {
		this.sentenceProductId = sentenceProductId;
		this.reviewproduct = reviewproduct;
		this.sentenceProductText = sentenceProductText;
		this.sentenceProductPolarity = sentenceProductPolarity;
	}

	// Property accessors
	@Id
	@Column(name = "sentenceProduct_id", unique = true, nullable = false, insertable = true, updatable = true)
	public long getSentenceProductId() {
		return this.sentenceProductId;
	}

	public void setSentenceProductId(long sentenceProductId) {
		this.sentenceProductId = sentenceProductId;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "reviewProduct_id", unique = false, nullable = false, insertable = true, updatable = true)
	public Reviewproduct getReviewproduct() {
		return this.reviewproduct;
	}

	public void setReviewproduct(Reviewproduct reviewproduct) {
		this.reviewproduct = reviewproduct;
	}

	@Column(name = "sentenceProduct_text", unique = false, nullable = true, insertable = true, updatable = true, length = 65535)
	public String getSentenceProductText() {
		return this.sentenceProductText;
	}

	public void setSentenceProductText(String sentenceProductText) {
		this.sentenceProductText = sentenceProductText;
	}

	@Column(name = "sentenceProduct_polarity", unique = false, nullable = true, insertable = true, updatable = true)
	public Integer getSentenceProductPolarity() {
		return this.sentenceProductPolarity;
	}

	public void setSentenceProductPolarity(Integer sentenceProductPolarity) {
		this.sentenceProductPolarity = sentenceProductPolarity;
	}

}
