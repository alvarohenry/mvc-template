package com.leitmotiff.persistence.dao;

import java.util.List;

import com.leitmotiff.persistence.entity.MostSearchedSentences;

public interface MostSearchedSentencesDao extends GenericDao<MostSearchedSentences> {

	List<MostSearchedSentences> getMostSearchedSentences(String search);
	
}
