package com.leitmotiff.persistence.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.leitmotiff.persistence.entity.Services;

@Repository("serviceDao")
public class ServiceDaoImpl extends GenericDaoImpl<Services> implements ServiceDao{
	
	
	private ServiceDaoImpl(){
		super(Services.class);
	}
	
	@Transactional
	public List<Services> getServiceOrderPrice(Long idCategory, boolean order){
		String ord = "ASC";
		if (!order) {ord = "DESC";}
		
		return (List<Services>) getCurrentSession().
				createQuery("SELECT t FROM Services AS t WHERE category = "
						+ idCategory + " ORDER BY servicePrice " + ord).list();
	}
	
	@Transactional
	public List<Services> getServiceOrderValue(Long idCategory, boolean order){
		String ord = "ASC";
		if (!order) {ord = "DESC";}
		
		return (List<Services>) getCurrentSession().
				createQuery("SELECT t FROM Services AS t WHERE category = " 
						+ idCategory + " ORDER BY (servicePositive - serviceNegative) " + ord).list();
	}

	public List<Services> getSearchedService_1_Init(String search){
		
		return  getCurrentSession().createQuery("select t from Services as t where serviceName like '" + search + "%'").list();		
	}
	
	public List<Services> getSearchedServices_2_Tokenizer(String search){
		
		String consult;
		consult = "select t from Services as t where serviceName like '%" + search + "%'";
		StringTokenizer sentenceSearch = new StringTokenizer(search);
		List<Services> returnList = new ArrayList<Services>();
		if(sentenceSearch.countTokens() > 1){
			List<Services> serviceList;
			while(sentenceSearch.hasMoreElements()){
				String word = sentenceSearch.nextElement().toString();
				consult = consult.concat(" and serviceName like '%" + word + "%'");
			}
			serviceList = getCurrentSession().createQuery(consult).list();
			for(Services service : serviceList){
				returnList.add(service);
			}
		}
		return returnList;
	}
	
	public List<Services> getSearchedService_3_Count(String search){
		
		StringTokenizer sentenceSearch = new StringTokenizer(search);
		List<Services> returnList = new ArrayList<Services>();
		
		if(sentenceSearch.countTokens() > 1){
			List<Services> serviceList;
			String word = null;
			int contador = 1;
			while(sentenceSearch.hasMoreElements()){
				sentenceSearch = new StringTokenizer(search);
				for(int i = 0; i<contador;i++){	
					word = sentenceSearch.nextElement().toString();
				}
				while(sentenceSearch.hasMoreElements()){
					String nextWord = sentenceSearch.nextElement().toString();
					serviceList = getCurrentSession().createQuery("select t from Services as t where serviceName like '%" + word + "%' and serviceName like '%" + nextWord + "%'").list();
					for(Services service : serviceList){
						returnList.add(service);
					}
				}
				contador++;
			}
			
		}
		return returnList;
		
	}
	
	public List<Services> getSearchedService_4_TokenizerInit(String search){
		
		StringTokenizer sentenceSearch = new StringTokenizer(search);
		List<Services> returnList = new ArrayList<Services>();
		while(sentenceSearch.hasMoreElements()){
			String word = sentenceSearch.nextElement().toString();
			List<Services> serviceList = getCurrentSession().createQuery("select t from Services as t where serviceName like '" + word + "%'").list();
			for(Services service : serviceList){
					returnList.add(service);
			}
		}
		return returnList;
	}
	
	public List<Services> getSearchedServices_5_Contiene(String search){
		 
		return getCurrentSession().createQuery("select t from Services as t where serviceName like '%" + search + "%' and not serviceName like '" + search + "%'").list();
	
	}
	
	public List<Services> getSearchedProduct_6_(String search){
		
		StringTokenizer sentenceSearch = new StringTokenizer(search);
		List<Services> returnList = new ArrayList<Services>();
		while(sentenceSearch.hasMoreElements()){
			String word = sentenceSearch.nextElement().toString();
			List<Services> serviceList = getCurrentSession().createQuery("select t from Services as t where serviceName like '%" + word + "%' and not serviceName like '" + word + "%'").list();
			for(Services service : serviceList){
					returnList.add(service);
			}
		}
		return returnList;
	}	
}
