package com.leitmotiff.persistence.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "source")
public class Source implements Serializable {

	private static final long serialVersionUID = 1L;

	private long sourceId;
	
	private String sourceName;
	
	private String sourceUrl;
	
	private String sourceDescription;
	
	private Set<Reviewproduct> reviewproducts = new HashSet<Reviewproduct>(0);
	
	private Set<Reviewservice> reviewservices = new HashSet<Reviewservice>(0);

	/** default constructor */
	public Source() {
	}

	/** minimal constructor */
	public Source(long sourceId) {
		this.sourceId = sourceId;
	}

	/** full constructor */
	public Source(long sourceId, String sourceName, String sourceUrl,
			String sourceDescription, Set<Reviewproduct> reviewproducts,
			Set<Reviewservice> reviewservices) {
		this.sourceId = sourceId;
		this.sourceName = sourceName;
		this.sourceUrl = sourceUrl;
		this.sourceDescription = sourceDescription;
		this.reviewproducts = reviewproducts;
		this.reviewservices = reviewservices;
	}

	// Property accessors
	@Id
	@Column(name = "source_id", unique = true, nullable = false, insertable = true, updatable = true)
	public long getSourceId() {
		return this.sourceId;
	}

	public void setSourceId(long sourceId) {
		this.sourceId = sourceId;
	}

	@Column(name = "source_name", unique = false, nullable = true, insertable = true, updatable = true)
	public String getSourceName() {
		return this.sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	@Column(name = "source_url", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
	public String getSourceUrl() {
		return this.sourceUrl;
	}

	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}

	@Column(name = "source_description", unique = false, nullable = true, insertable = true, updatable = true, length = 45)
	public String getSourceDescription() {
		return this.sourceDescription;
	}

	public void setSourceDescription(String sourceDescription) {
		this.sourceDescription = sourceDescription;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "source")
	public Set<Reviewproduct> getReviewproducts() {
		return this.reviewproducts;
	}

	public void setReviewproducts(Set<Reviewproduct> reviewproducts) {
		this.reviewproducts = reviewproducts;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "source")
	public Set<Reviewservice> getReviewservices() {
		return this.reviewservices;
	}

	public void setReviewservices(Set<Reviewservice> reviewservices) {
		this.reviewservices = reviewservices;
	}

}
