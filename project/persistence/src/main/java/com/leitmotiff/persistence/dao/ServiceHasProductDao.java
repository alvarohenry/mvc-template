package com.leitmotiff.persistence.dao;

import java.util.List;

import org.hibernate.criterion.Criterion;

import com.leitmotiff.persistence.entity.ServiceHasProduct;

public interface ServiceHasProductDao extends GenericDao<ServiceHasProduct> {
	
	List<ServiceHasProduct> getServiceHasProductOrderPrice(String table, Long id, boolean order);
	
	List<ServiceHasProduct> getServiceHasProductOrderValue(String table, Long id, boolean order);
	
	ServiceHasProduct getMinurService(long id);
	
}
