package com.leitmotiff.persistence.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.leitmotiff.persistence.entity.Reviewproduct;
import com.leitmotiff.persistence.entity.Reviewservice;

@Repository("reviewServiceDao")
public class ReviewServiceDaoImpl extends GenericDaoImpl<Reviewservice> implements ReviewServiceDao {

	public ReviewServiceDaoImpl(){
		super(Reviewservice.class);
	}
	
	public List<Integer> numberComentary(long id){
		Criterion criterion = Restrictions.eq("service.serviceId",id);
		List<Reviewservice> reviews = findByCriteria(criterion);
		List<Integer> result = new ArrayList<Integer>();
		result.add(reviews.size());
		int positivos = 0;
		int negativos = 0;
		int neutros = 0;
		for(Reviewservice review : reviews){
			if(review.getReviewServicePolarity() == 1){
				positivos++;
			}
			else if(review.getReviewServicePolarity() == 0){
				neutros++;
			}
			else{
				negativos++;
			}
		}
		result.add(positivos);
		result.add(neutros);
		result.add(negativos);
		return result;
	}
}
