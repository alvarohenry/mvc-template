package com.leitmotiff.persistence.dao;

import com.leitmotiff.persistence.entity.Company;

public interface CompanyDao extends GenericDao<Company>{

}
