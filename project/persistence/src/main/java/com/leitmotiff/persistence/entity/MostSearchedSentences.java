package com.leitmotiff.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "most_searched_sentences")
public class MostSearchedSentences implements Serializable {

	private static final long serialVersionUID = 1L;

	private long mostSearchedSentencesId;

	private String mostSearchedSentencesSentence;

	private long mostSearchedSentencesValue;

	public MostSearchedSentences() { }

	public MostSearchedSentences(long mostSearchedSentencesId,
			String mostSearchedSentencesSentence,
			long mostSearchedSentencesValue) {
		this.setMostSearchedSentencesId(mostSearchedSentencesId);
		this.setMostSearchedSentencesSentence(mostSearchedSentencesSentence);
		this.setMostSearchedSentencesValue(mostSearchedSentencesValue);
	}

	@Id
	@Column(name = "most_searched_sentences_id", unique = true, nullable = false, insertable = true, updatable = true)
	public long getMostSearchedSentencesId() {
		return mostSearchedSentencesId;
	}

	public void setMostSearchedSentencesId(long mostSearchedSentencesId) {
		this.mostSearchedSentencesId = mostSearchedSentencesId;
	}

	@Column(name = "most_searched_sentences_sentence", unique = false, nullable = false, insertable = true, updatable = true, length = 200)
	public String getMostSearchedSentencesSentence() {
		return mostSearchedSentencesSentence;
	}

	public void setMostSearchedSentencesSentence(
			String mostSearchedSentencesSentence) {
		this.mostSearchedSentencesSentence = mostSearchedSentencesSentence;
	}

	@Column(name = "most_searched_sentences_value", unique = false, nullable = false, insertable = true, updatable = true)
	public long getMostSearchedSentencesValue() {
		return mostSearchedSentencesValue;
	}

	public void setMostSearchedSentencesValue(long mostSearchedSentencesValue) {
		this.mostSearchedSentencesValue = mostSearchedSentencesValue;
	}

}
