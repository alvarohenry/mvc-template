package com.leitmotiff.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="dictionary")
public class Dictionary implements Serializable {

	private static final long serialVersionUID = 1L;

	private long dictionaryId;
	
	private String dictionaryWord;
	
	public Dictionary(){ }
	
	public Dictionary(long dictionaryId, String dictionaryWord){
		this.setDictionayId(dictionaryId);
		this.setDictionaryWord(dictionaryWord);
	}

	@Id
    @Column(name="dictionary_id", unique=true, nullable=false, insertable=true, updatable=true)
	public long getDictionayId() {
		return dictionaryId;
	}

	public void setDictionayId(long dictionayId) {
		this.dictionaryId = dictionayId;
	}
	
	@Column(name="dictionary_word", unique=false, nullable=false, insertable=true, updatable=true, length=200)
	public String getDictionaryWord() {
		return dictionaryWord;
	}

	public void setDictionaryWord(String dictionaryWord) {
		this.dictionaryWord = dictionaryWord;
	}
	
}
