package com.leitmotiff.persistence.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.leitmotiff.persistence.entity.Reviewproduct;

@Repository("reviewProductDao")
public class ReviewProductDaoImpl extends GenericDaoImpl<Reviewproduct> implements ReviewProductDao {

	protected ReviewProductDaoImpl(){
		super(Reviewproduct.class);
	}

	public List<Integer> numberComentary(long id){
		Criterion criterion = Restrictions.eq("product.id",id);
		List<Reviewproduct> reviews = findByCriteria(criterion);
		List<Integer> result = new ArrayList<Integer>();
		result.add(reviews.size());
		int positivos = 0;
		int negativos = 0;
		int neutros = 0;
		for(Reviewproduct review : reviews){
			if(review.getReviewProductPolarity() == 1){
				positivos++;
			}
			else if(review.getReviewProductPolarity() == 0){
				neutros++;
			}
			else{
				negativos++;
			}
		}
		result.add(positivos);
		result.add(neutros);
		result.add(negativos);
		return result;
	}
	
}
