package com.leitmotiff.persistence.dao;

import java.util.List;

import com.leitmotiff.persistence.entity.Reviewproduct;

public interface ReviewProductDao  extends GenericDao<Reviewproduct>{

	List<Integer> numberComentary(long id);
	
}
