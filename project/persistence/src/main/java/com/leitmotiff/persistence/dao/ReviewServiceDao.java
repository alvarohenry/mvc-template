package com.leitmotiff.persistence.dao;

import java.util.List;

import com.leitmotiff.persistence.entity.Reviewservice;

public interface ReviewServiceDao extends GenericDao<Reviewservice>{

	List<Integer> numberComentary(long id);
	
}
