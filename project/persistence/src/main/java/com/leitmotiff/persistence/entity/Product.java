package com.leitmotiff.persistence.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.transaction.annotation.Transactional;

@Entity
@Table(name = "product")
public class Product implements Serializable {

	private static final long serialVersionUID = 1L;

	private long productId;

	private Category category;

	private String productName;

	private String productBrand;

	private String productDimension;

	private String productOperatingSystem;

	private String productImagePath;

	private String productDescription;

	private long productPositive;

	private long productNegative;

	private Float productMemory;

	private Float productRam;

	private String productProcessor;

	private Float productWeight;

	private String productBattery;

	private String productExtraFeature;

	private Set<CompanyHasProduct> companyHasProducts = new HashSet<CompanyHasProduct>(0);

	private Set<Reviewproduct> reviewproducts = new HashSet<Reviewproduct>(0);

	private Set<ServiceHasProduct> serviceHasProducts = new HashSet<ServiceHasProduct>(0);
	
	

	/** default constructor */
	public Product() {
	}

	/** minimal constructor */
	public Product(long productId, Category category, String productName,
			long productPositive, long productNegative) {
		this.productId = productId;
		this.category = category;
		this.productName = productName;
		this.productPositive = productPositive;
		this.productNegative = productNegative;
	}

	/** full constructor */
	public Product(long productId, Category category, String productName,
			String productBrand, String productDimension,
			String productOperatingSystem, String productImagePath,
			String productDescription, long productPositive,
			long productNegative, Float productMemory, Float productRam,
			String productProcessor, Float productWeight,
			String productBattery, String productExtraFeature,
			Set<CompanyHasProduct> companyHasProducts,
			Set<Reviewproduct> reviewproducts,
			Set<ServiceHasProduct> serviceHasProducts) {
		this.productId = productId;
		this.category = category;
		this.productName = productName;
		this.productBrand = productBrand;
		this.productDimension = productDimension;
		this.productOperatingSystem = productOperatingSystem;
		this.productImagePath = productImagePath;
		this.productDescription = productDescription;
		this.productPositive = productPositive;
		this.productNegative = productNegative;
		this.productMemory = productMemory;
		this.productRam = productRam;
		this.productProcessor = productProcessor;
		this.productWeight = productWeight;
		this.productBattery = productBattery;
		this.productExtraFeature = productExtraFeature;
		this.companyHasProducts = companyHasProducts;
		this.reviewproducts = reviewproducts;
		this.serviceHasProducts = serviceHasProducts;
	}

	// Property accessors
	@Id
	@Column(name = "product_id", unique = true, nullable = false, insertable = true, updatable = true)
	public long getProductId() {
		return this.productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "category_id", unique = false, nullable = false, insertable = true, updatable = true)
	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Column(name = "product_name", unique = false, nullable = false, insertable = true, updatable = true, length = 200)
	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "product_brand", unique = false, nullable = true, insertable = true, updatable = true, length = 200)
	public String getProductBrand() {
		return this.productBrand;
	}

	public void setProductBrand(String productBrand) {
		this.productBrand = productBrand;
	}

	@Column(name = "product_dimension", unique = false, nullable = true, insertable = true, updatable = true, length = 45)
	public String getProductDimension() {
		return this.productDimension;
	}

	public void setProductDimension(String productDimension) {
		this.productDimension = productDimension;
	}

	@Column(name = "product_operating_system", unique = false, nullable = true, insertable = true, updatable = true, length = 200)
	public String getProductOperatingSystem() {
		return this.productOperatingSystem;
	}

	public void setProductOperatingSystem(String productOperatingSystem) {
		this.productOperatingSystem = productOperatingSystem;
	}

	@Column(name = "product_image_path", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
	public String getProductImagePath() {
		return this.productImagePath;
	}

	public void setProductImagePath(String productImagePath) {
		this.productImagePath = productImagePath;
	}

	@Column(name = "product_description", unique = false, nullable = true, insertable = true, updatable = true, length = 65535)
	public String getProductDescription() {
		return this.productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	@Column(name = "product_positive", unique = false, nullable = false, insertable = true, updatable = true)
	public long getProductPositive() {
		return this.productPositive;
	}

	public void setProductPositive(long productPositive) {
		this.productPositive = productPositive;
	}

	@Column(name = "product_negative", unique = false, nullable = false, insertable = true, updatable = true)
	public long getProductNegative() {
		return this.productNegative;
	}

	public void setProductNegative(long productNegative) {
		this.productNegative = productNegative;
	}

	@Column(name = "product_memory", unique = false, nullable = true, insertable = true, updatable = true, precision = 12, scale = 0)
	public Float getProductMemory() {
		return this.productMemory;
	}

	public void setProductMemory(Float productMemory) {
		this.productMemory = productMemory;
	}

	@Column(name = "product_ram", unique = false, nullable = true, insertable = true, updatable = true, precision = 12, scale = 0)
	public Float getProductRam() {
		return this.productRam;
	}

	public void setProductRam(Float productRam) {
		this.productRam = productRam;
	}

	@Column(name = "product_processor", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
	public String getProductProcessor() {
		return this.productProcessor;
	}

	public void setProductProcessor(String productProcessor) {
		this.productProcessor = productProcessor;
	}

	@Column(name = "product_weight", unique = false, nullable = true, insertable = true, updatable = true, precision = 12, scale = 0)
	public Float getProductWeight() {
		return this.productWeight;
	}

	public void setProductWeight(Float productWeight) {
		this.productWeight = productWeight;
	}

	@Column(name = "product_battery", unique = false, nullable = true, insertable = true, updatable = true, length = 200)
	public String getProductBattery() {
		return this.productBattery;
	}

	public void setProductBattery(String productBattery) {
		this.productBattery = productBattery;
	}

	@Column(name = "product_extra_feature", unique = false, nullable = true, insertable = true, updatable = true, length = 1000)
	public String getProductExtraFeature() {
		return this.productExtraFeature;
	}

	public void setProductExtraFeature(String productExtraFeature) {
		this.productExtraFeature = productExtraFeature;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "product")
	public Set<CompanyHasProduct> getCompanyHasProducts() {
		return this.companyHasProducts;
	}

	public void setCompanyHasProducts(Set<CompanyHasProduct> companyHasProducts) {
		this.companyHasProducts = companyHasProducts;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "product")
	public Set<Reviewproduct> getReviewproducts() {
		return this.reviewproducts;
	}

	public void setReviewproducts(Set<Reviewproduct> reviewproducts) {
		this.reviewproducts = reviewproducts;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "product")
	public Set<ServiceHasProduct> getServiceHasProducts() {
		return this.serviceHasProducts;
	}

	public void setServiceHasProducts(Set<ServiceHasProduct> serviceHasProducts) {
		this.serviceHasProducts = serviceHasProducts;
	}

}
