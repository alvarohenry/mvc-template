package com.leitmotiff.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "user")
public class User implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private long userId;
	
	private String userName;
	
	private String userFbId;
	
	private String userEmail;
	
	private String userPass;
	
	public User(){}
	
	public User(long userId, String userName, String userFbId, String userEmail, String userPass){
		this.userId = userId;
		this.userName = userName;
		this.userFbId = userFbId;
		this.userEmail = userEmail;
		this.userPass = userPass;
	}

	@Id
	@Column(name = "user_id",unique = true, nullable = false, insertable = true, updatable = true)
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	@Column(name = "user_name", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "user_fb_id", unique = false, nullable = true, insertable = true, updatable = true, length = 45)
	public String getUserFbId() {
		return userFbId;
	}

	public void setUserFbId(String userFbId) {
		this.userFbId = userFbId;
	}

	
	@Column(name = "user_email", unique = false, nullable = true, insertable = true, updatable = true, length = 100)
	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	@Column(name= "user_pass", unique = false, nullable = true, insertable = true, updatable = true, length = 40)
	public String getUserPass() {
		return userPass;
	}

	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}

}
