package com.leitmotiff.persistence.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.leitmotiff.persistence.entity.Product;

@Repository("productDao")
public class ProductDaoImpl  extends GenericDaoImpl<Product> implements ProductDao {
	
	protected ProductDaoImpl() {
		super(Product.class);
	}
	
	@Transactional
	public List<Product> getProductsOrderPrice(Long idCategory, boolean order){
		String ord = "ASC";
		if (!order) {
			ord = "DESC";
		}
		
		return (List<Product>) getCurrentSession().createQuery("select t from Product as t where category = "
						+ idCategory + " order by productPrice " + ord).list();
	}
	
	@Transactional
	public List<Product> getProductOrderValue(Long idCategory, boolean order){
		String ord = "asc";
		if (!order) {
			ord = "desc";
		}
		return (List<Product>) getCurrentSession().createQuery("select t from Product as t where category = "
				+ idCategory + " order by (productPositive - productNegative) " + ord).list();
	}
	
	public List<Product> getSearchedProduct_1_Init(String search){
		
		return  getCurrentSession().createQuery("select t from Product as t where productName like '" + search + "%'").list();		
	}
	
	public List<Product> getSearchedProduct_2_Tokenizer(String search){
		
		String consult;
		consult = "select t from Product as t where productName like '%" + search + "%'";
		StringTokenizer sentenceSearch = new StringTokenizer(search);
		List<Product> returnList = new ArrayList<Product>();
		if(sentenceSearch.countTokens() > 1){
			List<Product> productList;
			while(sentenceSearch.hasMoreElements()){
				String word = sentenceSearch.nextElement().toString();
				consult = consult.concat(" and productName like '%" + word + "%'");
			}
			productList = getCurrentSession().createQuery(consult).list();
			for(Product product : productList){
				returnList.add(product);
			}
		}
		return returnList;
	}
	
	public List<Product> getSearchedProduct_3_Count(String search){
		
		StringTokenizer sentenceSearch = new StringTokenizer(search);
		List<Product> returnList = new ArrayList<Product>();
		
		if(sentenceSearch.countTokens() > 1){
			List<Product> productList;
			String word = null;
			int contador = 1;
			while(sentenceSearch.hasMoreElements()){
				sentenceSearch = new StringTokenizer(search);
				for(int i = 0; i<contador;i++){	
					word = sentenceSearch.nextElement().toString();
				}
				while(sentenceSearch.hasMoreElements()){
					String nextWord = sentenceSearch.nextElement().toString();
					productList = getCurrentSession().createQuery("select t from Product as t where productName like '%" + word + "%' and productName like '%" + nextWord + "%'").list();
					for(Product product : productList){
						returnList.add(product);
					}
				}
				contador++;
			}
			
		}
		return returnList;
		
	}
	
	public List<Product> getSearchedProduct_4_TokenizerInit(String search){
		
		StringTokenizer sentenceSearch = new StringTokenizer(search);
		List<Product> returnList = new ArrayList<Product>();
		while(sentenceSearch.hasMoreElements()){
			String word = sentenceSearch.nextElement().toString();
			List<Product> productList = getCurrentSession().createQuery("select t from Product as t where productName like '" + word + "%'").list();
			for(Product product : productList){
					returnList.add(product);
			}
		}
		return returnList;
	}
	
	public List<Product> getSearchedProduct_5_Contiene(String search){
		 
		return getCurrentSession().createQuery("select t from Product as t where productName like '%" + search + "%' and not productName like '" + search + "%'").list();
	}
	
	public List<Product> getSearchedProduct_6_(String search){
		
		StringTokenizer sentenceSearch = new StringTokenizer(search);
		List<Product> returnList = new ArrayList<Product>();
		while(sentenceSearch.hasMoreElements()){
			String word = sentenceSearch.nextElement().toString();
			List<Product> productList = getCurrentSession().createQuery("select t from Product as t where productName like '%" + word + "%' and not productName like '" + word + "%'").list();
			for(Product product : productList){
					returnList.add(product);
			}
		}
		return returnList;
	}
		
}