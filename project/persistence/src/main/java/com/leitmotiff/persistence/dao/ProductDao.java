package com.leitmotiff.persistence.dao;

import java.util.List;

import com.leitmotiff.persistence.entity.Product;

public interface ProductDao extends GenericDao<Product> {

	List<Product> getProductsOrderPrice(Long idCategory, boolean order);
	
	List<Product> getProductOrderValue(Long idCategory, boolean order);

	List<Product> getSearchedProduct_1_Init(String search);
	
	List<Product> getSearchedProduct_2_Tokenizer(String search);
	
	List<Product> getSearchedProduct_3_Count(String search);
	
	List<Product> getSearchedProduct_4_TokenizerInit(String search);
	
	List<Product> getSearchedProduct_5_Contiene(String search);
	
	List<Product> getSearchedProduct_6_(String search);
	
}