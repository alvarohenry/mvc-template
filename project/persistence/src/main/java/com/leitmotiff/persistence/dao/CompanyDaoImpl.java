package com.leitmotiff.persistence.dao;

import org.springframework.stereotype.Repository;

import com.leitmotiff.persistence.entity.Company;

@Repository("companyDao")
public class CompanyDaoImpl extends GenericDaoImpl<Company> implements CompanyDao {
	
	protected CompanyDaoImpl() {
		super(Company.class);
	}
	
}
