package com.leitmotiff.persistence.dao;

import java.util.List;

import com.leitmotiff.persistence.entity.Product;
import com.leitmotiff.persistence.entity.Services;

public interface ServiceDao extends GenericDao<Services>{
	
	List<Services> getServiceOrderPrice(Long idCategory, boolean order);
	
	List<Services> getServiceOrderValue(Long idCategory, boolean order);
	
	List<Services> getSearchedService_1_Init(String search);
	
	List<Services> getSearchedServices_2_Tokenizer(String search);
	
	List<Services> getSearchedService_3_Count(String search);
	
	List<Services> getSearchedService_4_TokenizerInit(String search);
	
	List<Services> getSearchedServices_5_Contiene(String search);
	
	List<Services> getSearchedProduct_6_(String search);
	
}
