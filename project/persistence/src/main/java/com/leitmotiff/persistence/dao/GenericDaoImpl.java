package com.leitmotiff.persistence.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@SuppressWarnings({"unchecked", "rawtypes"})
public class GenericDaoImpl <T extends Serializable> implements	GenericDao<T> {

	@Autowired
	private SessionFactory sessionFactory;
	
	private Class<T> entity;
	
	protected int maxResults;
	
	protected int actualResult;
	
	protected boolean polarity;
	
	protected Criterion criterion;
	
	protected GenericDaoImpl(final Class<T> entity){
		this.entity = entity;
		this.actualResult = 0;
	}

	public void setEntity(Class<T> entity) {
		this.entity = entity;
	}
	
	public Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Transactional
	public T getById(Long id) {
		return (T) getCurrentSession().get(entity, id);
	}

	@Transactional
	public List<T> findAll() {
		return (List<T>) getCurrentSession().createQuery("from " + entity.getSimpleName()).list();
	}

	@Transactional
	public List<T> findAll(int maxResults) {
		this.actualResult = 0;
		this.maxResults = maxResults;
		Query limitedQuery;
		limitedQuery = getCurrentSession().createQuery("from " + entity.getSimpleName());
		limitedQuery.setFirstResult(actualResult);
		limitedQuery.setMaxResults(this.maxResults);
		polarity = false;
		return (List<T>) limitedQuery.list();
		
	}
	
	@Transactional
	public void saveOrUpdate(T entity) {
		getCurrentSession().saveOrUpdate(entity);	
	}

	@Transactional
	public void delete(T entity) {
		getCurrentSession().delete(entity);		
	}
	
	@Transactional
    public List<T> findByCriteria(Criterion criterion) {
        Criteria criteria = getCurrentSession().createCriteria(entity);
        criteria.add(criterion);
        return criteria.list();
    }

	@Transactional
	public List<T> findByCriteria(Criterion criterion, int maxResults){
		this.actualResult = 0;
		this.maxResults = maxResults;
		this.criterion = criterion;
		Criteria limitedCriteria;
		limitedCriteria = getCurrentSession().createCriteria(entity);
		limitedCriteria.add(this.criterion);
		limitedCriteria.setFirstResult(this.actualResult);
		limitedCriteria.setMaxResults(this.maxResults);		
		polarity = true;
		List<T> list = limitedCriteria.list();
		return limitedCriteria.list();
	}
	
	@Transactional
	public List getMore() {
		this.actualResult += this.maxResults;
		if(!polarity){
			Query limitedQuery;
			limitedQuery = getCurrentSession().createQuery("from " + entity.getSimpleName());
			limitedQuery.setFirstResult(actualResult);
			limitedQuery.setMaxResults(this.maxResults);
			return (List<T>)  limitedQuery.list();
		}
		else{
			Criteria limitedCriteria;
			limitedCriteria = getCurrentSession().createCriteria(entity);
			limitedCriteria.add(this.criterion);
			limitedCriteria.setFirstResult(this.actualResult);
			limitedCriteria.setMaxResults(this.maxResults);
			return (List<T>) limitedCriteria.list();
		}
	}
}
