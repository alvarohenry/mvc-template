package com.leitmotiff.business.product;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leitmotiff.business.product.dto.ProductDTO;
import com.leitmotiff.business.util.UtilBusiness;
import com.leitmotiff.persistence.dao.ProductDao;
import com.leitmotiff.persistence.dao.ServiceDao;
import com.leitmotiff.persistence.dao.ServiceHasProductDao;
import com.leitmotiff.persistence.entity.Product;



@Service("product")
public class ProductManagerImpl	implements ProductManager {

	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private ServiceHasProductDao serviceDao;
	
	public List<ProductDTO> getProducts(Long idCategory) {
		Criterion criterionIdCategory = Restrictions.eq("category.id",idCategory);
		return mappingList(productDao.findByCriteria(criterionIdCategory));
	}
	
	public List<ProductDTO> getProducts(Long idCategory, int maxResults){
		Criterion criterionIdCategory = Restrictions.eq("category.id",idCategory);
		return mappingList(productDao.findByCriteria(criterionIdCategory,maxResults));
	}
	
	public List<ProductDTO> getProductsOrderPrice(Long idCategory, boolean order) {
		return mappingList(productDao.getProductsOrderPrice(idCategory, order));
	}

	public List<ProductDTO> getProductsOrderValue(Long idCategory, boolean order) {
		return mappingList(productDao.getProductOrderValue(idCategory, order));
	}
		
	public ProductDTO getProduct(Long id){
		
		
		return (UtilBusiness.mappingDTO(productDao.getById(id),serviceDao.getMinurService(id)));
		
	}
	
	public List<ProductDTO> getMoreProducts(){
		return mappingList(productDao.getMore());
	}
	
	public List<ProductDTO> mappingList (List<Product> listProduct){
		List<ProductDTO> listaDto = new ArrayList<ProductDTO>();
		for (Product product : listProduct){
			listaDto.add(UtilBusiness.mappingDTO(product,serviceDao.getMinurService(product.getProductId())));
		}
		return listaDto;
	}
}