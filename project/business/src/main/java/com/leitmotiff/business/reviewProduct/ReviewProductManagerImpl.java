package com.leitmotiff.business.reviewProduct;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leitmotiff.business.review.dto.ReviewProductDTO;
import com.leitmotiff.business.util.UtilBusiness;
import com.leitmotiff.persistence.dao.ReviewProductDao;
import com.leitmotiff.persistence.entity.Reviewproduct;

@Service("reviewProduct")
public class ReviewProductManagerImpl implements ReviewProductManager {

	@Autowired
	private ReviewProductDao reviewProductDao;
	
	@Transactional
	public List<ReviewProductDTO> getReviews(Long id, int maxResults){
		Criterion criterion = Restrictions.eq("product.id",id);
		return mappingList(reviewProductDao.findByCriteria(criterion,maxResults));		
	}
	
	@Transactional
	public List<ReviewProductDTO> getMore(){	
		return mappingList(reviewProductDao.getMore());
	}
	
	@Transactional
	public void insertReview(String review){
		ReviewProductDTO reviewDto = new ReviewProductDTO();
		reviewDto.setBody(review);
		reviewProductDao.saveOrUpdate(UtilBusiness.mapping(reviewDto));
	}
	
	@Transactional
	public List<Integer> numberReview(long id){
		return reviewProductDao.numberComentary(id);
	}
	
	public List<ReviewProductDTO> mappingList (List listReview){
		List<ReviewProductDTO> listaDto = new ArrayList<ReviewProductDTO>();
		ReviewProductDTO reviewDto = new ReviewProductDTO();
		List<Reviewproduct> list = listReview;
		for (Reviewproduct review : list){
			listaDto.add(UtilBusiness.mappingDTO(review));
		}
		return listaDto;
	}
}