package com.leitmotiff.business.companyHasProduct.dto;

public class CompanyHasProductDTO {

	private String url;
	
	private boolean state;
	
	private Double price;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
}