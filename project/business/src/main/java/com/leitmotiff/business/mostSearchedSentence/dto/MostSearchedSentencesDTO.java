package com.leitmotiff.business.mostSearchedSentence.dto;


public class MostSearchedSentencesDTO {

	private Long id;
	
	private String sentence;
	
	private Long value;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSentence() {
		return sentence;
	}

	public void setSentence(String sentence) {
		this.sentence = sentence;
	}

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}
}