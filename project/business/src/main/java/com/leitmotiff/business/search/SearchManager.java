package com.leitmotiff.business.search;

import java.util.List;

import com.leitmotiff.business.search.dto.SearchDTO;

public interface SearchManager {

	List<SearchDTO> getSearched(String search, int maxResults);
	
	List<SearchDTO> getMore();
	
}
