package com.leitmotiff.business.dictionary.dto;


public class DictionaryDTO {

	private long Id;
	
	private String Word;

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getWord() {
		return Word;
	}

	public void setWord(String word) {
		Word = word;
	}
	
}
