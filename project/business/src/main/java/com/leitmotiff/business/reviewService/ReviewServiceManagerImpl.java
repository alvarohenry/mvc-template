package com.leitmotiff.business.reviewService;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leitmotiff.business.review.dto.ReviewServiceDTO;
import com.leitmotiff.business.util.UtilBusiness;
import com.leitmotiff.persistence.dao.ReviewServiceDao;
import com.leitmotiff.persistence.entity.Reviewservice;

@Service("reviewService")
public class ReviewServiceManagerImpl implements ReviewServiceManager {

	@Autowired
	private ReviewServiceDao reviewServiceDao;
	
	public List<ReviewServiceDTO> getReviews(Long id, int maxResults){
		Criterion criterion = Restrictions.eq("service.serviceId",id);
		List<ReviewServiceDTO> list = mappingList(reviewServiceDao.findByCriteria(criterion,maxResults));
		System.out.println("HOOOOOOOLAAAAAAAAAA");
		for(ReviewServiceDTO dto : list){
			System.out.println(dto.getTitle());
		}
		return list;
	}
	
	public List<ReviewServiceDTO> getMore(){
		return mappingList(reviewServiceDao.getMore());
	}
	
	public void insertReview(String review){
		ReviewServiceDTO reviewDto = new ReviewServiceDTO();
		reviewDto.setBody(review);
		reviewServiceDao.saveOrUpdate(UtilBusiness.mapping(reviewDto));
	}
	
	public List<Integer> numberReviews(long id){
		return reviewServiceDao.numberComentary(id);
	}
	
	public List<ReviewServiceDTO> mappingList (List<Reviewservice> listReview){
		List<ReviewServiceDTO> listaDto = new ArrayList<ReviewServiceDTO>();
		for (Reviewservice review : listReview){
			listaDto.add(UtilBusiness.mappingDTO(review));
		}
		return listaDto;
	}	
}
