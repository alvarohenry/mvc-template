package com.leitmotiff.business.util;

import java.text.SimpleDateFormat;

import org.springframework.transaction.annotation.Transactional;

import com.leitmotiff.business.category.dto.CategoryDTO;
import com.leitmotiff.business.company.dto.CompanyDTO;
import com.leitmotiff.business.companyHasProduct.dto.CompanyHasProductDTO;
import com.leitmotiff.business.dictionary.dto.DictionaryDTO;
import com.leitmotiff.business.mostSearchedSentence.dto.MostSearchedSentencesDTO;
import com.leitmotiff.business.product.dto.ProductDTO;
import com.leitmotiff.business.review.dto.ReviewProductDTO;
import com.leitmotiff.business.review.dto.ReviewServiceDTO;
import com.leitmotiff.business.service.dto.ServiceDTO;
import com.leitmotiff.business.serviceHasProduct.dto.ServiceHasProductDTO;
import com.leitmotiff.business.user.dto.UserDTO;
import com.leitmotiff.persistence.entity.Category;
import com.leitmotiff.persistence.entity.Company;
import com.leitmotiff.persistence.entity.CompanyHasProduct;
import com.leitmotiff.persistence.entity.Dictionary;
import com.leitmotiff.persistence.entity.MostSearchedSentences;
import com.leitmotiff.persistence.entity.Product;
import com.leitmotiff.persistence.entity.Reviewproduct;
import com.leitmotiff.persistence.entity.Reviewservice;
import com.leitmotiff.persistence.entity.ServiceHasProduct;
import com.leitmotiff.persistence.entity.Services;
import com.leitmotiff.persistence.entity.User;


public class UtilBusiness {


	public static String tokenRemplace(String token){ 
		
		String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ°!#$%&/()=?¡'¿´¨+*~{[^}]`,;.:-_\"";
		
	    String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
	    
	    String resultToken = token;
	    
	    for (int i=0; i<original.length(); i++) {
			resultToken = resultToken.replace(original.charAt(i), ascii.charAt(i));
		}
	    
	    return resultToken;	
	}
	
	public static Category mapping(CategoryDTO category){
		Category productCategory = new Category();
		productCategory.setCategoryId(category.getId());
		productCategory.setCategoryName(category.getName());
		productCategory.setCategoryImagePath(category.getImage());
		productCategory.setCategoryPatternId(category.getPatternId());
		productCategory.setCategoryTopId(category.getTopId());
		return productCategory;
	}
	
	public static CategoryDTO mappingDTO(Category productCategory){
		CategoryDTO category = new CategoryDTO();
		category.setName(productCategory.getCategoryName());
		category.setId(productCategory.getCategoryId());
		category.setImage(productCategory.getCategoryImagePath());
		category.setPatternId(productCategory.getCategoryPatternId());
		category.setTopId(productCategory.getCategoryTopId());
		category.setPolarity(productCategory.isCategoryPolarity());
		return category;
	}
	
	@Transactional
	public static CompanyDTO mappingDTO(Company company){
		CompanyDTO companyDto = new CompanyDTO();
		companyDto.setId(company.getCompanyId());
		companyDto.setDescription(company.getCompanyDescription());
		companyDto.setImage(company.getCompanyImagePath());
		companyDto.setName(company.getCompanyName());
		companyDto.setUrl(company.getCompanyUrl());
		return companyDto;
	}
	
	public static CompanyHasProductDTO mappingDTO(CompanyHasProduct company){
		CompanyHasProductDTO companyDTO = new CompanyHasProductDTO();
		companyDTO.setPrice(company.getCompanyHasProductPrice());
		companyDTO.setState(company.isCompanyHasProductState());
		companyDTO.setUrl(company.getCompanyHasProductUrl());
		return companyDTO;
	}
	
	public static DictionaryDTO mappingDTO(Dictionary dictionary){
		DictionaryDTO dictionaryDto = new DictionaryDTO();
		dictionaryDto.setId(dictionary.getDictionayId());
		dictionaryDto.setWord(dictionary.getDictionaryWord());
		return dictionaryDto;
	}
	
	public static Dictionary mapping(DictionaryDTO dictionaryDto){
		Dictionary dictionary = new Dictionary();
		dictionary.setDictionayId(dictionaryDto.getId());
		dictionary.setDictionaryWord(dictionary.getDictionaryWord());
		return dictionary;
	}
	
	public static MostSearchedSentences mapping(MostSearchedSentencesDTO sentenceDTO){
		MostSearchedSentences sentence = new MostSearchedSentences();
		sentence.setMostSearchedSentencesValue(sentenceDTO.getValue());
		sentence.setMostSearchedSentencesId(sentenceDTO.getId());
		sentence.setMostSearchedSentencesSentence(sentenceDTO.getSentence());
		return sentence;
	}
	
	public static MostSearchedSentencesDTO mappingDTO(MostSearchedSentences sentence){
		MostSearchedSentencesDTO sentenceDTO = new MostSearchedSentencesDTO();
		sentenceDTO.setValue(sentence.getMostSearchedSentencesValue());
		sentenceDTO.setId(sentence.getMostSearchedSentencesId());
		sentenceDTO.setSentence(sentence.getMostSearchedSentencesSentence());
		return sentenceDTO;
	}
	
	@Transactional
	public static ProductDTO mappingDTO(Product product,ServiceHasProduct services) {
		ProductDTO productDto = new ProductDTO();
		productDto.setId(product.getProductId());
		productDto.setName(product.getProductName());
		productDto.setDescription(product.getProductDescription());
		productDto.setImagePath(product.getProductImagePath());
		productDto.setPositive(product.getProductPositive());
		productDto.setNegative(product.getProductNegative());
		if(product.getProductMemory() != null){
			productDto.setMemory(product.getProductMemory());
		}
		if(product.getProductRam() != null)
		{
			productDto.setRam(product.getProductRam());
		}
		productDto.setProcessor(product.getProductProcessor());
		if(product.getProductWeight() != null){
			productDto.setWeigth(product.getProductWeight());
		}
		productDto.setBattery(product.getProductBattery());
		productDto.setExtraFeature(product.getProductExtraFeature());
				
		if(services != null){
			productDto.setMinurPrice(services.getServiceHasProductPrice());
		}
		
		
		return productDto;

	}

	public static ProductDTO mappingDTO(Product product) {
		ProductDTO productDto = new ProductDTO();
		productDto.setId(product.getProductId());
		productDto.setName(product.getProductName());
		productDto.setDescription(product.getProductDescription());
		productDto.setImagePath(product.getProductImagePath());
		productDto.setPositive(product.getProductPositive());
		productDto.setNegative(product.getProductNegative());
		if(product.getProductMemory() != null){
			productDto.setMemory(product.getProductMemory());
		}
		if(product.getProductRam() != null)
		{
			productDto.setRam(product.getProductRam());
		}
		productDto.setProcessor(product.getProductProcessor());
		if(product.getProductWeight() != null){
			productDto.setWeigth(product.getProductWeight());
		}
		productDto.setBattery(product.getProductBattery());
		productDto.setExtraFeature(product.getProductExtraFeature());
		
		return productDto;

	}
	
	
	public static Product mapping(ProductDTO productDto) {

		Product product = new Product();
		product.setProductId(productDto.getId());
		product.setProductName(productDto.getName());
		product.setProductDescription(productDto.getDescription());
		product.setProductImagePath(productDto.getImagePath());
		product.setProductPositive(productDto.getPositive());
		product.setProductNegative(product.getProductNegative());
		product.setProductMemory(productDto.getMemory());
		product.setProductRam(productDto.getRam());
		product.setProductProcessor(productDto.getProcessor());
		product.setProductWeight(productDto.getWeigth());
		product.setProductBattery(productDto.getBattery());
		product.setProductExtraFeature(productDto.getExtraFeature());
		return product;
	}
	
	public static ReviewServiceDTO mappingDTO(Reviewservice review){
		ReviewServiceDTO reviewDTO = new ReviewServiceDTO();
		reviewDTO.setId(review.getReviewServiceId());
		reviewDTO.setBody(review.getReviewServiceBody());
		reviewDTO.setPolarity(review.getReviewServicePolarity());
		reviewDTO.setTitle(review.getReviewServiceTitle());
		reviewDTO.setUrl(review.getReviewServiceUrl());
		if(review.getUser() !=null){
			reviewDTO.setUserName(review.getUser().getUserName());
		}
		if(review.getReviewServiceDate() != null){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd");
			reviewDTO.setDate(sdf.format(review.getReviewServiceDate().getTime()));
		}
		return reviewDTO;
	}
	
	public static Reviewservice mapping(ReviewServiceDTO reviewDto){
		Reviewservice review = new Reviewservice();
		review.setReviewServiceId(reviewDto.getId());
		review.setReviewServiceBody(reviewDto.getBody());
		review.setReviewServiceTitle(reviewDto.getTitle());
		review.setReviewServicePolarity(reviewDto.getPolarity());
		review.setReviewServiceUrl(reviewDto.getUrl());
		return review;
	}

	public static Reviewproduct mapping(ReviewProductDTO reviewDto){
		Reviewproduct review = new Reviewproduct();
		review.setReviewProductId(reviewDto.getId());
		review.setReviewProductTitle(reviewDto.getTitle());
		review.setReviewProductBody(reviewDto.getBody());
		review.setReviewProductPolarity(reviewDto.getPolarity());
		review.setReviewProductUrl(reviewDto.getUrl());
		return review;
	}
	
	public static ReviewProductDTO mappingDTO(Reviewproduct review){
		ReviewProductDTO reviewDto = new ReviewProductDTO();
		reviewDto.setId(review.getReviewProductId());
		reviewDto.setTitle(review.getReviewProductTitle());
		reviewDto.setBody(review.getReviewProductBody());
		reviewDto.setPolarity(review.getReviewProductPolarity());
		reviewDto.setUrl(review.getReviewProductUrl());
		if(review.getUser() != null){
			reviewDto.setUserName(review.getUser().getUserName());
		}
		if(review.getReviewProductDate() != null){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd");
			reviewDto.setDate(sdf.format(review.getReviewProductDate().getTime()));
		}
		return reviewDto;
	}

	@Transactional
	public static ServiceDTO mappingDTO(Services service){
		ServiceDTO serviceDto = new ServiceDTO();
		//CompanyDTO companyDto = mappingDTO(service.getCompany());
		serviceDto.setId(service.getServiceId());
		serviceDto.setName(service.getServiceName());
		serviceDto.setDescription(service.getServiceDescription());
		serviceDto.setPrice(service.getServicePrice());
		if(service.getServicePhoneMb() != null){
			serviceDto.setPhoneMb(service.getServicePhoneMb());
		}
		if(service.getServicePhoneMinAll() != null){
			serviceDto.setPhoneMinAll(service.getServicePhoneMinAll());
		}
		serviceDto.setPhoneExtra(service.getServicePhoneExtra());
		serviceDto.setTvChannel(service.getServiceTvChannel());
		serviceDto.setTvExtra(service.getServiceTvExtra());
		serviceDto.setPositive(service.getServicePositive());
		serviceDto.setNegative(service.getServiceNegative());
		serviceDto.setImage(service.getServiceImagePath());
		serviceDto.setUrl(service.getServiceUrl());
		serviceDto.setState(service.isServiceState());
		
		
		//serviceDto.setIdCompany(service.getCompany().getCompanyId());
		//serviceDto.setCompanyName(service.getCompany().getCompanyName());
//		serviceDto.setCompanyImage(service.getCompany().getCompanyImagePath());
		
		
		
		
//		List<ServiceHasProduct> listService = new ArrayList<ServiceHasProduct>();
//		List<ServiceHasProductDTO> listServiceDTO = new ArrayList<ServiceHasProductDTO>();
//		List<Reviewservice> listReview = new ArrayList<Reviewservice>();
//		List<ReviewServiceDTO> listReviewDTO = new ArrayList<ReviewServiceDTO>();
//		listReview.addAll(service.getReviewservices());
//		for(Reviewservice review : listReview){
//			listReviewDTO.add(mappingDTO(review));
//		}
//		serviceDto.setReviewService(listReviewDTO);
//		listService.addAll(service.getServiceHasProducts());
//		for(ServiceHasProduct product : listService){
//			listServiceDTO.add(mappingDTO(product));
//		}
//		serviceDto.setServiceHasProduct(listServiceDTO);
		return serviceDto;
	}

	public static ServiceHasProductDTO mappingDTO(ServiceHasProduct sHP){
		ServiceHasProductDTO sHPDto = new ServiceHasProductDTO();
		ServiceDTO serviceDto = mappingDTO(sHP.getService());
		ProductDTO productDto = mappingDTO(sHP.getProduct());
		sHPDto.setServiceId(serviceDto.getId());
		sHPDto.setServiceName(serviceDto.getName());
		sHPDto.setServiceDescription(serviceDto.getDescription());
		sHPDto.setServicePrice(serviceDto.getPrice());
		sHPDto.setServicePhoneMb(serviceDto.getPhoneMb());
		sHPDto.setServicePhoneMinAll(serviceDto.getPhoneMinAll());
		sHPDto.setServicePhoneExtra(serviceDto.getPhoneExtra());
		sHPDto.setServiceTvChannel(serviceDto.getTvChannel());
		sHPDto.setServiceTvExtra(serviceDto.getTvExtra());
		sHPDto.setServicePositive(serviceDto.getPositive());
		sHPDto.setServiceNegative(serviceDto.getNegative());
		sHPDto.setServiceImage(serviceDto.getImage());
		sHPDto.setServiceUrl(serviceDto.getUrl());
		sHPDto.setState(serviceDto.isState());
		
		sHPDto.setProductId(sHP.getProduct().getProductId());
		sHPDto.setProductName(sHP.getProduct().getProductName());
		sHPDto.setProductBrand(productDto.getBrand());
		sHPDto.setProductDimension(productDto.getDimension());
		sHPDto.setProductOperatingSystem(productDto.getOperatingSystem());
		sHPDto.setProductImage(productDto.getImagePath());
		sHPDto.setProductDescription(productDto.getDescription());
		sHPDto.setProductPositive(productDto.getPositive());
		sHPDto.setProductNegative(productDto.getNegative());
		sHPDto.setProductMemory(productDto.getMemory());
		sHPDto.setProductRam(productDto.getRam());
		sHPDto.setProductWeigth(productDto.getWeigth());
		sHPDto.setProductProcessor(productDto.getProcessor());
		sHPDto.setProductBattery(productDto.getBattery());
		sHPDto.setProductExtraFeature(productDto.getExtraFeature());
		
		sHPDto.setExtra(sHP.getServiceHasProductExtra());
		sHPDto.setUrl(sHP.getServiceHasProductUrl());
		sHPDto.setPrice(sHP.getServiceHasProductPrice());
		sHPDto.setState(sHP.isServiceHasProductState());
		return sHPDto;
	}
	
	public static User mapping(UserDTO userDto){
		User user = new User();
		user.setUserId(userDto.getId());
		user.setUserName(userDto.getName());
		user.setUserFbId(userDto.getFbId());
		user.setUserEmail(userDto.getEmail());
		user.setUserPass(userDto.getPass());
		return user;
	}
	
	public static UserDTO mappingDTO(User user){
		UserDTO userDto = new UserDTO();
		userDto.setId(user.getUserId());
		userDto.setName(user.getUserName());
		userDto.setEmail(user.getUserEmail());
		userDto.setFbId(user.getUserFbId());
		userDto.setPass(user.getUserPass());
		return userDto;
	}

}
