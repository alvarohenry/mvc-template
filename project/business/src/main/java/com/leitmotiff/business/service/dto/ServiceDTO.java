package com.leitmotiff.business.service.dto;

import java.util.ArrayList;
import java.util.List;

import com.leitmotiff.business.review.dto.ReviewServiceDTO;
import com.leitmotiff.business.serviceHasProduct.dto.ServiceHasProductDTO;
import com.leitmotiff.persistence.entity.Category;
import com.leitmotiff.persistence.entity.Company;
import com.leitmotiff.persistence.entity.Reviewservice;
import com.leitmotiff.persistence.entity.Services;
import com.leitmotiff.persistence.entity.ServiceHasProduct;

public class ServiceDTO {

	private long id;
	
	private String name;
	
	private String description;
	
	private double price;
	
	private int phoneMb;
	
	private int phoneMinAll;
	
	private String phoneExtra;
	
	private String tvChannel;
	
	private String tvExtra;
	
	private long positive;
	
	private long negative;
	
	private String image;
	
	private String url;
	
	private boolean state;
	
	private boolean polarity = true;
	
	private long idCompany;
	
	private String companyName;
	
	private String companyImage;
	
//	private List<ServiceHasProductDTO> serviceHasProduct;
//	
//	private List<ReviewServiceDTO> reviewService;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getPhoneMb() {
		return phoneMb;
	}

	public void setPhoneMb(int phoneMb) {
		this.phoneMb = phoneMb;
	}

	public int getPhoneMinAll() {
		return phoneMinAll;
	}

	public void setPhoneMinAll(int phoneMinAll) {
		this.phoneMinAll = phoneMinAll;
	}

	public String getPhoneExtra() {
		return phoneExtra;
	}

	public void setPhoneExtra(String phoneExtra) {
		this.phoneExtra = phoneExtra;
	}

	public String getTvChannel() {
		return tvChannel;
	}

	public void setTvChannel(String tvChannel) {
		this.tvChannel = tvChannel;
	}

	public String getTvExtra() {
		return tvExtra;
	}

	public void setTvExtra(String tvExtra) {
		this.tvExtra = tvExtra;
	}

	public long getPositive() {
		return positive;
	}

	public void setPositive(long positive) {
		this.positive = positive;
	}

	public long getNegative() {
		return negative;
	}

	public void setNegative(long negative) {
		this.negative = negative;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}
	
//	public List<ServiceHasProductDTO> getServiceHasProduct() {
//		return serviceHasProduct;
//	}
//
//	public void setServiceHasProduct(List<ServiceHasProductDTO> serviceHasProduct) {
//		this.serviceHasProduct = serviceHasProduct;
//	}
//
//	public List<ReviewServiceDTO> getReviewService() {
//		return reviewService;
//	}
//
//	public void setReviewService(List<ReviewServiceDTO> reviewService) {
//		this.reviewService = reviewService;
//	}

	public boolean isPolarity() {
		return polarity;
	}

	public void setPolarity(boolean polarity) {
		this.polarity = polarity;
	}

	public long getIdCompany() {
		return idCompany;
	}

	public void setIdCompany(long idCompany) {
		this.idCompany = idCompany;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyImage() {
		return companyImage;
	}

	public void setCompanyImage(String companyImage) {
		this.companyImage = companyImage;
	}
}
