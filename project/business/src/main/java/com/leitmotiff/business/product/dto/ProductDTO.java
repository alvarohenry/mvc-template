package com.leitmotiff.business.product.dto;

import java.util.List;

import com.leitmotiff.business.companyHasProduct.dto.CompanyHasProductDTO;
import com.leitmotiff.business.review.dto.ReviewProductDTO;
import com.leitmotiff.business.serviceHasProduct.dto.ServiceHasProductDTO;

public class ProductDTO {
	
	private Long id;
	
	private String name;
	
	private String brand;
	
	private String dimension;
	
	private String operatingSystem;
	
	private String image;
	
	private String description;
	
	private Long positive;
	
	private Long negative;

	private float memory;
	
	private float ram;
	
	private String processor;
	
	private float weigth;
	
	private String battery;
	
	private String extraFeature;
	
	private boolean polarity = false;
	
	private double minurPrice;
	
	private long minurServiceId;
	
	private String minurServiceName;
	
	//private String minurServiceCompany;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImagePath() {
		return image;
	}
	public void setImagePath(String imagePath) {
		this.image = imagePath;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getPositive() {
		return positive;
	}
	public void setPositive(Long positive) {
		this.positive = positive;
	}
	public Long getNegative() {
		return negative;
	}
	public void setNegative(Long negative) {
		this.negative = negative;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getDimension() {
		return dimension;
	}
	public void setDimension(String dimension) {
		this.dimension = dimension;
	}
	public String getOperatingSystem() {
		return operatingSystem;
	}
	public void setOperatingSystem(String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}
	public float getMemory() {
		return memory;
	}
	public void setMemory(float memory) {
		this.memory = memory;
	}
	public float getRam() {
		return ram;
	}
	public void setRam(float ram) {
		this.ram = ram;
	}
	public String getProcessor() {
		return processor;
	}
	public void setProcessor(String processor) {
		this.processor = processor;
	}
	public float getWeigth() {
		return weigth;
	}
	public void setWeigth(float weigth) {
		this.weigth = weigth;
	}
	public String getBattery() {
		return battery;
	}
	public void setBattery(String battery) {
		this.battery = battery;
	}
	public String getExtraFeature() {
		return extraFeature;
	}
	public void setExtraFeature(String extraFeature) {
		this.extraFeature = extraFeature;
	}
	public boolean isPolarity() {
		return polarity;
	}
	public void setPolarity(boolean polarity) {
		this.polarity = polarity;
	}
	public double getMinurPrice() {
		return minurPrice;
	}
	public void setMinurPrice(double minurPrice) {
		this.minurPrice = minurPrice;
	}
	public long getMinurServiceId() {
		return minurServiceId;
	}
	public void setMinurServiceId(long minurServiceId) {
		this.minurServiceId = minurServiceId;
	}
	public String getMinurServiceName() {
		return minurServiceName;
	}
	public void setMinurServiceName(String minurServiceName) {
		this.minurServiceName = minurServiceName;
	}
//	public String getMinurServiceCompany() {
//		return minurServiceCompany;
//	}
//	public void setMinurServiceCompany(String minurServiceCompany) {
//		this.minurServiceCompany = minurServiceCompany;
//	}
}
