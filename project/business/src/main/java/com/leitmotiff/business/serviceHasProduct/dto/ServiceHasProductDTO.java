package com.leitmotiff.business.serviceHasProduct.dto;

import com.leitmotiff.business.product.dto.ProductDTO;
import com.leitmotiff.business.service.dto.ServiceDTO;


public class ServiceHasProductDTO {
	
	/*Service*/
	
	private long serviceId;
	
	private String serviceName;
	
	private String serviceDescription;
	
	private double servicePrice;
	
	private int servicePhoneMb;
	
	private int servicePhoneMinAll;
	
	private String servicePhoneExtra;
	
	private String serviceTvChannel;
	
	private String serviceTvExtra;
	
	private long servicePositive;
	
	private long serviceNegative;
	
	private String serviceImage;
	
	private String serviceUrl;
	
	private boolean serviceState;
	
	private boolean servicePolarity = true;
	
//	private ServiceDTO service;
	
	/*Product*/
	
//	private ProductDTO product;
	
	private Long productId;
	
	private String productName;
	
	private String productBrand;
	
	private String productDimension;
	
	private String productOperatingSystem;
	
	private String productImage;
	
	private String productDescription;
	
	private Long productPositive;
	
	private Long productNegative;

	private float productMemory;
	
	private float productRam;
	
	private String productProcessor;
	
	private float productWeigth;
	
	private String productBattery;
	
	private String productExtraFeature;
	
	private boolean productPolarity = false;


	//ServiceHasProduct

	private Double price;
	
	private String extra;
	
	private String url;

	private boolean state;

	
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}



	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	public double getServicePrice() {
		return servicePrice;
	}

	public void setServicePrice(double servicePrice) {
		this.servicePrice = servicePrice;
	}


	public String getServiceUrl() {
		return serviceUrl;
	}

	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}

	public boolean isServiceState() {
		return serviceState;
	}

	public void setServiceState(boolean serviceState) {
		this.serviceState = serviceState;
	}

	public long getServiceId() {
		return serviceId;
	}

	public void setServiceId(long serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceDescription() {
		return serviceDescription;
	}

	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}

	public int getServicePhoneMb() {
		return servicePhoneMb;
	}

	public void setServicePhoneMb(int servicePhoneMb) {
		this.servicePhoneMb = servicePhoneMb;
	}

	public int getServicePhoneMinAll() {
		return servicePhoneMinAll;
	}

	public void setServicePhoneMinAll(int servicePhoneMinAll) {
		this.servicePhoneMinAll = servicePhoneMinAll;
	}

	public String getServicePhoneExtra() {
		return servicePhoneExtra;
	}

	public void setServicePhoneExtra(String servicePhoneExtra) {
		this.servicePhoneExtra = servicePhoneExtra;
	}

	public String getServiceTvChannel() {
		return serviceTvChannel;
	}

	public void setServiceTvChannel(String serviceTvChannel) {
		this.serviceTvChannel = serviceTvChannel;
	}

	public String getServiceTvExtra() {
		return serviceTvExtra;
	}

	public void setServiceTvExtra(String serviceTvExtra) {
		this.serviceTvExtra = serviceTvExtra;
	}

	public long getServicePositive() {
		return servicePositive;
	}

	public void setServicePositive(long servicePositive) {
		this.servicePositive = servicePositive;
	}

	public long getServiceNegative() {
		return serviceNegative;
	}

	public void setServiceNegative(long serviceNegative) {
		this.serviceNegative = serviceNegative;
	}

	public String getServiceImage() {
		return serviceImage;
	}

	public void setServiceImage(String serviceImage) {
		this.serviceImage = serviceImage;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductBrand() {
		return productBrand;
	}

	public void setProductBrand(String productBrand) {
		this.productBrand = productBrand;
	}

	public String getProductDimension() {
		return productDimension;
	}

	public void setProductDimension(String productDimension) {
		this.productDimension = productDimension;
	}

	public String getProductOperatingSystem() {
		return productOperatingSystem;
	}

	public void setProductOperatingSystem(String productOperatingSystem) {
		this.productOperatingSystem = productOperatingSystem;
	}

	public String getProductImage() {
		return productImage;
	}

	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public Long getProductPositive() {
		return productPositive;
	}

	public void setProductPositive(Long productPositive) {
		this.productPositive = productPositive;
	}

	public Long getProductNegative() {
		return productNegative;
	}

	public void setProductNegative(Long productNegative) {
		this.productNegative = productNegative;
	}

	public float getProductMemory() {
		return productMemory;
	}

	public void setProductMemory(float productMemory) {
		this.productMemory = productMemory;
	}

	public float getProductRam() {
		return productRam;
	}

	public void setProductRam(float productRam) {
		this.productRam = productRam;
	}

	public String getProductProcessor() {
		return productProcessor;
	}

	public void setProductProcessor(String productProcessor) {
		this.productProcessor = productProcessor;
	}

	public float getProductWeigth() {
		return productWeigth;
	}

	public void setProductWeigth(float productWeigth) {
		this.productWeigth = productWeigth;
	}

	public String getProductBattery() {
		return productBattery;
	}

	public void setProductBattery(String productBattery) {
		this.productBattery = productBattery;
	}

	public String getProductExtraFeature() {
		return productExtraFeature;
	}

	public void setProductExtraFeature(String productExtraFeature) {
		this.productExtraFeature = productExtraFeature;
	}
	
	
	
}
