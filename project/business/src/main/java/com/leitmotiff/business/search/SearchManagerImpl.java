package com.leitmotiff.business.search;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leitmotiff.business.search.dto.SearchDTO;
import com.leitmotiff.persistence.dao.ProductDao;
import com.leitmotiff.persistence.dao.ServiceDao;
import com.leitmotiff.persistence.dao.ServiceHasProductDao;
import com.leitmotiff.persistence.entity.Product;
import com.leitmotiff.persistence.entity.ServiceHasProduct;
import com.leitmotiff.persistence.entity.Services;

@Service("search")
public class SearchManagerImpl implements SearchManager {

	@Autowired
	private ServiceDao serviceDao;
	
	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private ServiceHasProductDao shpDao;
	
	private int actualResult;
	
	private int maxResults;
	
	private boolean more;
	
	private List<SearchDTO> searchResultList;
	
	public List<SearchDTO> repeatFiltre(List<SearchDTO> addList){
		List<SearchDTO> returnList = new ArrayList<SearchDTO>();
		boolean polarity;
		for(SearchDTO searchDto : addList){
			polarity = true;
			for(int i = 0; i < returnList.size(); i++){
				if(searchDto.getName() == returnList.get(i).getName()){
					polarity = false;
					break;
				}
			}
			if(polarity){
				returnList.add(searchDto);
			}
		}
		return returnList;
	}
	
	@Override
	@Transactional
	public List<SearchDTO> getSearched(String search, int maxResults){
		actualResult = 0;
		this.maxResults = maxResults;
		more = true;
		List<SearchDTO> resultList = mappingServiceList(serviceDao.getSearchedService_1_Init(search)); 
		resultList.addAll(mappingProductList(productDao.getSearchedProduct_1_Init(search)));
		resultList.addAll(mappingServiceList(serviceDao.getSearchedServices_2_Tokenizer(search)));
		resultList.addAll(mappingProductList(productDao.getSearchedProduct_2_Tokenizer(search)));
		resultList.addAll(mappingServiceList(serviceDao.getSearchedService_3_Count(search)));
		resultList.addAll(mappingProductList(productDao.getSearchedProduct_3_Count(search)));
		resultList.addAll(mappingServiceList(serviceDao.getSearchedService_4_TokenizerInit(search)));
		resultList.addAll(mappingProductList(productDao.getSearchedProduct_4_TokenizerInit(search)));
		resultList.addAll(mappingServiceList(serviceDao.getSearchedServices_5_Contiene(search)));
		resultList.addAll(mappingProductList(productDao.getSearchedProduct_5_Contiene(search)));
		resultList.addAll(mappingServiceList(serviceDao.getSearchedProduct_6_(search)));
		resultList.addAll(mappingProductList(productDao.getSearchedProduct_6_(search)));
		searchResultList = repeatFiltre(resultList);
		List<SearchDTO> returnList = new ArrayList<SearchDTO>();
		for(int i = 0;i<this.maxResults; i++){
			if(this.actualResult != searchResultList.size()){
				returnList.add(searchResultList.get(this.actualResult));
				this.actualResult++;
			}
			else{
				this.more = false;
				break;
			}
		}
		return returnList;
	}
	
	@Override
	@Transactional
	public List<SearchDTO> getMore(){
		List<SearchDTO> returnList = new ArrayList<SearchDTO>();
		if(more){
			for(int i = 0;i<this.maxResults; i++){
				if(this.actualResult != searchResultList.size()){
					returnList.add(searchResultList.get(this.actualResult));
					this.actualResult++;
				}
				else{
					this.more = false;
					break;
				}
			}
			return returnList;
		}
		return returnList;
	}
	
	public List<SearchDTO> mappingServiceList(List<Services> serviceList){
		List<SearchDTO> resultList = new ArrayList<SearchDTO>();
		for(Services service : serviceList){
			resultList.add(mappingDTO(service));
		}
		return resultList;
	}
	
	public List<SearchDTO> mappingProductList(List<Product> productList){
		List<SearchDTO> resultList = new ArrayList<SearchDTO>();
		for(Product product : productList){
			resultList.add(mappingDTO(product));
		}
		return resultList;
	}
	
	public SearchDTO mappingDTO(Services service){
		SearchDTO searchDto = new SearchDTO();
		searchDto.setId(service.getServiceId());
		searchDto.setName(service.getServiceName());
		searchDto.setImage(service.getServiceImagePath());
		searchDto.setDescription(service.getServiceDescription());
		searchDto.setPositive(service.getServicePositive());
		searchDto.setNegative(service.getServiceNegative());
		searchDto.setPolarity(true);
		searchDto.setPrice(service.getServicePrice());
		return searchDto;
	}
	public SearchDTO mappingDTO(Product product){
		SearchDTO searchDto = new SearchDTO();
		searchDto.setId(product.getProductId());
		searchDto.setName(product.getProductName());
		searchDto.setImage(product.getProductImagePath());
		searchDto.setDescription(product.getProductDescription());
		searchDto.setPositive(product.getProductPositive());
		searchDto.setNegative(product.getProductNegative());
		searchDto.setPolarity(false);
		ServiceHasProduct price = shpDao.getMinurService(product.getProductId());
		if(price != null){
			searchDto.setPrice(price.getServiceHasProductPrice());
		}
		return searchDto;
	}
	
}
