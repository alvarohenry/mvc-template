package com.leitmotiff.business.category.dto;

import java.util.List;

import com.leitmotiff.persistence.entity.Product;
import com.leitmotiff.persistence.entity.Services;


public class CategoryDTO {

	private Long id;
	
	private String name;
	
	private String image;
	
	private long topId;
	
	private long patternId;
	
	private boolean polarity;
	
	public CategoryDTO() {	}
	
	public CategoryDTO(Long id, String name, String image, long topId) {
		this.id = id;
		this.name = name;
		this.image = image;
		this.topId = topId;
	}
	
	public CategoryDTO(Long id, String name, String image, long topId, long patternId) {
		this.id = id;
		this.name = name;
		this.image = image;
		this.topId = topId;
		this.patternId = patternId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public long getTopId() {
		return topId;
	}

	public void setTopId(long topId) {
		this.topId = topId;
	}

	public long getPatternId() {
		return patternId;
	}

	public void setPatternId(long patternId) {
		this.patternId = patternId;
	}

	public boolean isPolarity() {
		return polarity;
	}

	public void setPolarity(boolean polarity) {
		this.polarity = polarity;
	}
	
}