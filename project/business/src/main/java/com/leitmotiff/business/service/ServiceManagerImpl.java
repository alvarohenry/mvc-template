package com.leitmotiff.business.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leitmotiff.business.service.dto.ServiceDTO;
import com.leitmotiff.business.util.UtilBusiness;
import com.leitmotiff.persistence.dao.ServiceDao;
import com.leitmotiff.persistence.entity.Services;

@Service("services")
public class ServiceManagerImpl implements ServiceManager{
	
	
	@Autowired
	private ServiceDao serviceDao;
	
	public List<ServiceDTO> getServices(Long idCategory){
		Criterion criterion = Restrictions.eq("category.id", idCategory);
		return mappingList(serviceDao.findByCriteria(criterion));
	}
	
	public List<ServiceDTO> getServices(Long idCategory, int maxResults){
		Criterion criterion = Restrictions.eq("category.id", idCategory);
		return mappingList(serviceDao.findByCriteria(criterion, maxResults));
	}
	
	public List<ServiceDTO> getServicesOrderPrice(Long idCategory, boolean order) {
		return mappingList(serviceDao.getServiceOrderPrice(idCategory, order));
	}

	public List<ServiceDTO> getServicesOrderValue(Long idCategory, boolean order) {

		return mappingList(serviceDao.getServiceOrderValue(idCategory, order));
		
	}
	
	@Override
	@Transactional
	public List<Services> getSearchedServices_1(String search){
		return serviceDao.getSearchedService_1_Init(search);
	}
	
	public List<ServiceDTO> getMoreService(){
		return mappingList(serviceDao.getMore());
	}
	
	public ServiceDTO getService(long id){
		return UtilBusiness.mappingDTO(serviceDao.getById(id));
	}
	
	public List<ServiceDTO> mappingList (List<Services> listServices){
		List<ServiceDTO> listaDto = new ArrayList<ServiceDTO>();
		for (Services service : listServices){
			listaDto.add(UtilBusiness.mappingDTO(service));
		}
		return listaDto;
	}	
}