package com.leitmotiff.business.service;

import java.util.List;

import com.leitmotiff.business.product.dto.ProductDTO;
import com.leitmotiff.business.service.dto.ServiceDTO;
import com.leitmotiff.persistence.entity.Services;

public interface ServiceManager {
	
	List<ServiceDTO> getServices(Long idCategory);
	
	List<ServiceDTO> getServices(Long idCategory, int maxResults);
	
	List<ServiceDTO> getServicesOrderPrice(Long idCategory, boolean order);
	
	List<ServiceDTO> getServicesOrderValue(Long idCategory, boolean order);

	List<ServiceDTO> getMoreService();

	ServiceDTO getService(long id);
	
	List<ServiceDTO> mappingList (List<Services> listServices);
	
	 List<Services> getSearchedServices_1(String search);
	
}