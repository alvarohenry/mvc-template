package com.leitmotiff.business.mostSearchedSentence;

import java.util.List;

import com.leitmotiff.business.mostSearchedSentence.dto.MostSearchedSentencesDTO;
import com.leitmotiff.persistence.entity.MostSearchedSentences;

public interface MostSearchedSentencesManager {

	List<MostSearchedSentencesDTO> getMostSearchedSentences(String search);

	void increaseValue(Long id);
	
	void addSentence(String search);
	
	List<MostSearchedSentencesDTO> mappingList(List<MostSearchedSentences> sentencesList);
	
	boolean sentenceFiltre(String sentence);
}
