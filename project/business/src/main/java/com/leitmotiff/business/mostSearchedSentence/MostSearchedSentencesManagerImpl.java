package com.leitmotiff.business.mostSearchedSentence;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leitmotiff.business.mostSearchedSentence.dto.MostSearchedSentencesDTO;
import com.leitmotiff.business.util.UtilBusiness;
import com.leitmotiff.persistence.dao.DictionaryDao;
import com.leitmotiff.persistence.dao.MostSearchedSentencesDao;
import com.leitmotiff.persistence.entity.Dictionary;
import com.leitmotiff.persistence.entity.MostSearchedSentences;

@Service("mostSearchedSentence")
public class MostSearchedSentencesManagerImpl implements MostSearchedSentencesManager {

	@Autowired
	private MostSearchedSentencesDao mostSearchedSentencesDao;
	
	@Autowired
	private DictionaryDao dictionaryDao;
	
	public List<MostSearchedSentencesDTO> getMostSearchedSentences(String search){
		return mappingList(mostSearchedSentencesDao.getMostSearchedSentences(search));
	}
	
	public void increaseValue(Long id){
		
		MostSearchedSentences sentence = mostSearchedSentencesDao.getById(id);
		sentence.setMostSearchedSentencesValue(sentence.getMostSearchedSentencesValue() + 1);
		mostSearchedSentencesDao.saveOrUpdate(sentence);
	}
	
	public void addSentence(String search){
		search = search.trim();
		search = UtilBusiness.tokenRemplace(search);
		MostSearchedSentences sentence = new MostSearchedSentences();
		System.out.println("Abuuuuuuuu23");
		if(search.length() > 1 && search != " " && sentenceFiltre(search)){
			Criterion criterion = Restrictions.eq("mostSearchedSentencesSentence",search);
			List<MostSearchedSentences> sentences = mostSearchedSentencesDao.findByCriteria(criterion);
			System.out.println("Abuuuuuuuu24");
			if(sentences.isEmpty()){
				sentence.setMostSearchedSentencesSentence(search.toLowerCase());
				sentence.setMostSearchedSentencesValue(1L);
				mostSearchedSentencesDao.saveOrUpdate(sentence);
				System.out.println("Abuuuuuuu25");
			} else{
				sentence = sentences.get(0);
				sentence.setMostSearchedSentencesValue(sentence.getMostSearchedSentencesValue() + 1);
				mostSearchedSentencesDao.saveOrUpdate(sentence);
				System.out.println("Abuuuuuuuu26");
			}
		}
	}
	
	public List<MostSearchedSentencesDTO> mappingList(List<MostSearchedSentences> sentencesList){
		List<MostSearchedSentencesDTO> sentencesDTO = new ArrayList<MostSearchedSentencesDTO>(); 
		for(MostSearchedSentences sentence : sentencesList){
			sentencesDTO.add(UtilBusiness.mappingDTO(sentence));
		}
		return sentencesDTO;
	}
	
	public boolean sentenceFiltre(String sentence){
		StringTokenizer words = new StringTokenizer(sentence); 
		
		while (words.hasMoreElements()){
			String token = words.nextElement().toString();
			Criterion criterion = Restrictions.eq("dictionaryWord",token);
			List<Dictionary> dictionary = dictionaryDao.findByCriteria(criterion);
			if(dictionary.isEmpty()){
				return false;
			}
		}
		return true;
	}
}