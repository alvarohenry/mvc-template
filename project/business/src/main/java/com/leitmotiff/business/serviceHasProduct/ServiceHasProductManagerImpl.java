package com.leitmotiff.business.serviceHasProduct;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leitmotiff.business.serviceHasProduct.dto.ServiceHasProductDTO;
import com.leitmotiff.business.util.UtilBusiness;
import com.leitmotiff.persistence.dao.ServiceHasProductDao;
import com.leitmotiff.persistence.entity.ServiceHasProduct;

@Service("serviceHasProduct")
public class ServiceHasProductManagerImpl implements ServiceHasProductManager {
	
	@Autowired
	private ServiceHasProductDao sHasPDao;
	
	@Override
	public List<ServiceHasProductDTO> getServiceHasProducts(Long id,
			boolean table) {
		Criterion criterion;
		if(table){
			criterion = Restrictions.eq("service.serviceId", id);
		}else{
			criterion = Restrictions.eq("product.productId", id);
		}
		
		return mappingList(sHasPDao.findByCriteria(criterion));
	}

	@Override
	@Transactional
	public List<ServiceHasProductDTO> getServiceHasProducts(Long id,
			boolean table, int maxResults) {
		Criterion criterion;
		if(table){
			criterion = Restrictions.eq("service.id", id);
		}else{
			criterion = Restrictions.eq("product.id", id);
		}
		return mappingList(sHasPDao.findByCriteria(criterion, maxResults));
	}

	@Override
	public List<ServiceHasProductDTO> getOrderPrice(Long id, boolean table,
			boolean order) {
		if(table){
			return mappingList(sHasPDao.getServiceHasProductOrderPrice("product", id, order));
		}else{
			return mappingList(sHasPDao.getServiceHasProductOrderPrice("service", id, order));
		}
	}

	@Override
	public List<ServiceHasProductDTO> getOrderValue(Long id, boolean table,
			boolean order) {
		if(table){
			return mappingList(sHasPDao.getServiceHasProductOrderValue("product", id, order));
		}else{
			return mappingList(sHasPDao.getServiceHasProductOrderValue("service", id, order));
		}
	}

	@Override
	@Transactional
	public List<ServiceHasProductDTO> getMore() {
		return mappingList(sHasPDao.getMore());
	}
	

	@Override
	public List<ServiceHasProductDTO> mappingList(
			List<ServiceHasProduct> listServiceHasProduct) {
		List<ServiceHasProductDTO> listHas = new ArrayList<ServiceHasProductDTO>();
		for (ServiceHasProduct shasP : listServiceHasProduct){
			listHas.add(UtilBusiness.mappingDTO(shasP));
		}
		return listHas;
	}
	
}
