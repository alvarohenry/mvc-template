package com.leitmotiff.business.company;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leitmotiff.business.company.dto.CompanyDTO;
import com.leitmotiff.persistence.dao.CompanyDao;
import com.leitmotiff.persistence.entity.Company;

@Service("company")
public class CompanyManagerImpl implements CompanyManager{
	
	@Autowired
	private CompanyDao companyDao;
	
	public List<CompanyDTO> getAll() {
		List<Company> companies = companyDao.findAll();
		List<CompanyDTO> comDTOs = new ArrayList<CompanyDTO>();
		for (Company company : companies) {
			comDTOs.add(new CompanyDTO(company.getCompanyId(),
										company.getCompanyName(),
										company.getCompanyDescription(),
										company.getCompanyUrl(),
										company.getCompanyImagePath()));
		}
		return comDTOs;
	}
}
