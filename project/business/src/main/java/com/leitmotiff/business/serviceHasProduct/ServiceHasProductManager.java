package com.leitmotiff.business.serviceHasProduct;

import java.util.List;

import com.leitmotiff.business.serviceHasProduct.dto.ServiceHasProductDTO;
import com.leitmotiff.persistence.entity.ServiceHasProduct;

public interface ServiceHasProductManager {

	List<ServiceHasProductDTO> getServiceHasProducts(Long id, boolean table);
	
	List<ServiceHasProductDTO> getServiceHasProducts(Long id, boolean table, int maxResults);
	
	List<ServiceHasProductDTO> getOrderPrice(Long id, boolean table, boolean order);
	
	List<ServiceHasProductDTO> getOrderValue(Long id, boolean table, boolean order);

	List<ServiceHasProductDTO> getMore();
	
	List<ServiceHasProductDTO> mappingList (List<ServiceHasProduct> listServiceHasProduct);
}
