package com.leitmotiff.business.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leitmotiff.business.user.dto.UserDTO;
import com.leitmotiff.business.util.UtilBusiness;
import com.leitmotiff.persistence.dao.UserDao;


@Service("user")
public class UserManagerImpl implements UserManager {

	@Autowired
	private UserDao userDao;
	
	@Override
	public UserDTO getUser(long id){
		return UtilBusiness.mappingDTO(userDao.getById(id));
	}
	
}
