package com.leitmotiff.business.company;

import java.util.List;

import com.leitmotiff.business.company.dto.CompanyDTO;

public interface CompanyManager {

	List<CompanyDTO> getAll();
	
}
