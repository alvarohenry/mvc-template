package com.leitmotiff.business.user;

import com.leitmotiff.business.user.dto.UserDTO;

public interface UserManager {

	UserDTO getUser(long id);
	
}
