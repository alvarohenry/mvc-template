package com.leitmotiff.business.category;

import java.util.List;

import com.leitmotiff.business.category.dto.CategoryDTO;

public interface CategoryManager {

	List<CategoryDTO> getAll();

	CategoryDTO findById(long id);

}
