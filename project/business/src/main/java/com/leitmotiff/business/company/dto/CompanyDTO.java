package com.leitmotiff.business.company.dto;


public class CompanyDTO {
	
	private Long id;
	
	private String name;
	
	private String url;
	
	private String description;
	
	private String image;
    
	public CompanyDTO() {	}
	
	public CompanyDTO(Long id, String name, String url, String description, String image) {
		this.id = id;
		this.name = name;
		this.url = url;
		this.description = description;
		this.setImage(image);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
}
