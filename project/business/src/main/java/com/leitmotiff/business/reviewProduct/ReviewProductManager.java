package com.leitmotiff.business.reviewProduct;

import java.util.List;

import com.leitmotiff.business.review.dto.ReviewProductDTO;
import com.leitmotiff.persistence.entity.Reviewproduct;

public interface ReviewProductManager {

	List getReviews(Long id, int maxResults);
	
	List mappingList (List listReview);
	
	List getMore();
	
	void insertReview(String review);
	
	List<Integer> numberReview(long id);
	
}
