package com.leitmotiff.business.category;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leitmotiff.business.category.dto.CategoryDTO;
import com.leitmotiff.business.util.UtilBusiness;
import com.leitmotiff.persistence.dao.CategoryDao;
import com.leitmotiff.persistence.entity.Category;

@Service("category")
public class CategoryManagerImpl implements CategoryManager {
	
	@Autowired
	private CategoryDao categoryDao;
	
	public List<CategoryDTO> getAll() {
		List<Category> categories = categoryDao.findAll();
		List<CategoryDTO> catDTOs = new ArrayList<CategoryDTO>();
		for (Category category : categories) {
			catDTOs.add(UtilBusiness.mappingDTO(category));
		}
		return catDTOs;
	}
	
	@Override
	public CategoryDTO findById(long id){
		return UtilBusiness.mappingDTO(categoryDao.getById(id));
	}
}
