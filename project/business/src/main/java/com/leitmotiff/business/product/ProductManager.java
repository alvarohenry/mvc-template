package com.leitmotiff.business.product;

import java.util.List;

import com.leitmotiff.business.product.dto.ProductDTO;
import com.leitmotiff.persistence.entity.Product;

public interface ProductManager {
	
	List<ProductDTO> getProducts(Long idCategory);
	
	List<ProductDTO> getProductsOrderPrice(Long idCategory, boolean order);

	List<ProductDTO> getProductsOrderValue(Long idCategory, boolean order);
	
	List<ProductDTO> mappingList(List<Product> listProduct);
	
	ProductDTO getProduct(Long id);

	List<ProductDTO> getProducts(Long idCategory, int maxResults);

	List<ProductDTO> getMoreProducts();
	
}
