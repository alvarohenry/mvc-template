package com.leitmotiff.business.reviewService;

import java.util.List;

import com.leitmotiff.business.review.dto.ReviewServiceDTO;
import com.leitmotiff.persistence.entity.Reviewservice;

public interface ReviewServiceManager {

	List<ReviewServiceDTO> getReviews(Long id, int maxResults);
	
	List<ReviewServiceDTO> mappingList (List<Reviewservice> listReview);
	
	List<ReviewServiceDTO> getMore();
	
	void insertReview(String review);
	
	List<Integer> numberReviews(long id);
	
}
