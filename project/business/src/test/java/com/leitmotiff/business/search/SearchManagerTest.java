package com.leitmotiff.business.search;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.leitmotiff.business.search.dto.SearchDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-business-context.xml" })
public class SearchManagerTest {

	@Resource
	private SearchManager searchManager;
	
	@Test
	public void testGetSearched(){
		List<SearchDTO> searchList = searchManager.getSearched("string",6);
		assertEquals("Product Test 1", searchList.get(1).getName());
		assertEquals("Product Test 3", searchList.get(5).getName());
		assertEquals("Service Test 1", searchList.get(0).getName());
		assertEquals("Service Test 3", searchList.get(4).getName());
	}
	
	@Test public void testGetMore(){
		List<SearchDTO> searchList = searchManager.getSearched("string",6);
		searchList = searchManager.getMore();
		assertEquals("Product Test 4", searchList.get(1).getName());
		assertEquals("Product Test 6", searchList.get(5).getName());
		assertEquals("Service Test 4", searchList.get(0).getName());
		assertEquals("Service Test 6", searchList.get(4).getName());
	}

}
