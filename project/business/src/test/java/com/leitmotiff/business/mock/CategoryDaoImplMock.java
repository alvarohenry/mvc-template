package com.leitmotiff.business.mock;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;

import com.leitmotiff.persistence.dao.CategoryDao;
import com.leitmotiff.persistence.entity.Category;

public class CategoryDaoImplMock implements CategoryDao {

	@Override
	public Category getById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findAll() {
		List<Category> cats = new ArrayList<Category>();
		Category cat = new Category(Long.parseLong("1"), "categoryTest", "/home/category",1L);
		cats.add(cat);
		return cats;
	}

	@Override
	public void saveOrUpdate(Category entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Category entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List findByCriteria(Criterion criterion) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findByCriteria(Criterion criterion, int maxResults) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List getMore() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findAll(int maxResults) {
		// TODO Auto-generated method stub
		return null;
	}

}
