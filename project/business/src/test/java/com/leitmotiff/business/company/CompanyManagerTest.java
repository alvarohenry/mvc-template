package com.leitmotiff.business.company;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.leitmotiff.business.company.dto.CompanyDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-business-context.xml" })
public class CompanyManagerTest {
	
	@Resource
	private CompanyManager companyManager;

	@Test
	public void testGetAll() {
		List<CompanyDTO> companies = companyManager.getAll();
		assertTrue(companies.size() == 1);
		assertEquals("companyTest", companies.get(0).getName());
	}
}
