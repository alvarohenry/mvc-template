package com.leitmotiff.business.mock;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;

import com.leitmotiff.business.search.dto.SearchDTO;
import com.leitmotiff.persistence.dao.ProductDao;
import com.leitmotiff.persistence.entity.Category;
import com.leitmotiff.persistence.entity.Product;

public class ProductDaoImplMock implements ProductDao {

	public List getProductsOrderPrice(Long idCategory, boolean order){
//		List<Product> products = new ArrayList<Product>();
//		Product product1 = new Product();
//		Product product2 = new Product();
//		Category category = new Category();
//		category.setCategoryId(idCategory);
//		product1.setProductId(1L);
//		product1.setProductName("productTest1");
//		product2.setProductId(2L);
//		product2.setProductName("productTest2");
//		
//		if(order){
//			products.add(product1);
//			products.add(product2);
//		}
//		else{
//			products.add(product2);
//			products.add(product1);
//		}
//		
//		return products;
		return null;
	}
	
	public List<Product> getProductOrderValue(Long idCategory, boolean order){
		List<Product> products = new ArrayList<Product>();
		Category category = new Category(idCategory, "", "", 0L);
		Product product1 = new Product(1L, category, "productTest1", 10L, 1L);
		Product product2 = new Product(2L, category, "productTest2", 100L, 10L);
		product1.setProductMemory(1F);
		product1.setProductRam(1F);
		product1.setProductWeight(1F);
		product2.setProductMemory(1F);
		product2.setProductRam(1F);
		product2.setProductWeight(1F);
		
		if(order){
			products.add(product1);
			products.add(product2);
		}
		else{
			products.add(product2);
			products.add(product1);
		}
		
		return products;
	}
	
	public List findByCriteria(){
		//TODO preguntar sobre ello
		return null;
	}

	@Override
	public Product getById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveOrUpdate(Product entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Product entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List findByCriteria(Criterion criterion) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List findAll(int maxResults) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findByCriteria(Criterion criterion, int maxResults) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List getMore() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<Product> getSearchedProduct_1_Init(String search) {
		List<Product> resultList = new ArrayList<Product>();
		Category category = new Category(1L, "", "", 0L);
		Product product = new Product();
		product.setProductId(1L);
		product.setProductName("Product Test 1");
		product.setCategory(category);
		product.setProductWeight(100F);
		product.setProductRam(100F);
		product.setProductMemory(100F);
		resultList.add(product);
		return resultList;
	}

	@Override
	public List<Product> getSearchedProduct_2_Tokenizer(String search) {
		List<Product> resultList = new ArrayList<Product>();
		Product product = new Product();
		Category category = new Category(1L, "", "", 0L);
		product.setProductId(2L);
		product.setProductName("Product Test 2");
		product.setCategory(category);
		product.setProductWeight(100F);
		product.setProductRam(100F);
		product.setProductMemory(100F);
		resultList.add(product);
		return resultList;
	}

	@Override
	public List<Product> getSearchedProduct_3_Count(String search) {
		List<Product> resultList = new ArrayList<Product>();
		Category category = new Category(1L, "", "", 0L);
		Product product = new Product();
		product.setProductId(3L);
		product.setCategory(category);
		product.setProductName("Product Test 3");
		product.setProductWeight(100F);
		product.setProductRam(100F);
		product.setProductMemory(100F);
		resultList.add(product);
		return resultList;
	}

	@Override
	public List<Product> getSearchedProduct_4_TokenizerInit(String search) {
		List<Product> resultList = new ArrayList<Product>();
		Category category = new Category(1L, "", "", 0L);
		Product product = new Product();
		product.setProductId(4L);
		product.setCategory(category);
		product.setProductName("Product Test 4");
		product.setProductWeight(100F);
		product.setProductRam(100F);
		product.setProductMemory(100F);
		resultList.add(product);
		return resultList;
	}
	

	@Override
	public List<Product> getSearchedProduct_5_Contiene(String search) {
		List<Product> resultList = new ArrayList<Product>();
		Product product = new Product();
		Category category = new Category(1L, "", "", 0L);
		product.setProductId(5L);
		product.setCategory(category);
		product.setProductName("Product Test 5");
		product.setProductWeight(100F);
		product.setProductRam(100F);
		product.setProductMemory(100F);
		resultList.add(product);
		return resultList;
	}

	@Override
	public List<Product> getSearchedProduct_6_(String search) {
		List<Product> resultList = new ArrayList<Product>();
		Category category = new Category(1L, "", "", 0L);
		Product product = new Product();
		product.setProductId(6L);
		product.setCategory(category);
		product.setProductName("Product Test 6");
		product.setProductWeight(100F);
		product.setProductRam(100F);
		product.setProductMemory(100F);
		resultList.add(product);
		return resultList;
	}
}
