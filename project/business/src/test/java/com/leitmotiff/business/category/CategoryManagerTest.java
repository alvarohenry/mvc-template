package com.leitmotiff.business.category;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.leitmotiff.business.category.dto.CategoryDTO;

/**
 * Unit test for CategoryManager.
 */
@ContextConfiguration(locations = { "classpath:test-business-context.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class CategoryManagerTest {

	@Resource
	private CategoryManager categoryManager;

	@Test
	public void testGetAll() {
		List<CategoryDTO> categories = categoryManager.getAll();
		assertTrue(categories.size() == 1);
		assertEquals("categoryTest", categories.get(0).getName());
	}
	
}
