package com.leitmotiff.business.review;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.leitmotiff.business.review.dto.ReviewProductDTO;
import com.leitmotiff.business.reviewProduct.ReviewProductManager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-business-context.xml" })
public class ReviewProductManagerTest {
	
	@Resource
	private ReviewProductManager reviewProductManager;
	
	@Test
	public void testGetReviews(){
		List<ReviewProductDTO> list = reviewProductManager.getReviews(1L, 5);
		assertEquals("Body de Prueba",list.get(0).getBody());
		assertEquals("2013 feb 28",list.get(0).getDate());
		assertEquals("Nicoll",list.get(0).getUserName());
	}
	
	@Test
	public void testGetMore(){
		List<ReviewProductDTO> list = reviewProductManager.getMore();
		assertEquals("Body de Prueba More",list.get(0).getBody());
	}
	
}
