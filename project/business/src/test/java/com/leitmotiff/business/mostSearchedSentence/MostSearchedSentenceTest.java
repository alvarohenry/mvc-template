package com.leitmotiff.business.mostSearchedSentence;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.leitmotiff.business.mostSearchedSentence.dto.MostSearchedSentencesDTO;
import com.leitmotiff.persistence.entity.Dictionary;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-business-context.xml" })
public class MostSearchedSentenceTest {

	@Resource
	private MostSearchedSentencesManager mSSManager;
	
	@Test
	public void testGetMostSearchedSentences(){
		List<MostSearchedSentencesDTO> resultList = mSSManager.getMostSearchedSentences("Sentencia");
		assertEquals("Sentencia Prueba",resultList.get(0).getSentence());
	}
	@Test 
	public void testIncreaseValue(){
		try{
			mSSManager.increaseValue(1L);
		}
		catch(Exception exception){
			Assert.fail();
		}
	}
	
	@Test
	public void testSentenceFiltre(){
		assertTrue(mSSManager.sentenceFiltre("sentence"));
	}
	
}