package com.leitmotiff.business.mock;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;

import com.leitmotiff.persistence.dao.ServiceHasProductDao;
import com.leitmotiff.persistence.entity.Category;
import com.leitmotiff.persistence.entity.Company;
import com.leitmotiff.persistence.entity.Product;
import com.leitmotiff.persistence.entity.Reviewproduct;
import com.leitmotiff.persistence.entity.ServiceHasProduct;
import com.leitmotiff.persistence.entity.ServiceHasProductId;
import com.leitmotiff.persistence.entity.Services;

public class ServiceHasProductDaoImplMock implements ServiceHasProductDao{

	@Override
	public ServiceHasProduct getById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findAll(int maxResults) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveOrUpdate(ServiceHasProduct entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(ServiceHasProduct entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<ServiceHasProduct> findByCriteria(Criterion criterion) {
		List<ServiceHasProduct> shasP = new ArrayList<ServiceHasProduct>();
		Category category = new Category(1L, "", "", 0L);
		Company company = new Company(1L, "company");
		Services service1 = new Services(1L, category, company ,"service1", 10L, 1L, true);
		Services service2 = new Services(2L, category, company, "service2", 100L, 10L, true);
		service1.setServicePrice(20.0);
		service1.setServicePhoneMb(10);
		service1.setServicePhoneMinAll(10);
		service2.setServicePrice(200.0);
		service2.setServicePhoneMb(10);
		service2.setServicePhoneMinAll(10);
		
		Product product1 = new Product(1L, category, "product1", 10L, 1L);
		Product product2 = new Product(2L, category, "product2", 100L, 10L);
		product1.setProductMemory(1F);
		product1.setProductRam(1F);
		product1.setProductWeight(1F);
		product2.setProductMemory(1F);
		product2.setProductRam(1F);
		product2.setProductWeight(1F);
		
		ServiceHasProductId hasId = new ServiceHasProductId(service1.getServiceId(), product1.getProductId(), 12);
		ServiceHasProduct has = new ServiceHasProduct(hasId, product1, service1,80.99, "", "", true);
		shasP.add(has);
		hasId = new ServiceHasProductId(service1.getServiceId(), product2.getProductId(), 12);
		has = new ServiceHasProduct(hasId, product2, service1, 220.50, "", "", true);
		shasP.add(has);
		hasId = new ServiceHasProductId(service2.getServiceId(), product1.getProductId(), 18);
		has = new ServiceHasProduct(hasId, product1, service2, 20.0, "", "", true);
		shasP.add(has);
		return shasP;
	}

	@Override
	public List<ServiceHasProduct> findByCriteria(Criterion criterion, int maxResults) {
		List<ServiceHasProduct> shasP = new ArrayList<ServiceHasProduct>();
		Category category = new Category(1L, "", "", 0L);
		Company company = new Company(1L, "company");
		Services service1 = new Services(1L, category, company ,"service1", 10L, 1L, true);
		service1.setServicePrice(20.0);
		service1.setServicePhoneMb(10);
		service1.setServicePhoneMinAll(10);
		Product product1 = new Product(1L, category, "product1", 10L, 1L);
		product1.setProductMemory(1F);
		product1.setProductRam(1F);
		product1.setProductWeight(1F);
		
		ServiceHasProductId hasId1 = new ServiceHasProductId(service1.getServiceId(), product1.getProductId(), 12);
		ServiceHasProduct has1 = new ServiceHasProduct(hasId1, product1, service1,80.99, "", "", true);
		shasP.add(has1);
		return shasP;
	}

	@Override
	public List<ServiceHasProduct> getMore() {
		List<ServiceHasProduct> shasP = new ArrayList<ServiceHasProduct>();
		Category category = new Category(1L, "", "", 0L);
		Company company = new Company(1L, "company");
		Services service1 = new Services(1L, category, company ,"service1", 10L, 1L, true);
		service1.setServicePrice(20.0);
		service1.setServicePhoneMb(10);
		service1.setServicePhoneMinAll(10);
		Product product2 = new Product(1L, category, "product2", 10L, 1L);
		product2.setProductMemory(1F);
		product2.setProductRam(1F);
		product2.setProductWeight(1F);
		
		ServiceHasProductId hasId1 = new ServiceHasProductId(service1.getServiceId(), product2.getProductId(), 12);
		ServiceHasProduct has1 = new ServiceHasProduct(hasId1, product2, service1,80.99, "", "", true);
		shasP.add(has1);
		return shasP;
	}
	
	@Override
	public List<ServiceHasProduct> getServiceHasProductOrderPrice(String table,
			Long id, boolean order) {
		List<ServiceHasProduct> shasP = new ArrayList<ServiceHasProduct>();
		Category category = new Category(1L, "", "", 0L);
		Company company = new Company(1L, "company");
		Services service1 = new Services(1L, category, company ,"service1", 10L, 1L, true);
		Services service2 = new Services(2L, category, company, "service2", 100L, 10L, true);
		service1.setServicePrice(20.0);
		service1.setServicePhoneMb(10);
		service1.setServicePhoneMinAll(10);
		service2.setServicePrice(200.0);
		service2.setServicePhoneMb(10);
		service2.setServicePhoneMinAll(10);
		
		Product product1 = new Product(1L, category, "product1", 10L, 1L);
		Product product2 = new Product(2L, category, "product2", 100L, 10L);
		product1.setProductMemory(1F);
		product1.setProductRam(1F);
		product1.setProductWeight(1F);
		product2.setProductMemory(1F);
		product2.setProductRam(1F);
		product2.setProductWeight(1F);
		
		ServiceHasProductId hasId1 = new ServiceHasProductId(service1.getServiceId(), product1.getProductId(), 12);
		ServiceHasProduct has1 = new ServiceHasProduct(hasId1, product1, service1,80.99, "", "", true);
		
		ServiceHasProductId hasId2 = new ServiceHasProductId(service1.getServiceId(), product2.getProductId(), 12);
		ServiceHasProduct has2 = new ServiceHasProduct(hasId2, product2, service1, 220.50, "", "", true);
		
		ServiceHasProductId hasId4 = new ServiceHasProductId(service2.getServiceId(), product1.getProductId(), 18);
		ServiceHasProduct has4 = new ServiceHasProduct(hasId4, product1, service2, 20.0, "", "", true);
		
		if(table == "product"){
			if (order){ 
				shasP.add(has1);
				shasP.add(has2);
			}
			else{
				shasP.add(has2);
				shasP.add(has1);
			}
		}
		else if(table == "service"){
			if (order){ 
				shasP.add(has1);
				shasP.add(has4);
			}
			else{
				shasP.add(has4);
				shasP.add(has1);
			}
		}
		
		return shasP;
	}

	@Override
	public List<ServiceHasProduct> getServiceHasProductOrderValue(String table,
			Long id, boolean order) {
		List<ServiceHasProduct> shasP = new ArrayList<ServiceHasProduct>();
		Category category = new Category(1L, "", "", 0L);
		Company company = new Company(1L, "company");
		Services service1 = new Services(1L, category, company ,"service1", 10L, 1L, true);
		Services service2 = new Services(2L, category, company, "service2", 100L, 10L, true);
		service1.setServicePrice(20.0);
		service1.setServicePhoneMb(10);
		service1.setServicePhoneMinAll(10);
		service2.setServicePrice(200.0);
		service2.setServicePhoneMb(10);
		service2.setServicePhoneMinAll(10);
		
		Product product1 = new Product(1L, category, "product1", 10L, 1L);
		Product product2 = new Product(2L, category, "product2", 100L, 10L);
		product1.setProductMemory(1F);
		product1.setProductRam(1F);
		product1.setProductWeight(1F);
		product2.setProductMemory(1F);
		product2.setProductRam(1F);
		product2.setProductWeight(1F);
		
		ServiceHasProductId hasId1 = new ServiceHasProductId(service1.getServiceId(), product1.getProductId(), 12);
		ServiceHasProduct has1 = new ServiceHasProduct(hasId1, product1, service1,80.99, "", "", true);
		
		ServiceHasProductId hasId2 = new ServiceHasProductId(service1.getServiceId(), product2.getProductId(), 12);
		ServiceHasProduct has2 = new ServiceHasProduct(hasId2, product2, service1, 220.50, "", "", true);
		
		ServiceHasProductId hasId4 = new ServiceHasProductId(service2.getServiceId(), product1.getProductId(), 18);
		ServiceHasProduct has4 = new ServiceHasProduct(hasId4, product1, service2, 20.0, "", "", true);
		
		if(table == "product"){
			if (order){ 
				shasP.add(has1);
				shasP.add(has2);
			}
			else{
				shasP.add(has2);
				shasP.add(has1);
			}
		}
		else if(table == "service"){
			if (order){ 
				shasP.add(has1);
				shasP.add(has4);
			}
			else{
				shasP.add(has4);
				shasP.add(has1);
			}
		}
		
		return shasP;
	}

	@Override
	public ServiceHasProduct getMinurService(long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
