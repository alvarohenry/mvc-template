package com.leitmotiff.business.mock;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;

import com.leitmotiff.persistence.dao.DictionaryDao;
import com.leitmotiff.persistence.entity.Dictionary;

public class DictionaryDaoMock implements DictionaryDao {

	@Override
	public Dictionary getById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findAll(int maxResults) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveOrUpdate(Dictionary entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Dictionary entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List findByCriteria(Criterion criterion) {
		List<Dictionary> list = new ArrayList<Dictionary>();	
		Dictionary dictionary = new Dictionary(1L,"Palabra Prueba");
		list.add(dictionary);
		return list;
	}

	@Override
	public List findByCriteria(Criterion criterion, int maxResults) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List getMore() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void createDictionary() {
		// TODO Auto-generated method stub
		
	}

}
