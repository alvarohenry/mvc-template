package com.leitmotiff.business.product;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.leitmotiff.business.product.dto.ProductDTO;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-business-context.xml" })
public class ProductManagerTest {

	@Resource
	private ProductManager productManager;
	
//	@Test
//	public void testGetProductsOrderPrice(){
//		List<ProductDTO> products = productManager.getProductsOrderPrice(1L, true);
//		assertTrue(products.size() == 2);
//		assertEquals("productTest1",products.get(0).getName());
//		products = productManager.getProductsOrderPrice(1L, false);
//		assertTrue(products.size() == 2);
//		assertEquals("productTest2",products.get(0).getName());
//	}
	
	@Test
	public void testGetProductsOrderValue(){
		List<ProductDTO> products = productManager.getProductsOrderValue(1L, true);
		assertTrue(products.size() == 2);
		assertEquals("productTest1",products.get(0).getName());
		products = productManager.getProductsOrderValue(1L, false);
		assertTrue(products.size() == 2);
		assertEquals("productTest2",products.get(0).getName());
	}

	
	
}
