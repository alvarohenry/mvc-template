package com.leitmotiff.business.mock;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;

import com.leitmotiff.persistence.dao.MostSearchedSentencesDao;
import com.leitmotiff.persistence.entity.MostSearchedSentences;

public class MostSearchedSentencesDaoImplMock implements MostSearchedSentencesDao {

	@Override
	public MostSearchedSentences getById(Long id) {
		MostSearchedSentences sentence = new  MostSearchedSentences(id, "Sentencia Prueba", 17L);
		return sentence;
	}

	@Override
	public List findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveOrUpdate(MostSearchedSentences entity) {

		
	}

	@Override
	public void delete(MostSearchedSentences entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List findByCriteria(Criterion criterion) {
		
		List<MostSearchedSentences> resultList = new ArrayList<MostSearchedSentences>();
		MostSearchedSentences sentence = new MostSearchedSentences(1L, "SentenciaPrueba", 17L);
		resultList.add(sentence);
		return resultList;
	}

	@Override
	public List<MostSearchedSentences> getMostSearchedSentences(String search) {
		
		List<MostSearchedSentences> resultList = new ArrayList<MostSearchedSentences>();
		MostSearchedSentences sentence = new MostSearchedSentences();
		sentence.setMostSearchedSentencesId(1L);
		sentence.setMostSearchedSentencesSentence("Sentencia Prueba");
		sentence.setMostSearchedSentencesValue(14L);
		resultList.add(sentence);
		return resultList;
		
	}

	@Override
	public List findAll(int maxResults) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List getMore() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findByCriteria(Criterion criterion, int maxResults) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
