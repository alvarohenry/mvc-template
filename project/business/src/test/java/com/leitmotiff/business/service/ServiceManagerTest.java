package com.leitmotiff.business.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.leitmotiff.business.product.dto.ProductDTO;
import com.leitmotiff.business.service.dto.ServiceDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-business-context.xml" })
public class ServiceManagerTest {
	
	@Resource
	private ServiceManager serviceManager;
	
	@Test
	public void testGetServicesOrderPrice(){
		List<ServiceDTO> services = serviceManager.getServicesOrderPrice(1L, true);
		assertTrue(services.size() == 2);
		assertEquals("service1",services.get(0).getName());
		services = serviceManager.getServicesOrderPrice(1L, false);
		assertTrue(services.size() == 2);
		assertEquals("service2",services.get(0).getName());
	}
	
	@Test
	public void testGetServicesOrderValue(){
		List<ServiceDTO> services = serviceManager.getServicesOrderValue(1L, true);
		assertTrue(services.size() == 2);
		assertEquals("service1",services.get(0).getName());
		services = serviceManager.getServicesOrderValue(1L, false);
		assertTrue(services.size() == 2);
		assertEquals("service2",services.get(0).getName());
	}
}
