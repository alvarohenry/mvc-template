package com.leitmotiff.business.mock;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;

import com.leitmotiff.persistence.dao.ServiceDao;
import com.leitmotiff.persistence.entity.Category;
import com.leitmotiff.persistence.entity.Company;
import com.leitmotiff.persistence.entity.Product;
import com.leitmotiff.persistence.entity.Services;

public class ServiceDaoImplMock implements ServiceDao{

	@Override
	public Services getById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findAll(int maxResults) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveOrUpdate(Services entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Services entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List findByCriteria(Criterion criterion) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findByCriteria(Criterion criterion, int maxResults) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List getMore() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Services> getServiceOrderPrice(Long idCategory, boolean order) {
		List<Services> services = new ArrayList<Services>();
		Category category = new Category(idCategory, "", "", 0L);
		Company company = new Company(1L, "company");
		Services service1 = new Services(1L, category, company ,"service1", 10L, 1L, true);
		Services service2 = new Services(2L, category, company, "service2", 100L, 10L, true);
		service1.setServicePrice(20.0);
		service1.setServicePhoneMb(10);
		service1.setServicePhoneMinAll(10);
		service2.setServicePrice(200.0);
		service2.setServicePhoneMb(10);
		service2.setServicePhoneMinAll(10);
		
		if(order){
			services.add(service1);
			services.add(service2);
		}
		else{
			services.add(service2);
			services.add(service1);
		}
		
		return services;
	}

	@Override
	public List<Services> getServiceOrderValue(Long idCategory, boolean order) {
		List<Services> services = new ArrayList<Services>();
		Category category = new Category(idCategory, "", "", 0L);
		Company company = new Company(1L, "company");
		Services service1 = new Services(1L, category, company ,"service1", 10L, 1L, true);
		Services service2 = new Services(2L, category, company, "service2", 100L, 10L, true);
		service1.setServicePrice(20.0);
		service1.setServicePhoneMb(10);
		service1.setServicePhoneMinAll(10);
		service2.setServicePrice(20.0);
		service2.setServicePhoneMb(10);
		service2.setServicePhoneMinAll(10);
		
		if(order){
			services.add(service1);
			services.add(service2);
		}
		else{
			services.add(service2);
			services.add(service1);
		}
		
		return services;
	}

	@Override
	public List<Services> getSearchedService_1_Init(String search) {
		List<Services> resultList = new ArrayList<Services>();
		Category category = new Category(1L, "", "", 0L);
		Company company = new Company(1L, "company");
		Services service1 = new Services(1L, category, company ,"Service Test 1", 10L, 1L, true);
		service1.setServicePrice(20.0);
		service1.setServicePhoneMb(10);
		service1.setServicePhoneMinAll(10);
		resultList.add(service1);
		return resultList;
		
	}

	@Override
	public List<Services> getSearchedServices_2_Tokenizer(String search) {
		List<Services> resultList = new ArrayList<Services>();
		Category category = new Category(1L, "", "", 0L);
		Company company = new Company(1L, "company");
		Services service1 = new Services(2L, category, company ,"Service Test 2", 10L, 1L, true);
		service1.setServicePrice(20.0);
		service1.setServicePhoneMb(10);
		service1.setServicePhoneMinAll(10);
		resultList.add(service1);
		return resultList;
	}

	@Override
	public List<Services> getSearchedService_3_Count(String search) {
		List<Services> resultList = new ArrayList<Services>();
		Category category = new Category(1L, "", "", 0L);
		Company company = new Company(1L, "company");
		Services service1 = new Services(3L, category, company ,"Service Test 3", 10L, 1L, true);
		service1.setServicePrice(20.0);
		service1.setServicePhoneMb(10);
		service1.setServicePhoneMinAll(10);
		resultList.add(service1);
		return resultList;
	}

	@Override
	public List<Services> getSearchedService_4_TokenizerInit(String search) {
		List<Services> resultList = new ArrayList<Services>();
		Category category = new Category(1L, "", "", 0L);
		Company company = new Company(1L, "company");
		Services service1 = new Services(4L, category, company ,"Service Test 4", 10L, 1L, true);
		service1.setServicePrice(20.0);
		service1.setServicePhoneMb(10);
		service1.setServicePhoneMinAll(10);
		resultList.add(service1);
		return resultList;
	}

	@Override
	public List<Services> getSearchedServices_5_Contiene(String search) {
		List<Services> resultList = new ArrayList<Services>();
		Category category = new Category(1L, "", "", 0L);
		Company company = new Company(1L, "company");
		Services service1 = new Services(5L, category, company ,"Service Test 5", 10L, 1L, true);
		service1.setServicePrice(20.0);
		service1.setServicePhoneMb(10);
		service1.setServicePhoneMinAll(10);
		resultList.add(service1);
		return resultList;
	}

	@Override
	public List<Services> getSearchedProduct_6_(String search) {
		List<Services> resultList = new ArrayList<Services>();
		Category category = new Category(1L, "", "", 0L);
		Company company = new Company(1L, "company");
		Services service1 = new Services(6L, category, company ,"Service Test 6", 10L, 1L, true);
		service1.setServicePrice(20.0);
		service1.setServicePhoneMb(10);
		service1.setServicePhoneMinAll(10);
		resultList.add(service1);
		return resultList;
	}

}
