package com.leitmotiff.business.mock;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.hibernate.criterion.Criterion;

import com.leitmotiff.persistence.dao.ReviewProductDao;
import com.leitmotiff.persistence.entity.Reviewproduct;
import com.leitmotiff.persistence.entity.User;

public class ReviewProductDaoImplMock implements ReviewProductDao {

	@Override
	public Reviewproduct getById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findAll(int maxResults) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveOrUpdate(Reviewproduct entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Reviewproduct entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List findByCriteria(Criterion criterion) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findByCriteria(Criterion criterion, int maxResults) {
		List<Reviewproduct> list = new ArrayList<Reviewproduct>();
		Reviewproduct review = new Reviewproduct();
		review.setReviewProductId(1L);
		review.setReviewProductBody("Body de Prueba");
		review.setReviewProductPolarity(1);
		Calendar calendar = new GregorianCalendar(2013,1,28);
		review.setReviewProductDate(calendar);
		User user = new User();
		user.setUserId(1L);
		user.setUserName("Nicoll");
		review.setUser(user);
		list.add(review);
		return list;
	}

	@Override
	public List getMore() {
		List<Reviewproduct> list = new ArrayList<Reviewproduct>();
		Reviewproduct review = new Reviewproduct();
		review.setReviewProductId(2L);
		review.setReviewProductBody("Body de Prueba More");
		review.setReviewProductPolarity(1);
		list.add(review);
		return list;
	}

	@Override
	public List<Integer> numberComentary(long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
