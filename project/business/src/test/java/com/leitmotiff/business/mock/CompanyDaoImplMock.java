package com.leitmotiff.business.mock;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;

import com.leitmotiff.persistence.dao.CompanyDao;
import com.leitmotiff.persistence.entity.Company;

public class CompanyDaoImplMock implements CompanyDao {
	
	@Override
	public Company getById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findAll() {
		List<Company> coms = new ArrayList<Company>();
		Company com = new Company(Long.valueOf("1"), "companyTest", "test company description", "com", "www.company.com");
		coms.add(com);
		return coms;
	}

	@Override
	public void saveOrUpdate(Company entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Company entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List findByCriteria(Criterion criterion) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findAll(int maxResults) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List findByCriteria(Criterion criterion, int maxResults) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List getMore() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
