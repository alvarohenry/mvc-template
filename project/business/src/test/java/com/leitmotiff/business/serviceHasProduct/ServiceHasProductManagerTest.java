package com.leitmotiff.business.serviceHasProduct;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.leitmotiff.business.review.dto.ReviewProductDTO;
import com.leitmotiff.business.serviceHasProduct.dto.ServiceHasProductDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-business-context.xml" })
public class ServiceHasProductManagerTest {
	
	@Resource
	private ServiceHasProductManager shasPManager;
	
	
	
	@Test
	public void testGetOrderPrice(){
//		List<ServiceHasProductDTO> shasP = shasPManager.getOrderPrice(1L, true, true);
//		assertTrue(shasP.size() == 2);
//		assertEquals("product1",shasP.get(0).getProduct().getName());
//		shasP = shasPManager.getOrderPrice(1L, false, false);
//		assertTrue(shasP.size() == 2);
//		assertEquals("service2",shasP.get(0).getService().getName());
	}
//	
//	@Test
//	public void testGetOrderValue(){
//		List<ServiceHasProductDTO> shasP = shasPManager.getOrderPrice(1L, true, true);
//		assertTrue(shasP.size() == 2);
//		assertTrue(10 == shasP.get(0).getProduct().getPositive());
//		shasP = shasPManager.getOrderPrice(1L, false, false);
//		assertTrue(shasP.size() == 2);
//		assertTrue(100 == shasP.get(0).getService().getPositive());
//	}
	
//	@Test
//	public void testGetServiceHasProduct(){
//		List<ServiceHasProductDTO> list = shasPManager.getServiceHasProducts(1L, true, 1);
//		assertEquals("product1",list.get(0).getProduct().getName());
//	}
//	
//	@Test
//	public void testGetMore(){
//		List<ServiceHasProductDTO> list = shasPManager.getMore();
//		assertEquals("product2",list.get(0).getProduct().getName());
//	}
//	
}
