package com.leitmotiff.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.leitmotiff.business.product.ProductManager;
import com.leitmotiff.business.product.dto.ProductDTO;

@Controller
public class ProductController {

	@Autowired
	private ProductManager productManager;

	@RequestMapping(value="/products", method=RequestMethod.POST)
//	@RequestMapping(value="/products", method=RequestMethod.GET,headers="Accept=application/json")
	public @ResponseBody List<ProductDTO> listProducts(@RequestParam("id") Long id,@RequestParam("maxResults") int maxResults) {	
		return productManager.getProducts(id,maxResults);
	}
	
	@RequestMapping(value="/products/more", method=RequestMethod.POST)
	public @ResponseBody List<ProductDTO> listMoreProducts(){
		return productManager.getMoreProducts();
	}
	
	
	@RequestMapping(value="/products/price", method=RequestMethod.POST)
	public @ResponseBody List<ProductDTO> listProductsOrderPrice(@RequestParam("id") Long id, @RequestParam("order") boolean ord) {	
		return productManager.getProductsOrderPrice(id, ord);
	}
	
	@RequestMapping(value="/products/value", method=RequestMethod.POST)
	public @ResponseBody List<ProductDTO> listProductsOrderValue(@RequestParam("id") Long id, @RequestParam("order") boolean ord) {	
		return productManager.getProductsOrderValue(id, ord);
	}
	
	@RequestMapping(value="product", method=RequestMethod.POST)
	public @ResponseBody ProductDTO getProduct(@RequestParam("id") Long id){
		return productManager.getProduct(id);
	}
	
	@RequestMapping("/index3")
	public String prueba(){
		return "index3";
	}
	
}
