package com.leitmotiff.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.leitmotiff.business.service.ServiceManager;
import com.leitmotiff.business.service.dto.ServiceDTO;

@Controller
public class ServiceController {

	@Autowired
	private ServiceManager serviceManager;
	
	@RequestMapping(value="/services", method=RequestMethod.POST)
	public @ResponseBody List<ServiceDTO> listServices(@RequestParam("id") long idCategory, @RequestParam("maxResults") int maxResults ){
		return serviceManager.getServices(idCategory, maxResults);
	}
	
	@RequestMapping(value="/service", method=RequestMethod.POST)
	public @ResponseBody ServiceDTO getService(@RequestParam("id") long id){
		return serviceManager.getService(id);
	}
	
	@RequestMapping(value="/services/more", method=RequestMethod.POST)
	public @ResponseBody List<ServiceDTO> listMore(){
		return serviceManager.getMoreService();
	}
	
}