package com.leitmotiff.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.leitmotiff.business.search.SearchManager;
import com.leitmotiff.business.search.dto.SearchDTO;

@Controller
public class SearchController {

	@Autowired
	private SearchManager searchManager;
	
	@RequestMapping(value="/search",method=RequestMethod.POST)
	public @ResponseBody List<SearchDTO> getSearched(@RequestParam("search") String search, @RequestParam("maxResults") int maxResults){
		return searchManager.getSearched(search, maxResults);
	}
	
	@RequestMapping(value="/search/more",method=RequestMethod.POST)
	public @ResponseBody List<SearchDTO> getMore(){
		return searchManager.getMore();
	}
	
}
