package com.leitmotiff.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.leitmotiff.business.review.dto.ReviewProductDTO;
import com.leitmotiff.business.review.dto.ReviewServiceDTO;
import com.leitmotiff.business.reviewProduct.ReviewProductManager;
import com.leitmotiff.business.reviewService.ReviewServiceManager;

@Controller
public class ReviewController {

	@Autowired
	private ReviewProductManager reviewProductManager;
	
	@Autowired
	private ReviewServiceManager reviewServiceManager;
	
	@RequestMapping(value="/reviews/product", method=RequestMethod.POST)
	public @ResponseBody List<ReviewProductDTO> getProductReviews(@RequestParam("id") Long idProduct, @RequestParam("maxResults") int maxResult){
		return reviewProductManager.getReviews(idProduct, maxResult);
	}
	
	@RequestMapping(value="/reviews/product/more", method=RequestMethod.POST)
	public @ResponseBody List<ReviewProductDTO> getMoreProductReviews(){
		return reviewProductManager.getMore();
	}
	
	@RequestMapping(value="/reviews/service", method=RequestMethod.POST)
	public @ResponseBody List<ReviewServiceDTO> getServiceReviews(@RequestParam("id") Long idProduct, @RequestParam("maxResults") int maxResult){
		return reviewServiceManager.getReviews(idProduct, maxResult);
	}
	
	@RequestMapping(value="/reviews/service/more", method=RequestMethod.POST)
	public @ResponseBody List<ReviewServiceDTO> getMoreServiceReviews(){
		return reviewServiceManager.getMore();
	}	
	
	@RequestMapping(value = "/reviews/insert", method=RequestMethod.POST)
	public void insertReview(@RequestParam("review") String review, @RequestParam("bool") boolean bool){
		if(bool){
			reviewServiceManager.insertReview(review);
		}
		else{
			reviewProductManager.insertReview(review);
		}
	}
	
	@RequestMapping(value = "/reviews/number", method=RequestMethod.POST)
	public @ResponseBody List<Integer> getNumberReview(@RequestParam("id") long id, @RequestParam("boll") boolean bool){
		if(bool){
			return reviewServiceManager.numberReviews(id);
		}
		else{
			return reviewProductManager.numberReview(id);
		}
	}
}