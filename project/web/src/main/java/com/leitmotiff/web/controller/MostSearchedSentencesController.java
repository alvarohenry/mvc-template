package com.leitmotiff.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.leitmotiff.business.mostSearchedSentence.MostSearchedSentencesManager;
import com.leitmotiff.business.mostSearchedSentence.dto.MostSearchedSentencesDTO;

@Controller
public class MostSearchedSentencesController {

	@Autowired
	private MostSearchedSentencesManager mSSManager;
	
	@RequestMapping(value="/autocomplete", method=RequestMethod.POST)
	public @ResponseBody List<MostSearchedSentencesDTO> getAutocompleate(@RequestParam ("search") String search, @RequestParam ("maxResults") int maxResults){
		return mSSManager.getMostSearchedSentences(search);
	}
	
	@RequestMapping(value="/increase", method=RequestMethod.POST)
	public @ResponseBody boolean increaseValue(@RequestParam ("id") Long id){
		mSSManager.increaseValue(id);
		return true;
	}
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public @ResponseBody boolean addSentence(@RequestParam ("search") String search){
		mSSManager.addSentence(search);
		return true;
	}
	
}