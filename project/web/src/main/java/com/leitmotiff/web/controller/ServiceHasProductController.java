package com.leitmotiff.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.leitmotiff.business.serviceHasProduct.ServiceHasProductManager;
import com.leitmotiff.business.serviceHasProduct.dto.ServiceHasProductDTO;

@Controller
public class ServiceHasProductController {
	
	@Autowired
	private ServiceHasProductManager sHPM;
	
	@RequestMapping(value = "/serviceHasProduct", method=RequestMethod.POST)
	public @ResponseBody List<ServiceHasProductDTO> getServiceHsProduct(@RequestParam("id") long id, @RequestParam("table") boolean table, @RequestParam("maxResults") int maxResults){
		System.out.println("Abuuuuuuuuuuu1");
		return sHPM.getServiceHasProducts(id, table, maxResults);
	}
	
	@RequestMapping(value = "/serviceHasProduct/more", method = RequestMethod.POST)
	public @ResponseBody List<ServiceHasProductDTO> getMore(){
		return sHPM.getMore();
	}
	
	@RequestMapping(value = "/serviceHasProduct/price",method = RequestMethod.POST)
	public @ResponseBody List<ServiceHasProductDTO> getOrderByPrice(@RequestParam("id") long id, @RequestParam("table") boolean table, @RequestParam("order") boolean order){
		return sHPM.getOrderPrice(id, table, order);
	}
	
	@RequestMapping(value = "/serviceHasProduct/value", method = RequestMethod.POST)
	public @ResponseBody List<ServiceHasProductDTO> getOrderByValue(@RequestParam("id") long id, @RequestParam("table") boolean table, @RequestParam("order") boolean order){
		return sHPM.getOrderValue(id, table, order);
	}
}
