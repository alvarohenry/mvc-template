package com.leitmotiff.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.leitmotiff.business.category.CategoryManager;
import com.leitmotiff.business.category.dto.CategoryDTO;

@Controller
public class CategoryController {

	@Autowired
	private CategoryManager categoryManager;
	
	@RequestMapping(value="/categories", method=RequestMethod.POST)
	public @ResponseBody List<CategoryDTO> listCategories() {
		return categoryManager.getAll();
	}
	/*metodo test para la vista index*/
	
	@RequestMapping("/index")
	public String pagina(){
		return "index2";
	}	
}