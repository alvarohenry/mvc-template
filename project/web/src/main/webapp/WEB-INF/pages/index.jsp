<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
	<!-- perfect scroll -->
	<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
	
	<script src="<c:url value="/resources/libs_perfectscrollbar/js/perfect-scrollbar.js"/>"></script>
	<script src="<c:url value="/resources/libs_perfectscrollbar/js/scroll.js"/>"></script>
	<script src="<c:url value="/resources/libs_perfectscrollbar/js/perfect-scrollbar.jquery.js"/>"></script>
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/libs_perfectscrollbar/css/perfect-scrollbar.css" />">
	
  	<!-- otros -->
  	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/estilo.css"/>">
	<!-- <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/list.css"/>">  -->
	<!-- <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/comments.css"/>">  -->

    <script src="<c:url value="/resources/js/List.js"/>"></script>
 	<script src="<c:url value="/resources/js/Search_AutoComplete.js"/>"></script>
   	<script src="<c:url value="/resources/js/comments.js"/>"></script>
   
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/jquery/jquery-ui.css"/>">
	<script src="<c:url value="/resources/jquery/jquery-ui.js"/>" type="text/javascript"> </script>
    <script type="text/javascript" src="<c:url value="/resources/js/category.js"/>"></script>
    
  	
    <title>Leitmotiff</title>
    
    <style>
		#Demo {
		  position: relative;
		  margin: 0px auto;
		  padding: 0px;
		  width: 100%;
		  height: 650px;
		  
		  overflow: hidden;
		}
		#Demo2{
		  position: relative;
		  margin: 0px auto;
		  padding: 0px;
		  width: 100%;
		  height: 100%;
		  background-color: inherit;
		  overflow: hidden;
		}
		.content{
		  background-color: inherit;
		}
		.ps-container {
		  overflow: hidden !important;
		}
		div {
		  display: block;
		}
	</style>
</head>
	 
<body>
		
<div id="container">
 
    <div id="left">
    	<div id ="logo"> <p>ParlaKuy</div>
        <div id="desplegable">
        	<div id="social2"> 
				<div><img src="resources/images/views.png" title="Vistas"/><span>1</span></div> 
            	<div><img src="resources/images/comments.png" title="N�mero de Comentarios"/><span>120</span></div>
            	<div><img src="resources/images/like.png" title="Comentarios Positivos"/><span>123</span></div>
            	<div><img src="resources/images/dislike.png" title="Comentarios Negativos"/><span>4</span></div>
            	<div><a href="#"><img src="resources/images/redirigir.fw.png"/></a></div>
            </div>    
            <div id="image">
            	<img src="resources/images/MOTO G XT 1040 LTE.png"/>
            	<div id="categoryproduct">  Smartphones </div>
    			<div id="nameproduct"> moto E </div>     
    			<div id="priceproduct"> S/. <span> 10.00</span></div>
    			<div id="descriptionproduct"> descripcion del producto descripcion del producto descripcion del producto descripcion del producto descripcion del producto descripcion del producto descripcion del producto descripcion del producto descripcion del producto descripcion del producto descripcion del producto descripcion del producto descripcion del producto descripcion del producto descripcion del producto descripcion del producto descripcion del producto descripcion del producto </div>
            </div>
            
        </div>
    </div>
	 
	<div id ="center">

        <div id="searchbox">
		    <input id="elem" autofocus placeholder="Escribe aqui" type="text" autocomplete="off" onkeypress="comprobador()">
		    <button  id="submit" onClick="busqueda()">Buscar</button>
		</div>
		        
        <div id="iconMenu" onclick="showGrid()"><img src="resources/images/back.png" width="20px" height="20px">	</div>
        <div id="iconMenu" onclick="showGrid()"><img src="resources/images/grid.png" width="20px" height="20px">	</div>
        
        <div class="menu">
            <div id="fil1">
	            <div id="Demo"
					 class="ps-container ps-active-x ps-active-y" 
					 data-ps-id="63a79a76-be1e-0f62-9c6a-35c7b08aa617">
			
			         <div class="content">
			         	<div id="list">
       				 	</div>
       				 	<div>Categorias:</div>
			         	<div id="contenidoC">
			         	
			         	</div>
			         </div>
			         
			         <div class="ps-scrollbar-y-rail" style="top: 0px; height: 200px; right: 3px;">
			         	<div class="ps-scrollbar-y" style="top: 0px; height: 200px;"></div>
			         </div>
			    </div>
            </div>
        </div>
	    

	</div>
 
    <div id="rigth">
    
    	<div id="containerComents">
    	<div> <span>La gente dice:</span></div>
    	 <div id="Demo2" 
		 	class="ps-container ps-active-x ps-active-y" 
			data-ps-id="63a79a76-be1e-0f62-9c6a-35c7b08aa617">
			<div class="content">
					<div id="allComments"> <!-- Algo no esta bioen aqui no entiendo lo comentare -->
    					<!-- <div id='comment'><img src='resources/images/dislike.png'> <p>"+elem.body+"--------------- - ----- -------- -------- ------- ----- -------- ------ sigue el coemntarioo comentarioo<div id="info_comment"><img src='resources/images/user_mini.png'><div >Lucho</div><img src='resources/images/date_mini.png'>26 Jun, 2015 <a href="#"><img src='resources/images/face_mini.png'></a></div></div>  -->
    				</div>
			</div>
			
			<div class="ps-scrollbar-y-rail" style="top: 0px; height: 200px; right: 3px;">
				<div class="ps-scrollbar-y" style="top: 0px; height: 200px;"></div>
				</div>
		</div>

    	
    		<!-- <div id='BPaginate'> <button onclick="ShowMoreCommentsAbout()">Ver mas...</button></div>  -->
     	
    </div>
    <!-- <div id="socialMenu">
    		<a><img src="resources/images/face.png"></a>
    		<a><img src="resources/images/tweeter.png"></a>
    		<a><img src="resources/images/gplus.png"></a>
    </div>
     -->
    
    
</div>
   

</body>

</html>